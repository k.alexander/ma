# check if LIBTORCH_COMPUTE_PLATFORM is set, if not set it to cpu
if(NOT DEFINED LIBTORCH_COMPUTE_PLATFORM)
  set(LIBTORCH_COMPUTE_PLATFORM "cpu")
endif()


if(CMAKE_BUILD_TYPE MATCHES Debug)
  # target_compile_definitions(segment PUBLIC DEBUG)
  set(TORCH_BUILD_TYPE "debug")
elseif(CMAKE_BUILD_TYPE MATCHES Release OR CMAKE_BUILD_TYPE MATCHES
                                           RelWithDebInfo)
  set(TORCH_BUILD_TYPE "release")
else()
  message(FATAL_ERROR "Unknown build type: ${CMAKE_BUILD_TYPE}")
endif()

set(LIBTORCH_DIR
    "${CMAKE_SOURCE_DIR}/library/deps/torch/${LIBTORCH_COMPUTE_PLATFORM}/${TORCH_BUILD_TYPE}/libtorch"
)

set(Torch_DIR "${LIBTORCH_DIR}/share/cmake/Torch")


file(
  GLOB
  TORCH_DLLS
  "${LIBTORCH_DIR}/lib/*${CMAKE_SHARED_LIBRARY_SUFFIX}"
  "${LIBTORCH_DIR}/lib/*${CMAKE_SHARED_LIBRARY_SUFFIX}.1" # there's one library
                                                          # with a .1 suffix on
                                                          # linux
)