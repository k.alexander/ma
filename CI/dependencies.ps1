$url = "https://download.pytorch.org/libtorch/cpu/libtorch-win-shared-with-deps-2.2.0%2Bcpu.zip"
$output = ".\torch.zip"
$extractTo = "library/deps/torch/cpu/release"

if (!(Test-Path -Path $extractTo)) {
    Write-Output "Downloading and extracting libtorch..."
    New-Item -ItemType Directory -Force -Path $extractTo
    Invoke-WebRequest -Uri $url -OutFile $output
    Expand-Archive -Path $output -DestinationPath $extractTo
    # Remove the downloaded zip file
    Remove-Item -Path $output -Force
} else {
    Write-Output "libtorch already exists, skipping download and extraction."
}
