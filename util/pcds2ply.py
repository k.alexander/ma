import sys
import os


try:
    import open3d as o3d
    import pandas as pd
    import numpy as np
    from pyquaternion import Quaternion
    import math
except ImportError:
    print("Please install open3d, pandas and numpy with `pip install open3d pandas numpy`")
    sys.exit(1)

def pcds_to_ply(pcd_files_path, ply_file_path):
    pcd = o3d.geometry.PointCloud()
    q = Quaternion(axis=[1, 0, 0], degrees=-90)
    # load poses .csv file
    poses = pd.read_csv(pcd_files_path + "/poses.txt", delimiter="\s+", header=None)
    for file in os.listdir(pcd_files_path):
        if file.endswith(".pcd"):
            index = int(file.split(".")[0]) + 1
            scan = o3d.io.read_point_cloud(pcd_files_path + "/" + file)
            translation_vec = poses.iloc[index, 2:5].to_numpy().astype(np.float64)
            rotation = poses.iloc[index, 5:9].to_numpy().astype(np.float64)
            # w x y z -> x y z w
            column_index = [1, 2, 3, 0]
            rotation = rotation[column_index]


            rotation = q * Quaternion(rotation)

            # convert quaternion to rotation matrix
            rotation_3x3 = o3d.geometry.get_rotation_matrix_from_quaternion(rotation.q)

            # rotate translation_vec by q
            # translation_vec = q.rotate(translation_vec)

            # convert translation to 4x4 transformation matrix
            translation = np.eye(4)
            translation[0:3, 3] = translation_vec

            # convert 3x3 rotation matrix to 4x4 transformation matrix
            rotation = np.eye(4)
            rotation[0:3, 0:3] = rotation_3x3

            # combine the transformation
            transformation = translation @ rotation
            # apply transformation
            scan.transform(transformation)
            pcd += scan



    # Write the point cloud to a .ply file
    o3d.io.write_point_cloud(ply_file_path, pcd)

if len(sys.argv) != 3:
    print("Usage: python pcd2ply.py <input_pcd_files_folder> <output_ply_file>")
    sys.exit(1)

input = sys.argv[1]
output = sys.argv[2]

pcds_to_ply(input, output)