import sys

try:
    import open3d as o3d
except ImportError:
    print("Please install open3d with `pip install open3d`")
    sys.exit(1)

def pcd_to_ply(pcd_file_path, ply_file_path):
    # Load the point cloud
    pcd = o3d.io.read_point_cloud(pcd_file_path)

    # check if the point cloud has color information
    if len(pcd.colors) > 0:
        print("The point cloud has color information")

    # Write the point cloud to a .ply file
    o3d.io.write_point_cloud(ply_file_path, pcd)

if len(sys.argv) != 3:
    print("Usage: python pcd2ply.py <input_pcd_file> <output_ply_file>")
    sys.exit(1)

input = sys.argv[1]
output = sys.argv[2]

pcd_to_ply(input, output)