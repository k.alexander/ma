/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <segment.hpp>
#include <string>
#include <vector>

#include "util/types.hpp"

namespace segment {

struct RandLANetConfig : public SegmentRandLANetConfig {

    float noise_init; // noise initial parameter
    int num_points;   // number of points to sample
    int num_layers;
    int num_classes;
    int k_n;
    int val_batch_size;

    std::vector<int> subsampling_ratio;

    float augment_scale_min {};
    float augment_scale_max {};
    bool augment_scale_anisotropic {};
    std::vector<bool> augment_symmetries {};
    float augment_noise {};

    RandLANetConfig() { init_defaults(); }

    SegmentError load(Path const& path);

    void init_defaults();
};

}