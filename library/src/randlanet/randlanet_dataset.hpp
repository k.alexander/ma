/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <CKDT.hpp>
#include <atomic>
#include <deque>
#include <mutex>
#include <segment.h>
#include <thread>

#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "randlanet/randlanet_sampler.hpp"
#include "util/types.hpp"
#include "util/util.hpp"

namespace segment {
namespace util {
class Logger;
}

struct RandLANetConfig;

class RandLANetDataset {

    RandLANetConfig const& m_config;
    util::Logger* m_logger {};

    std::unique_ptr<RandLANetSampler> m_sampler;

    Path m_original_pointcloud_path {};
    Path m_temp_file_path {}; // for calibration, kdtree and projection data

    util::Pointcloud m_pointcloud {}, m_downsampled_cloud {};
    std::shared_ptr<ckdt::kdtree> m_downsampled_kd_tree {};

    Tensor m_potential {};
    Tensor m_argmin_potential {};
    Tensor m_min_potential {};
    Tensor m_downsampled_cloud_points {};
    Tensor m_downsampled_cloud_features {};
    Tensor m_projection_indices {}; // indices of closest point in the downsampled cloud for each point in the original cloud

    float m_batch_limit { 1 };

    // Threaded data fetching
    std::mutex m_item_queue_mutex {};
    std::deque<TensorList> m_item_queue {};
    std::vector<std::thread> m_data_fetch_threads {};
    std::atomic<bool> m_dataset_error { false };
    std::atomic<bool> m_data_fetch_threads_running { false };

    std::vector<int> m_label_values = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> m_ignored_labels = { 0 };

    TensorList fetch_potential_item();

    void fetch_thread_method(int thread_id);

    Tensor augment_input(Tensor xyz, Tensor rgb);

public:
    TensorList build_input(Tensor xyz, Tensor rgb, Tensor indices);

    TensorList segmentation_inputs(Tensor stacked_points, Tensor stacked_features, Tensor labels, Tensor stacked_lengths) const;

    RandLANetDataset(Path const& original_pointcloud, RandLANetConfig const& cfg, util::Logger* log);

    RandLANetDataset(float const* points, float const* colors, uint64_t num_points, RandLANetConfig const& cfg, util::Logger* log);

    RandLANetDataset(RandLANetDataset const&) = delete;

    ~RandLANetDataset() { stop_data_fetch_threads(); }

    SegmentError prepare_pointcloud_data();

    SegmentError prepare_projection();

    void start_data_fetch_threads();

    void stop_data_fetch_threads();

    TensorList fetch_potential_from_queue();

    Tensor downsampled_points() const { return m_downsampled_cloud_points; }

    util::Pointcloud const& pointcloud() const
    {
        return m_pointcloud;
    }

    util::Pointcloud& pointcloud() { return m_pointcloud; }

    Tensor projection_indices() const { return m_projection_indices; }

    std::vector<int> const& label_values() const { return m_label_values; }
    std::vector<int> const& ignored_labels() const { return m_ignored_labels; }

    std::shared_ptr<ckdt::kdtree> downsampled_kd_tree() const { return m_downsampled_kd_tree; }

    RandLANetConfig const& config() const { return m_config; }

    RandLANetSampler* sampler() { return m_sampler.get(); }
};
}
