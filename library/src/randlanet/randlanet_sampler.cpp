/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "randlanet/randlanet_sampler.hpp"

#include "randlanet/randlanet_config.hpp"
#include "randlanet/randlanet_dataset.hpp"

#include <torch/script.h>

namespace segment {

thread_local Tensor possibility, min_possibility;

#if WITH_TESTS
void RandLANetSampler::set_possibility(Tensor _possibility)
{
    possibility = _possibility;
}
#endif

RandLANetSampler::RandLANetSampler(RandLANetDataset* dataset)
    : m_dataset(dataset)

{
    m_kd_tree = m_dataset->downsampled_kd_tree();
    m_shared_possibility = torch::zeros({ m_dataset->downsampled_points().size(0) }, torch::kFloat32);
#if 0
    torch::jit::script::Module sampler_data = torch::jit::load(TEST_DATA_PATH "/randla_sampler/py_sampler_input.pt");
    indices = sampler_data.attr("pick_idx").toTensor().to(torch::kInt64);
    _noise = sampler_data.attr("noise").toTensor().to(torch::kFloat32);
#endif
}

std::tuple<Tensor, Tensor> RandLANetSampler::sample()
{
    if (possibility.numel() == 0) {
        possibility = torch::rand({ m_dataset->downsampled_points().size(0) }) * 1e-3;
        min_possibility = torch::min(possibility);
    } else {
        m_mutex.lock();
        possibility.add_(m_shared_possibility);
        m_mutex.unlock();
    }

    auto point_idx = torch::argmin(possibility);

    auto points = m_dataset->downsampled_points();

    auto center_point = points[point_idx].clone();

    auto noise = torch::randn_like(center_point) * (m_dataset->config().noise_init / 10);
    auto pick_point = (center_point + noise).to(torch::kCPU).to(torch::kFloat32);

    Vec3f point = Vec3f::one_from_tensor(pick_point);
    ckdt::v3 point_v3 = { point.x, point.y, point.z };
    auto results = m_kd_tree->knn(&point_v3, 1, m_dataset->config().num_points);

    if (results.distances.size() != m_dataset->config().num_points) {
        throw std::runtime_error("Failed to find neighbors");
    }

    auto indices_tensor = torch::from_blob(results.indices.data(), { m_dataset->config().num_points }, torch::kInt64);

    points = points.index_select(0, indices_tensor) - pick_point;
    auto distst = torch::sum(torch::pow(points, 2), 1, false, torch::kFloat32);

    auto delta = torch::pow(1 - distst / torch::max(distst), 2).to(torch::kFloat32);

    possibility.index_add_(0, indices_tensor, delta);
    min_possibility = torch::min(possibility);

    m_mutex.lock();
    m_shared_possibility.index_add_(0, indices_tensor, delta);
    m_mutex.unlock();
    return { pick_point,  indices_tensor.clone() };
}

#if defined(WITH_TESTS)
Tensor RandLANetSampler::sample_center_point()
{
    static int index = 0;
    auto point_idx = torch::argmin(possibility);

    auto points = m_dataset->downsampled_points();

    auto center_point = points[point_idx].clone();

    auto noise = _noise[index++];
    auto pick_point = (center_point + noise).to(torch::kCPU).to(torch::kFloat32);

    Vec3f point = Vec3f::one_from_tensor(pick_point);
    ckdt::v3 point_v3 = { point.x, point.y, point.z };
    auto results = m_kd_tree->knn(&point_v3, 1, m_dataset->config().num_points);

    if (results.distances.size() != m_dataset->config().num_points) {
        throw std::runtime_error("Failed to find neighbors");
    }

    auto indices_tensor = torch::from_blob(results.indices.data(), { m_dataset->config().num_points }, torch::kInt64);

    points = points.index_select(0, indices_tensor) - pick_point;
    auto distst = torch::sum(torch::pow(points, 2), 1, false, torch::kFloat32);

    auto delta = torch::pow(1 - distst / torch::max(distst), 2).to(torch::kFloat32);

    possibility.index_add_(0, indices_tensor, delta);

    min_possibility = torch::min(possibility);

    return pick_point - noise; // only for testing
}
#endif
}
