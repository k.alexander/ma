/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "util/types.hpp"

#include "randlanet_config.hpp"

namespace segment {

struct RandLANetBatch {

    int list_size = 0;
    int n_layers = 0;
    TensorList xyz {}, neigh_idx {}, sub_idx {}, interp_idx;
    Tensor labels {}, features {}, input_inds {}, cloud_inds {};

    RandLANetBatch() = default;

    RandLANetBatch(TensorList input_list, int n_layers);

    void print_sizes() const;

    void to(torch::Device device);

    void to_coallesced_list(TensorList& list, int index, RandLANetConfig const& cfg) const;

    bool is_equal(const RandLANetBatch& other) const;
};
}