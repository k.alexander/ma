/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <CKDT.hpp>
#include <mutex>

#include "pointcloud/pointcloud.hpp"
#include "util/types.hpp"
#include "util/vec3.hpp"

namespace segment {

class RandLANetDataset;

class RandLANetSampler {
    RandLANetDataset* m_dataset;

    std::shared_ptr<ckdt::kdtree> m_kd_tree {};
    std::mutex m_mutex;
    Tensor m_shared_possibility; // shared between threads

#if defined(WITH_TESTS)
    Tensor indices, _noise;
#endif

public:
    RandLANetSampler(RandLANetDataset* dataset);

    ~RandLANetSampler() = default;

    std::tuple<Tensor, Tensor> sample();

#if defined(WITH_TESTS)
    Tensor sample_center_point();

    void set_possibility(Tensor possibility);
#endif
};

}
