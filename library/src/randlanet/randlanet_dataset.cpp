/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
#include <CKDT.hpp>

#include "randlanet/randlanet_dataset.hpp"

#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/pointcloud.hpp"
#include "randlanet/randlanet.hpp"
#include "util/fmt.hpp"
#include "util/logger.hpp"
#include "util/util.hpp"

namespace segment {

RandLANetDataset::RandLANetDataset(Path const& original_pointcloud, RandLANetConfig const& cfg, util::Logger* log)
    : m_config(cfg)
    , m_original_pointcloud_path(original_pointcloud)
    , m_temp_file_path(original_pointcloud.parent_path())
    , m_logger(log)
{
}

RandLANetDataset::RandLANetDataset(float const* points, float const* colors, uint64_t num_points, RandLANetConfig const& cfg, util::Logger* log)
    : m_config(cfg)
    , m_original_pointcloud_path(util::get_temp_file("ply", points, 256))
    , m_temp_file_path(m_original_pointcloud_path.parent_path())
    , m_logger(log)
{
    m_pointcloud.points.resize(num_points);
    m_pointcloud.colors.resize(num_points);
    m_pointcloud.file_path = m_original_pointcloud_path;
    std::memcpy(m_pointcloud.points.data(), points, num_points * 3 * sizeof(float));
    std::memcpy(m_pointcloud.colors.data(), colors, num_points * 3 * sizeof(float));
}

SegmentError RandLANetDataset::prepare_pointcloud_data()
{
    m_logger->set_progress(0.0f, "Loading point cloud");
    if (m_pointcloud.points.size() == 0 && !util::load_ply(m_original_pointcloud_path, m_pointcloud, 1 / 255.f))
        return SEGMENT_POINTCLOUD_LOADING_FAILED;

    m_downsampled_cloud = util::downsample_cloud(m_pointcloud, m_temp_file_path, 0.06f, m_logger);

    if (m_downsampled_cloud.points.size() == 0)
        return SEGMENT_DOWNSAMPLING_FAILED;

    m_downsampled_cloud_points = m_downsampled_cloud.points_to_tensor();
    m_downsampled_cloud_features = m_downsampled_cloud.colors_to_tensor();

    assert(torch::max(m_downsampled_cloud_features).item<float>() <= 1.0f && "RGB values should be normalized to 1.0");
    assert(torch::min(m_downsampled_cloud_features).item<float>() >= 0.0f && "RGB values should be normalized to 0.0");

    m_logger->set_progress(0.0f, "Creating or loading kdtree");
    auto path = util::get_temp_file("bin", m_downsampled_cloud.points.data(), 256);

    ckdt::tree_settings settings {};
    settings.m_leafsize = 50;
    settings.m_copy_data = true;
    auto points = m_downsampled_cloud_points.to(torch::kFloat64).contiguous();
    m_downsampled_kd_tree = std::make_shared<ckdt::kdtree>(points.data_ptr<double>(), m_downsampled_cloud.points.size(), 3, settings);

    assert(ckdt::isfinite((ckdt::v3 const*)points.data_ptr<double>(), m_downsampled_cloud.points.size()));

    if (!m_downsampled_kd_tree)
        return SEGMENT_KDTREE_CREATION_FAILED;

    m_sampler = std::make_unique<RandLANetSampler>(this);

    return SEGMENT_OK;
}

SegmentError RandLANetDataset::prepare_projection()
{
    m_logger->set_progress(0.0f, "Preparing projection");
    auto proj_file = m_temp_file_path / fmt::format("{}_{}", m_pointcloud.file_path.filename().stem().u8string(), "proj.pt");

    // Try to load previous indices
    if (std::filesystem::exists(proj_file)) {
        torch::load(m_projection_indices, proj_file.u8string());
        return m_projection_indices.size(0) > 0 ? SEGMENT_OK : SEGMENT_LOADING_PROJECTION_FAILED;
    }

    auto labels = torch::zeros({ (int64_t)m_pointcloud.points.size() }, torch::dtype(torch::kInt32));

    // Compute projection inds
    m_projection_indices = torch::empty({ (int64_t)m_pointcloud.points.size() }, torch::dtype(torch::kInt32));
    auto accessor = m_projection_indices.accessor<int32_t, 1>();
    for (size_t i = 0; i < m_pointcloud.points.size(); ++i) {
        ckdt::v3 point = { m_pointcloud.points[i][0], m_pointcloud.points[i][1], m_pointcloud.points[i][2] };
        auto result = m_downsampled_kd_tree->knn(&point, 1, 1);
        accessor[i] = (int32_t)result.indices[0];
    }

    // save indices
    torch::save(m_projection_indices, proj_file.u8string());
    m_logger->set_progress(1.0f, "Preparing projection");
    return SEGMENT_OK;
}

TensorList RandLANetDataset::fetch_potential_from_queue()
{
    while (true) {
        {
            std::lock_guard<std::mutex> lock(m_item_queue_mutex);
            if (!m_item_queue.empty()) {
                auto item = m_item_queue.front();
                m_item_queue.pop_front();
                return item;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if (m_dataset_error)
            return {};
    }
}

Tensor RandLANetDataset::augment_input(Tensor xyz, Tensor rgb)
{

    auto theta = torch::rand({ 1 }, torch::dtype(torch::kFloat32)) * 2 * M_PI;

    auto transformed_xyz = util::rotate(xyz, 0.f, 0.f, theta[0].item<float>());

    auto min_s = m_config.augment_scale_min;
    auto max_s = m_config.augment_scale_max;

    Tensor scales;

    if (m_config.augment_scale_anisotropic) {
        scales = torch::rand({ 3 }) * (max_s - min_s) + min_s;
    } else {
        scales = torch::rand({ 1 }) * (max_s - min_s) + min_s;
        scales = torch::cat({ scales, scales, scales });
    }

    Tensor symmetries = torch::zeros({ 3 }, torch::dtype(torch::kFloat32));
    for (int i = 0; i < 3; i++) {
        auto s = m_config.augment_symmetries[i];
        if (s) {
            symmetries.index_put_({ i }, torch::round(torch::rand({ 1 })) * 2 - 1);
        } else {
            symmetries.index_put_({ i }, 1);
        }
    }

    scales *= symmetries;

    transformed_xyz *= scales;

    auto noise = torch::randn_like(transformed_xyz) * m_config.augment_noise;
    transformed_xyz = transformed_xyz + noise;

    return torch::cat({ transformed_xyz, rgb }, -1);
}

inline Tensor kd_tree(Tensor data, Tensor xyz, int k)
{
    auto kdpoints = data.to(torch::kFloat64);
    auto query_points = xyz.to(torch::kFloat64);

    ckdt::kdtree tree(kdpoints.data_ptr<double>(), kdpoints.size(0));
    auto accessor = query_points.accessor<double, 2>();

    Tensor result = torch::zeros({ xyz.size(0), k }, torch::dtype(torch::kInt32));
    auto result_accessor = result.accessor<int32_t, 2>();

    for (int i = 0; i < xyz.size(0); i++) {
        ckdt::v3 point = { accessor[i][0], accessor[i][1], accessor[i][2] };
        auto result = tree.knn(&point, 1, k);

        for (int j = 0; j < k; j++) {
            result_accessor[i][j] = result.indices[j];
        }
    }
    return result;
}

TensorList RandLANetDataset::build_input(Tensor xyz, Tensor rgb, Tensor indices)
{
    auto features = augment_input(xyz, rgb);
    auto labels = torch::zeros({ (int64_t)xyz.size(0) }, torch::dtype(torch::kInt64));
    auto pc_id = torch::zeros({ 1 }, torch::dtype(torch::kInt32));

    TensorList input_points {};
    TensorList input_neighbors {};
    TensorList input_pools {};
    TensorList input_up_samples {};

    for (int i = 0; i < m_config.num_layers; i++) {
        auto neigh_idx = kd_tree(xyz, xyz, m_config.k_n);
        auto sub_sampling_idx = xyz.size(0) / m_config.subsampling_ratio[i];
        auto sub_points = xyz.slice(0, 0, sub_sampling_idx);
        auto pool_i = neigh_idx.slice(0, 0, sub_sampling_idx);
        auto up_i = kd_tree(sub_points, xyz, 1).squeeze(-1);

        input_points.push_back(xyz);
        input_neighbors.push_back(neigh_idx);
        input_pools.push_back(pool_i);
        input_up_samples.push_back(up_i);

        xyz = sub_points;
    }

    TensorList inputs {};
    inputs.insert(inputs.end(), input_points.begin(), input_points.end());
    inputs.insert(inputs.end(), input_neighbors.begin(), input_neighbors.end());
    inputs.insert(inputs.end(), input_pools.begin(), input_pools.end());
    inputs.insert(inputs.end(), input_up_samples.begin(), input_up_samples.end());
    inputs.push_back(features);
    inputs.push_back(labels);
    inputs.push_back(indices);
    inputs.push_back(pc_id);

    return inputs;
}

TensorList RandLANetDataset::fetch_potential_item()
{
    auto [center, indices] = m_sampler->sample();

    
    // shuffle index inplace
    auto shuffle_indices = torch::randperm(indices.size(0), torch::kInt64);
    indices = indices.index_select(0, shuffle_indices).clone();

    auto queried_pc_xyz = m_downsampled_cloud_points.index_select(0, indices).clone() - center;
    auto queried_pc_color = m_downsampled_cloud_features.index_select(0, indices).clone();


    return build_input(queried_pc_xyz, queried_pc_color, indices);
}

void RandLANetDataset::fetch_thread_method(int thread_id)
{
    at::init_num_threads();
    std::size_t queue_size = 0;
    int failed_attempts = 0;
    while (m_data_fetch_threads_running) {
        while (true) {
            m_item_queue_mutex.lock();
            queue_size = m_item_queue.size();
            m_item_queue_mutex.unlock();
            if (queue_size < 10 || !m_data_fetch_threads_running)
                break;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        auto item = fetch_potential_item();
        if (item.size() > 0) {
            std::lock_guard<std::mutex> lock(m_item_queue_mutex);
            m_item_queue.push_back(item);
        } else {
            failed_attempts++;
            m_logger->error("Failed to fetch item");
            if (failed_attempts > 10) {
                m_dataset_error = true;
                m_logger->error("Too many failed attempts to fetch item, stopping thread");
                break;
            }
        }
    }
}

void RandLANetDataset::start_data_fetch_threads()
{
    const int num_threads = std::clamp<int>(m_config.base.num_fetch_threads, 1, std::thread::hardware_concurrency());
    if (m_data_fetch_threads_running)
        return;
    m_data_fetch_threads_running = true;
    for (int i = 0; i < num_threads; ++i) {
        m_data_fetch_threads.push_back(std::thread(std::bind(&RandLANetDataset::fetch_thread_method, this, i)));
    }
}

void RandLANetDataset::stop_data_fetch_threads()
{
    if (!m_data_fetch_threads_running)
        return;
    m_data_fetch_threads_running = false;
    for (auto& thread : m_data_fetch_threads)
        thread.join();

    m_data_fetch_threads.clear();
}

}