/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "randlanet/randlanet_batch.hpp"
#include "util/util.hpp"

namespace segment {

RandLANetBatch::RandLANetBatch(TensorList input_list, int _n_layers)
{
    list_size = input_list.size();
    n_layers = _n_layers;
    xyz = TensorList(input_list.begin(), input_list.begin() + _n_layers);
    neigh_idx = TensorList(input_list.begin() + _n_layers, input_list.begin() + 2 * _n_layers);
    sub_idx = TensorList(input_list.begin() + 2 * _n_layers, input_list.begin() + 3 * _n_layers);
    interp_idx = TensorList(input_list.begin() + 3 * _n_layers, input_list.begin() + 4 * _n_layers);

    features = input_list[4 * _n_layers];
    labels = input_list[4 * _n_layers + 1];
    input_inds = input_list[4 * _n_layers + 2];
    cloud_inds = input_list[4 * _n_layers + 3];
    if (cloud_inds.dim() > 2)
        cloud_inds = cloud_inds.squeeze(2);
}

void RandLANetBatch::to_coallesced_list(TensorList& list, int index, RandLANetConfig const& cfg) const
{
    if (list.empty()) {
        // prepare list
        list.resize(list_size);
        int start = 0;
        for (int i = start; i < n_layers; i++)
            list[i] = torch::zeros({ cfg.val_batch_size, xyz[i].size(0), xyz[i].size(1) }, torch::kFloat32);

        start += n_layers;
        for (int i = start; i < 2 * n_layers; i++)
            list[i] = torch::zeros({ cfg.val_batch_size, neigh_idx[i - n_layers].size(0), neigh_idx[i - n_layers].size(1) }, torch::kInt32);

        start += n_layers;
        for (int i = start; i < 3 * n_layers; i++)
            list[i] = torch::zeros({ cfg.val_batch_size, sub_idx[i - 2 * n_layers].size(0), sub_idx[i - 2 * n_layers].size(1) }, torch::kInt32);

        start += n_layers;
        for (int i = start; i < 4 * n_layers; i++)
            list[i] = torch::zeros({ cfg.val_batch_size, interp_idx[i - 3 * n_layers].size(0) }, torch::kInt32);

        list[4 * n_layers] = torch::zeros({ cfg.val_batch_size, features.size(0), features.size(1) }, torch::kFloat32);
        list[4 * n_layers + 1] = torch::zeros({ cfg.val_batch_size, labels.size(0) }, torch::kInt64);
        list[4 * n_layers + 2] = torch::zeros({ cfg.val_batch_size, cfg.num_points }, torch::kInt32);
        list[4 * n_layers + 3] = torch::zeros({ cfg.val_batch_size }, torch::kInt32);
    }

    // put all tensors in the list at the given index
    int start = 0;

    for (int i = start; i < n_layers; i++)
        list[i][index] = xyz[i].clone();

    start += n_layers;
    for (int i = start; i < 2 * n_layers; i++)
        list[i][index] = neigh_idx[i - n_layers].clone();

    start += n_layers;
    for (int i = start; i < 3 * n_layers; i++)
        list[i][index] = sub_idx[i - 2 * n_layers].clone();

    start += n_layers;
    for (int i = start; i < 4 * n_layers; i++)
        list[i][index] = interp_idx[i - 3 * n_layers].clone();

    list[4 * n_layers][index] = features.clone();
    list[4 * n_layers + 1][index] = labels.clone();
    list[4 * n_layers + 2][index] = input_inds.clone();
    // list[4 * n_layers + 3][index] = cloud_inds.clone(); // cloud indices ar always zero anyways
}

void RandLANetBatch::print_sizes() const
{
    std::cout << "Points:\n";
    for (auto const& p : xyz)
        util::print_shape(p);
    std::cout << "neigh_idx:\n";
    for (auto const& p : neigh_idx)
        util::print_shape(p);
    std::cout << "sub_idx:\n";
    for (auto const& p : sub_idx)
        util::print_shape(p);
    std::cout << "interp_idx:\n";
    for (auto const& p : interp_idx)
        util::print_shape(p);

    util::print_shape(features);
    util::print_shape(labels);
    util::print_shape(input_inds);
    util::print_shape(cloud_inds);
}

void RandLANetBatch::to(torch::Device device)
{
    for (auto& p : xyz)
        p = p.to(device);
    for (auto& p : neigh_idx)
        p = p.to(device);
    for (auto& p : sub_idx)
        p = p.to(device);
    for (auto& p : interp_idx)
        p = p.to(device);

    features = features.to(device);
    labels = labels.to(device);
    cloud_inds = cloud_inds.to(device);
    input_inds = input_inds.to(device);
}

bool RandLANetBatch::is_equal(RandLANetBatch const& other) const
{
    bool equal = true;
    equal &= list_size == other.list_size;
    equal &= n_layers == other.n_layers;
    equal &= torch::allclose(features, other.features, 1e-5);
    equal &= torch::allclose(labels, other.labels, 1e-5);
    equal &= torch::allclose(input_inds, other.input_inds, 1e-5);
    equal &= torch::allclose(cloud_inds, other.cloud_inds, 1e-5);

    for (int i = 0; i < n_layers; i++) {
        equal &= torch::allclose(xyz[i], other.xyz[i], 1e-5);
        equal &= torch::allclose(neigh_idx[i], other.neigh_idx[i], 1e-5);
        equal &= torch::allclose(sub_idx[i], other.sub_idx[i], 1e-5);
        equal &= torch::allclose(interp_idx[i], other.interp_idx[i], 1e-5);
    }

    return equal;
}

} // namespace segment