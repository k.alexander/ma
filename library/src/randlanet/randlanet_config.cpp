/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "randlanet/randlanet_config.hpp"
#include "util/util.hpp"
#include <ini.h>

namespace segment {
SegmentError RandLANetConfig::load(Path const& path)
{
    return SEGMENT_FILE_NOT_FOUND; // TODO: implement
}

void RandLANetConfig::init_defaults()
{
    base.prefer_gpu_inference = true;
    base.inference_device = 0;
    base.num_iterations = 100;
    base.validation_size = 100;
    base.num_fetch_threads = is_gpu_available() ? 4 : 1;

    noise_init = 3.5f;
    num_points = 40000;

    num_layers = 5;
    k_n = 16;
    num_classes = 9; // 8 object classes + 1 for unclassified
    val_batch_size = 16;

    subsampling_ratio = { 4, 4, 4, 4, 2 };

    augment_scale_min = 0.8;
    augment_scale_max = 1.2;
    augment_scale_anisotropic = true;
    augment_symmetries = { true, false, false };
    augment_noise = 0.001f;
}

} // namespace segment
