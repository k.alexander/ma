/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <tuple>

#include "randlanet/randlanet.hpp"

#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "randlanet/randlanet_batch.hpp"
#include "randlanet/randlanet_dataset.hpp"
#include "util/fmt.hpp"
#include "util/util.hpp"

namespace segment {

RandLANet::RandLANet(const char* path)
    : m_model_path(path)
{
    m_logger.info("Initializing RandLA Network");
#if WITH_TESTS
    m_logger.enable_stdout();
#endif
}

bool RandLANet::try_load_model()
{
    m_logger.set_progress(0.0f, "Loading model from disk");
    try {
        m_model = torch::jit::load(m_model_path);
    } catch (const c10::Error& e) {
        m_logger.critical(fmt::format("Error loading model from {}: {}", m_model_path, e.what()));
        return false;
    }
    m_logger.set_progress(1.0f, "Loading model from disk");
    m_loaded = true;
    return true;
}

SegmentError RandLANet::segment_dataset(RandLANetDataset& dataset, util::Pointcloud& result)
{
    /*
        1. Load point cloud & kd trees and downsample
        2. Load calibration or recalibrate
        3. Segment point cloud
            1. iterate for the configured amount of steps
                1. iterate over all batches
                2. feed each batch to the network
                3. retrieve classifications for each point fed to the network
                4. add result to the output cloud
            2. normalize the predicted labels in the output cloud
            3. determine the label for each point in the output cloud based on the highest probability
            4. reproject the downsampled cloud with predictions to the original point cloud
            5. save the output cloud
    */

    auto const num_iterations = m_config.base.num_iterations;
    auto const n_classes = m_config.num_classes;
    auto const n_points = dataset.downsampled_points().size(0);

    auto n_votes = 2;

    auto xyz_probs
        = torch::zeros({ n_points, n_classes }, torch::kFloat32);
    xyz_probs.fill_(std::numeric_limits<float>::quiet_NaN());

    auto visited = torch::zeros(n_points, torch::kInt32);

    auto dev = torch::Device(torch::kCPU);

    if (segment_is_gpu_available() && m_config.base.prefer_gpu_inference)
#if defined(LIBTORCH_CUDA)
        dev = torch::Device(torch::kCUDA);
#elif defined(LIBTORCH_ROCM)
        dev = torch::Device(torch::kCUDA);
#else
        ;
#endif

    m_model.to(dev);
    m_model.eval();
    torch::NoGradGuard no_grad;
    torch::jit::getProfilingMode() = false;

    bool interrupted = false;

    double total_duration_ms = 0;
    int num_actual_iterations = 0;
    double total_main_idle_time_ms = 0;
    int num_fetch_calls = 0;

    double total_inference_time = 0;
    int num_inferences = 0;

    for (int i = 0; i < num_iterations && !interrupted; i++) {
        for (int validation_step = 0; validation_step < m_config.base.validation_size && !interrupted; validation_step++) {

            auto start = std::chrono::high_resolution_clock::now();
            // === Fetching batches ===

            m_logger.debug(fmt::format("Fetching batches..."));
            TensorList coallesced_input_list;

            for (int batch_index = 0; batch_index < m_config.val_batch_size; batch_index++) {
                auto start2 = std::chrono::high_resolution_clock::now();
                num_fetch_calls++;
                auto input_list = dataset.fetch_potential_from_queue();
                
                auto end2 = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::milli> dur2 = end2 - start2;
                total_main_idle_time_ms += dur2.count();

                m_logger.debug(fmt::format("Fetched batch {}/{}", batch_index, m_config.val_batch_size));

                // can happen if the dataset is empty or some other error occurred
                if (input_list.empty()) {
                    interrupted = true;
                    break;
                }
                auto batch = RandLANetBatch(input_list, m_config.num_layers);

                batch.to_coallesced_list(coallesced_input_list, batch_index, m_config);
            }

            float progress = float(i * m_config.base.validation_size + validation_step) / float(num_iterations * m_config.base.validation_size);
            m_logger.info(fmt::format("Iteration: {}/{} batch {}/{}", i, num_iterations, validation_step, m_config.base.validation_size));
            m_logger.set_progress(progress, "Segmentation");

            // === Forward pass ===

            auto batch = RandLANetBatch(coallesced_input_list, m_config.num_layers);
            c10::Dict<std::string, TensorList> inputs_dict;

            batch.to(dev);

            inputs_dict.insert("xyz", batch.xyz);
            inputs_dict.insert("neigh_idx", batch.neigh_idx);
            inputs_dict.insert("sub_idx", batch.sub_idx);
            inputs_dict.insert("interp_idx", batch.interp_idx);
            inputs_dict.insert("features", TensorList { batch.features });
            inputs_dict.insert("labels", TensorList { batch.labels });
            inputs_dict.insert("input_inds", TensorList { batch.input_inds });
            inputs_dict.insert("cloud_inds", TensorList { batch.cloud_inds });

            auto inference_start = std::chrono::high_resolution_clock::now();

            auto outputs = m_model.forward({ inputs_dict }).toTensor().detach().to(torch::kCPU);

            auto inferene_end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> inference_dur = inferene_end - inference_start;
            total_inference_time += inference_dur.count();
            m_logger.info(fmt::format("Inference step took {}ms", inference_dur.count()));
            num_inferences++;

            // === Evaluate segmentation for this pass ===

            outputs = torch::log_softmax(outputs, 1);
            outputs = torch::reshape(outputs, { m_config.val_batch_size, -1, m_config.num_classes });

            if (dev.is_cuda())
                torch::cuda::synchronize(dev.has_index() ? dev.index() : 0);

            for (int j = 0; j < outputs.size(0); j++) {
                auto probs = outputs[j].to(torch::kCPU).detach().to(torch::kFloat32);
                auto ids = batch.input_inds[j].to(torch::kCPU).detach().to(torch::kInt32);
                auto exp_probs = torch::exp(probs);
                auto nanmean = torch::nanmean(torch::stack({ xyz_probs.index({ ids }), exp_probs }), 0);
                xyz_probs.index_put_({ ids }, nanmean);
                visited.index_add_(0, ids, torch::ones_like(ids));
            }
            auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> dur = end - start;
            total_duration_ms += dur.count();
            num_actual_iterations++;
        }


        auto [unique_tensor, _] = torch::_unique(visited, false);
        auto least_visited = torch::min(unique_tensor);

        if (least_visited.item<int32_t>() > n_votes) {
            m_logger.info(fmt::format("Each point was visited at least {} times", n_votes));
            break;
        } else {
            m_logger.debug(fmt::format("Least visited point was visited {} times", least_visited.item<int32_t>()));
        }


    }

    m_logger.info(fmt::format("done after {} iterations\n Average iteration time: {}ms\n"
        " Avg. main idle time: {}ms\n"
        " Avg. inference time: {}ms",
        num_actual_iterations, total_duration_ms / num_actual_iterations, total_main_idle_time_ms / num_fetch_calls,
        total_inference_time / num_inferences));

    if (interrupted) {
        m_interrupt_segmentation = false;
        result.labels.clear();
        return SEGMENT_INTERRUPTED;
    }

    auto xyz_labels = torch::argmax(xyz_probs, 1).to(torch::kCPU).detach().to(torch::kInt32);

    assert(torch::max(xyz_labels).item<int32_t>() <= n_classes);

    auto projected_probs = xyz_labels.index({ dataset.projection_indices() });
    result.labels = util::from_tensor<uint8_t>(projected_probs);

    return SEGMENT_OK;
}

SegmentError RandLANet::segment_pointcloud(char const* input_cloud_path, char const* output_cloud_folder)
{
    if (!m_loaded)
        return SEGMENT_NO_MODEL_LOADED;

    RandLANetDataset dataset(input_cloud_path, m_config, &m_logger);
    SegmentError result = dataset.prepare_pointcloud_data();

    if (result != SEGMENT_OK)
        return result;
    dataset.start_data_fetch_threads();
    result = dataset.prepare_projection();
    if (result != SEGMENT_OK)
        return result;

    util::Pointcloud& output_cloud = dataset.pointcloud();
    result = segment_dataset(dataset, output_cloud);
    if (result == SEGMENT_OK)
        util::save_ply(output_cloud_folder, output_cloud);
    return result;
}

SegmentError RandLANet::segment_points(float const* points, float const* colors, uint64_t num_points, uint8_t** classifications)
{
    if (!m_loaded)
        return SEGMENT_NO_MODEL_LOADED;

    // measure time
    auto start = std::chrono::high_resolution_clock::now();
    RandLANetDataset dataset(points, colors, num_points, m_config, &m_logger);
    SegmentError result = dataset.prepare_pointcloud_data();

    m_logger.info(fmt::format("Preparation of {} points took: {} ms", num_points,  std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count()));

    if (result != SEGMENT_OK)
        return result;

    dataset.start_data_fetch_threads();
    result = dataset.prepare_projection();
    if (result != SEGMENT_OK)
        return result;

    util::Pointcloud& output_cloud = dataset.pointcloud();

    start = std::chrono::high_resolution_clock::now();
    result = segment_dataset(dataset, output_cloud);
    m_logger.info(fmt::format("Segmentation took: {} seconds", std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start).count()));

    if (result == SEGMENT_OK) {
        auto const& labels = output_cloud.labels;
        *classifications = (uint8_t*)malloc(labels.size() * sizeof(uint8_t));
        std::copy(labels.begin(), labels.end(), *classifications);
    }
    return result;
}

SegmentError RandLANet::initialize()
{
    if (!try_load_model())
        return SEGMENT_MODEL_LOADING_FAILED;

    m_config.init_defaults();
    return SEGMENT_OK;
}

} // namespace segment
