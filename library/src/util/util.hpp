/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <filesystem>
#include <fstream>
#include <highwayhash.h>
#include <sstream>
#include <string>
#include <torch/torch.h>

#include "util/fmt.hpp"
#include "util/types.hpp"

namespace segment::util {

template<typename T>
inline std::vector<T> from_tensor(Tensor tensor)
{
    std::vector<T> result(tensor.size(0));
    for (int i = 0; i < tensor.size(0); i++)
        result[i] = tensor[i].item<int>();
    return result;
}

template<typename T>
inline void serialize_vector(std::vector<T>& vec, std::filesystem::path const& output_file_path)
{
    std::ofstream output_file(output_file_path, std::ios::binary);
    for (auto const& value : vec) {
        output_file.write(reinterpret_cast<const char*>(&value), sizeof(T));
    }

    output_file.close();
}

template<typename T>
inline std::vector<T> deserialize_vector(std::filesystem::path const& output_file_path)
{
    std::vector<T> result;
    std::ifstream input_file(output_file_path, std::ios::binary);

    T value;
    for (;;) {
        input_file.read(reinterpret_cast<char*>(&value), sizeof(T));
        if (input_file.eof())
            break;
        result.emplace_back(value);
    }

    input_file.close();
    return std::move(result);
}

template<typename T>
inline void serialize_map(std::unordered_map<std::string, T>& map, std::filesystem::path const& output_file_path)
{
    std::ofstream output_file(output_file_path, std::ios::binary);
    for (auto const& [key, value] : map) {
        output_file << key.c_str() << '\0';
        output_file.write(reinterpret_cast<const char*>(&value), sizeof(T));
    }

    output_file.close();
}

template<typename T>
inline std::unordered_map<std::string, T> deserialize_map(std::filesystem::path const& output_file_path)
{
    std::unordered_map<std::string, T> result;
    std::ifstream input_file(output_file_path, std::ios::binary);
    std::string key;
    T value;
    for (;;) {
        std::getline(input_file, key, '\0');
        input_file.read(reinterpret_cast<char*>(&value), sizeof(T));
        if (input_file.eof())
            break;
        result[key] = value;
    }

    input_file.close();
    return result;
}

#define print_shape(tensor) _print_shape(#tensor, tensor)

inline void _print_shape(const char* name, Tensor const& tensor)
{
    std::cout << name << " [";
    for (int i = 0; i < tensor.dim(); i++)
        std::cout << ' ' << tensor.size(i);
    std::cout << " ] " << tensor.dtype().name() << std::endl;
}

inline double unix_timestamp()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() / 1000.0;
}

inline std::vector<std::string> split(const std::string& s, char delimiter)
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
        tokens.push_back(token);
    return tokens;
}

inline void calculate_hash(uint8_t const* data, uint64_t size, uint64_t hash[2])
{
    static constexpr uint64_t key[4] = { 1ull, 2ull, 3ull, 4ull };
    HighwayHash128(data, size, key, hash);
}

inline std::string hash_string(void const* data, size_t size)
{
    uint64_t hash[2];
    calculate_hash((uint8_t*)data, size, hash);
    return fmt::format("{:016x}{:016x}", hash[0], hash[1]);
}

inline Path get_temp_file(std::string const& extension, void const* hash_data, size_t hash_size)
{
    auto hash = hash_string(hash_data, hash_size);
    static int counter = 0;
    return std::filesystem::temp_directory_path() / fmt::format("segment_{}.{}", hash, extension);
}

inline Tensor rotate(Tensor xyz, float roll, float pitch, float yaw)
{
    auto R_x = torch::tensor({ { 1.f, 0.f, 0.f },
                                 { 0.f, std::cos(roll), -std::sin(roll) },
                                 { 0.f, std::sin(roll), std::cos(roll) } },
        torch::dtype(torch::kFloat32));

    auto R_y = torch::tensor({ { std::cos(pitch), 0.f, std::sin(pitch) },
                                 { 0.f, 1.f, 0.f },
                                 { -std::sin(pitch), 0.f, std::cos(pitch) } },
        torch::dtype(torch::kFloat32));

    auto R_z = torch::tensor({ { std::cos(yaw), -std::sin(yaw), 0.f },
                                 { std::sin(yaw), std::cos(yaw), 0.f },
                                 { 0.f, 0.f, 1.f } },
        torch::dtype(torch::kFloat32));

    auto R = R_z.mm(R_y).mm(R_x);

    return xyz.mm(R);
}
}