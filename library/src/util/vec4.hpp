/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cmath>
#include <iostream>
#include <torch/torch.h>

namespace segment {

struct Vec4f {
    union {
        struct {
            float x, y, z, w;
        };
        float data[4];
    };

    Vec4f()
    {
        x = 0;
        y = 0;
        z = 0;
        w = 0;
    }
    Vec4f(float x0, float y0, float z0, float w0)
    {
        x = x0;
        y = y0;
        z = z0;
        w = w0;
    }

    // array type accessor
    float operator[](int i) const
    {
        return data[i];
    }

    // opperations
    float dot(const Vec4f P) const
    {
        return x * P.x + y * P.y + z * P.z + w * P.w;
    }

    float sq_norm()
    {
        return x * x + y * y + z * z + w * w;
    }

    Vec4f cross(const Vec4f P) const
    {
        return Vec4f(y * P.z - z * P.y, z * P.x - x * P.z, x * P.y - y * P.x, 0);
    }

    Vec4f& operator+=(const Vec4f& P)
    {
        x += P.x;
        y += P.y;
        z += P.z;
        w += P.w;
        return *this;
    }

    Vec4f& operator-=(const Vec4f& P)
    {
        x -= P.x;
        y -= P.y;
        z -= P.z;
        w -= P.w;
        return *this;
    }

    Vec4f& operator*=(const float& a)
    {
        x *= a;
        y *= a;
        z *= a;
        w *= a;
        return *this;
    }

    inline Vec4f floor()
    {
        return Vec4f(std::floor(x), std::floor(y), std::floor(z), std::floor(w));
    }

    Tensor to_tensor()
    {
        return torch::tensor({ x, y, z, w }, torch::dtype(torch::kFloat32));
    }
};

static_assert(sizeof(Vec4f) == 16);

inline Vec4f operator+(const Vec4f A, const Vec4f B)
{
    return Vec4f(A.x + B.x, A.y + B.y, A.z + B.z, A.w + B.w);
}

inline Vec4f operator-(const Vec4f A, const Vec4f B)
{
    return Vec4f(A.x - B.x, A.y - B.y, A.z - B.z, A.w - B.w);
}

inline Vec4f operator*(const Vec4f P, const float a)
{
    return Vec4f(P.x * a, P.y * a, P.z * a, P.w * a);
}

inline Vec4f operator*(const float a, const Vec4f P)
{
    return Vec4f(P.x * a, P.y * a, P.z * a, P.w * a);
}

inline std::ostream& operator<<(std::ostream& os, const Vec4f P)
{
    return os << "[" << P.x << ", " << P.y << ", " << P.z << ", " << P.w << "]";
}

inline bool operator==(const Vec4f A, const Vec4f B)
{
    static constexpr float eps = 1e-6f;
    return std::abs(A.x - B.x) < eps && std::abs(A.y - B.y) < eps && std::abs(A.z - B.z) < eps && std::abs(A.w - B.w) < eps;
}

}