/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cmath>
#include <iostream>
#include <torch/torch.h>

#include "util/types.hpp"

namespace segment {

struct Vec3f {
    union {
        struct {
            float x, y, z;
        };
        float data[3];
    };

    Vec3f()
    {
        x = 0;
        y = 0;
        z = 0;
    }
    Vec3f(float x0, float y0, float z0)
    {
        x = x0;
        y = y0;
        z = z0;
    }

    // array type accessor
    float operator[](int i) const
    {
        return data[i];
    }

    // opperations
    float dot(const Vec3f P) const
    {
        return x * P.x + y * P.y + z * P.z;
    }

    float sq_norm()
    {
        return x * x + y * y + z * z;
    }

    Vec3f cross(const Vec3f P) const
    {
        return Vec3f(y * P.z - z * P.y, z * P.x - x * P.z, x * P.y - y * P.x);
    }

    Vec3f& operator+=(const Vec3f& P)
    {
        x += P.x;
        y += P.y;
        z += P.z;
        return *this;
    }

    Vec3f& operator-=(const Vec3f& P)
    {
        x -= P.x;
        y -= P.y;
        z -= P.z;
        return *this;
    }

    Vec3f& operator*=(const float& a)
    {
        x *= a;
        y *= a;
        z *= a;
        return *this;
    }

    inline Vec3f floor()
    {
        return Vec3f(std::floor(x), std::floor(y), std::floor(z));
    }

    Tensor to_tensor()
    {
        return torch::tensor({ x, y, z }, torch::dtype(torch::kFloat32));
    }

    static inline std::vector<Vec3f> from_tensor(Tensor tensor)
    {
        // make sure the tensor is contiguous and on the CPU and float32
        tensor = tensor.to(torch::kCPU).contiguous();
        if (tensor.dtype() != torch::kFloat32) {
            tensor = tensor.to(torch::kFloat32);
        }
        std::vector<Vec3f> points(tensor.size(0));
        // just copy the data
        std::memcpy(points.data(), tensor.data_ptr(), sizeof(float) * tensor.numel());

        return std::move(points);
    }

    static inline Vec3f one_from_tensor(Tensor tensor)
    {
        assert(tensor.numel() == 3);
        // make sure the tensor is contiguous and on the CPU and float32
        tensor = tensor.to(torch::kCPU).contiguous();
        if (tensor.dtype() != torch::kFloat32) {
            tensor = tensor.to(torch::kFloat32);
        }
        Vec3f point;
        // just copy the data
        std::memcpy(point.data, tensor.data_ptr(), sizeof(float) * tensor.numel());

        return std::move(point);
    }
};

static_assert(sizeof(Vec3f) == 12);

inline Vec3f operator+(const Vec3f A, const Vec3f B)
{
    return Vec3f(A.x + B.x, A.y + B.y, A.z + B.z);
}

inline Vec3f operator-(const Vec3f A, const Vec3f B)
{
    return Vec3f(A.x - B.x, A.y - B.y, A.z - B.z);
}

inline Vec3f operator*(const Vec3f P, const float a)
{
    return Vec3f(P.x * a, P.y * a, P.z * a);
}

inline Vec3f operator*(const float a, const Vec3f P)
{
    return Vec3f(P.x * a, P.y * a, P.z * a);
}

inline Vec3f operator/(const Vec3f P, const float a)
{
    return Vec3f(P.x / a, P.y / a, P.z / a);
}

inline std::ostream& operator<<(std::ostream& os, const Vec3f P)
{
    return os << "[" << P.x << ", " << P.y << ", " << P.z << "]";
}

inline bool operator==(const Vec3f A, const Vec3f B)
{
    static constexpr float eps = 1e-6f;
    return std::abs(A.x - B.x) < eps && std::abs(A.y - B.y) < eps && std::abs(A.z - B.z) < eps;
}

static inline Vec3f max_point(std::vector<Vec3f> points)
{
    // Initialize limits
    Vec3f maxP(points[0]);

    // Loop over all points
    for (auto p : points) {
        if (p.x > maxP.x)
            maxP.x = p.x;

        if (p.y > maxP.y)
            maxP.y = p.y;

        if (p.z > maxP.z)
            maxP.z = p.z;
    }

    return maxP;
}

static inline Vec3f min_point(std::vector<Vec3f> const& points)
{
    // Initialize limits
    Vec3f minP(points[0]);

    // Loop over all points
    for (auto& p : points) {
        if (p.x < minP.x)
            minP.x = p.x;

        if (p.y < minP.y)
            minP.y = p.y;

        if (p.z < minP.z)
            minP.z = p.z;
    }

    return minP;
}

}