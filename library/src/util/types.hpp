/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <torch/torch.h>
#include <vector>

namespace segment {
using Tensor = torch::Tensor;
using TensorList = std::vector<Tensor>;

using Path = std::filesystem::path;
}