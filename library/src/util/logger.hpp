/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <mutex>
#include <queue>
#include <string>
#include <iostream>

#include "util/fmt.hpp"

namespace segment::util {

enum LogLevel {
    LOG_DEBUG,
    LOG_INFO,
    LOG_WARN,
    LOG_ERROR,
    LOG_CRITICAL,
};

class Logger {
    mutable std::mutex m_mutex;
    mutable std::queue<std::string> m_queue;
    mutable std::string m_current_step;
    mutable float m_progress = 0.0f;
    bool m_print_to_std_out{};

    static inline const char* log_level_to_string(LogLevel lvl)
    {
        switch (lvl) {
        case LOG_DEBUG:
            return "DEBUG";
        case LOG_INFO:
            return "INFO";
        case LOG_WARN:
            return "WARN";
        case LOG_ERROR:
            return "ERROR";
        case LOG_CRITICAL:
            return "CRITICAL";
        default:
            return "UNKNOWN";
        }
    }

    static inline std::string timestamp()
    {
        auto now = std::chrono::system_clock::now();
        auto now_c = std::chrono::system_clock::to_time_t(now);
        auto now_tm = std::localtime(&now_c);
        return fmt::format("{:02d}:{:02d}:{:02d}", now_tm->tm_hour, now_tm->tm_min, now_tm->tm_sec);
    }

public:
    void enable_stdout() { m_print_to_std_out = true;}

    void set_progress(float progress, std::string const& step) const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_progress = progress;
        m_current_step = step;
    }

    void log(LogLevel lvl, std::string const& message) const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
	auto str = fmt::format("{} [{}] {}", timestamp(), log_level_to_string(lvl), message);
        m_queue.push(str);
	if (m_print_to_std_out)
	    std::cout << str << "\n";
    }

    void error(std::string const& message) const
    {
        log(LOG_ERROR, message);
    }

    void warn(std::string const& message) const
    {
        log(LOG_WARN, message);
    }

    void info(std::string const& message) const
    {
        log(LOG_INFO, message);
    }

    void debug(std::string const& message) const
    {
#if defined(DEBUG)
        log(LOG_DEBUG, message);
#endif
    }

    void critical(std::string const& message) const
    {
        log(LOG_CRITICAL, message);
    }

    std::string fetch_log() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::string log;
        while (!m_queue.empty()) {
            log += m_queue.front() + "\n";
            m_queue.pop();
        }
        return log;
    }

    float progress() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_progress;
    }

    std::string current_step() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_current_step;
    }
};

}
