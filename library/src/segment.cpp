/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <malloc.h>
#include <segment.hpp>
#include <torch/torch.h>

#if defined(LIBTORCH_ROCM)
#    define __HIP_PLATFORM_AMD__
#    include <hip/hip_runtime.h>
#elif defined(LIBTORCH_CUDA)
#include <cuda_runtime.h>
#else
#endif

#include "kpconv/kpconv.hpp"
#include "randlanet/randlanet.hpp"

bool segment_is_gpu_available()
{
#if defined(LIBTORCH_CUDA)
    return torch::cuda::is_available();
#elif defined(LIBTORCH_ROCM)
    int device_count = 0;
    auto error = hipGetDeviceCount(&device_count);
    return error == hipSuccess && device_count > 0;
#else
    return false;
#endif
}

SegmentHandle segment_create_kpconv_network(const char* model_path)
{
    return new segment::KPConvNetwork(model_path);
}

SegmentHandle segment_create_randla_network(const char* model_path)
{
    return new segment::RandLANet(model_path);
}

void segment_free_network(SegmentHandle network)
{
    delete static_cast<segment::INetwork*>(network);
}

void segment_configure_kpconv_network(SegmentHandle network, const SegmentKPConvConfig* config)
{
    if (config && network)
        static_cast<segment::KPConvNetwork*>(network)->configure((SegmentHandle)config);
}

void segment_configure_randla_network(SegmentHandle network, const SegmentRandLANetConfig* config)
{
    if (config && network)
        static_cast<segment::RandLANet*>(network)->configure((SegmentHandle)config);
}

SegmentError segment_segment_pointcloud(SegmentHandle network, char const* input_cloud_path, char const* output_cloud_folder)
{
    if (network)
        return static_cast<segment::INetwork*>(network)->segment_pointcloud(input_cloud_path, output_cloud_folder);
    return SEGMENT_INVALID_ARGUMENT;
}

SegmentError segment_segment_points(SegmentHandle network, float const* points, uint64_t num_points, uint8_t** classifications)
{
    if (network)
        return static_cast<segment::INetwork*>(network)->segment_points(points, num_points, classifications);
    return SEGMENT_INVALID_ARGUMENT;
}

SegmentError segment_segment_points_with_rgb(SegmentHandle network, float const* points, float const* colors, uint64_t num_points, uint8_t** classifications)
{
    if (network)
        return static_cast<segment::INetwork*>(network)->segment_points(points, colors, num_points, classifications);
    return SEGMENT_INVALID_ARGUMENT;
}

SegmentError segment_initialize_network(SegmentHandle network)
{
    if (network)
        return static_cast<segment::INetwork*>(network)->initialize();
    return SEGMENT_INVALID_ARGUMENT;
}

char* segment_fetch_log(SegmentHandle network)
{
    if (network)
        return strdup(static_cast<segment::INetwork*>(network)->fetch_log().c_str());
    return nullptr;
}

char* segment_fetch_progress(SegmentHandle network, float* progress)
{
    if (network) {
        return strdup(static_cast<segment::INetwork*>(network)->fetch_progress(progress).c_str());
    }
    return nullptr;
}

void* segment_get_info_data(SegmentHandle network, SegmentInfoType type, uint64_t* out_info_size)
{
    if (network) {
        return static_cast<segment::INetwork*>(network)->get_info_data(type, out_info_size);
    }
    return nullptr;
}

uint8_t segment_is_info_data_updated(SegmentHandle network, SegmentInfoType type)
{
    if (network) {
        return static_cast<segment::INetwork*>(network)->is_info_data_updated(type);
    }
    return 0;
}

void segment_interrupt_segmentation(SegmentHandle network)
{
    if (network) {
        static_cast<segment::INetwork*>(network)->interrupt_segmentation();
    }
}

void segment_free(void* ptr)
{
    free(ptr);
}

void segment_set_torch_seed(uint64_t seed)
{
    torch::init();
    torch::manual_seed(seed);
}

void segment_get_version_info(SegmentVersionInfo* out_info)
{
    if (!out_info)
        return;

    out_info->library_version.major = SEGMENT_VERSION_MAJOR;
    out_info->library_version.minor = SEGMENT_VERSION_MINOR;
    out_info->library_version.patch = SEGMENT_VERSION_PATCH;

    out_info->torch_version.major = TORCH_VERSION_MAJOR;
    out_info->torch_version.minor = TORCH_VERSION_MINOR;
    out_info->torch_version.patch = TORCH_VERSION_PATCH;

#if defined(LIBTORCH_ROCM)
    out_info->compute_platform = SEGMENT_COMPUTE_ROCM;
    int driverVersion = 0;
    auto error = hipRuntimeGetVersion(&driverVersion);
    if (error != hipSuccess) {
        *out_info = {};
        return;
    }

    // driverVersion = hip_major * 10000000 + hip_minor * 100000 + hip_patch;

    out_info->compute_version.major = driverVersion / 10000000;
    out_info->compute_version.minor = (driverVersion / 100000) % 100;
    out_info->compute_version.patch = driverVersion % 100000;

#elif defined(LIBTORCH_CUDA)
    out_info->compute_platform = SEGMENT_COMPUTE_CUDA;
    int cudaVersion = 0;
    cudaError_t cudaStatus = cudaRuntimeGetVersion(&cudaVersion);
    if (cudaStatus == cudaSuccess) {
        out_info->compute_version.major = cudaVersion / 1000;
        out_info->compute_version.minor = (cudaVersion % 100) / 10;
    }
#else
    out_info->compute_platform = SEGMENT_COMPUTE_CPU;
    out_info->compute_version = out_info->torch_version;
#endif
}