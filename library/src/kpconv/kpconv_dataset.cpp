/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "kpconv/kpconv_dataset.hpp"
#include "kpconv/kpconv.hpp"
#include "kpconv/kpconv_batch.hpp"
#include "kpconv/kpconv_util.hpp"
#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/pointcloud.hpp"
#include "util/fmt.hpp"
#include "util/logger.hpp"
#include "util/util.hpp"

namespace segment {

KPConvDataset::KPConvDataset(Path const& original_pointcloud, KPConvConfig const& cfg, util::Logger* log)
    : m_config(cfg)
    , m_original_pointcloud_path(original_pointcloud)
    , m_temp_file_path(original_pointcloud.parent_path())
    , m_logger(log)
{
}

KPConvDataset::KPConvDataset(float const* points, uint64_t num_points, KPConvConfig const& cfg, util::Logger* log)
    : m_config(cfg)
    , m_original_pointcloud_path(util::get_temp_file("ply", points, 256))
    , m_temp_file_path(m_original_pointcloud_path.parent_path())
    , m_logger(log)
{
    m_pointcloud.points.resize(num_points);
    m_pointcloud.colors.resize(num_points);
    m_pointcloud.file_path = m_original_pointcloud_path;
    std::memcpy(m_pointcloud.points.data(), points, num_points * 3 * sizeof(float));
}

SegmentError KPConvDataset::prepare_projection()
{
    m_logger->set_progress(0.0f, "Preparing projection");
    auto proj_file = m_temp_file_path / fmt::format("{}_{}", m_pointcloud.file_path.filename().stem().u8string(), "proj.pt");

    // Try to load previous indices
    if (std::filesystem::exists(proj_file)) {
        torch::load(m_projection_indices, proj_file.u8string());
        return m_projection_indices.size(0) > 0 ? SEGMENT_OK : SEGMENT_LOADING_PROJECTION_FAILED;
    }

    auto labels = torch::zeros({ (int64_t)m_pointcloud.points.size() }, torch::dtype(torch::kInt32));

    // Compute projection inds
    m_projection_indices = torch::empty({ (int64_t)m_pointcloud.points.size() }, torch::dtype(torch::kInt32));
    auto accessor = m_projection_indices.accessor<int32_t, 1>();
    for (size_t i = 0; i < m_pointcloud.points.size(); ++i) {
        nanoflann::KNNResultSet<float> resultSet(1);
        std::vector<size_t> indices(1);
        std::vector<float> dists(1);
        resultSet.init(&indices[0], &dists[0]);
        m_downsampled_kd_tree->findNeighbors(resultSet, m_pointcloud.points[i].data);
        accessor[i] = (int32_t)indices[0];
    }

    // save indices
    torch::save(m_projection_indices, proj_file.u8string());
    m_logger->set_progress(1.0f, "Preparing projection");
    return SEGMENT_OK;
}

SegmentError KPConvDataset::prepare_pointcloud_data()
{
    m_logger->set_progress(0.0f, "Loading point cloud");
    if (m_pointcloud.points.size() == 0 && util::load_ply(m_original_pointcloud_path, m_pointcloud))
        return SEGMENT_POINTCLOUD_LOADING_FAILED;

    // downsample point cloud, or load it if it exists
    m_downsampled_cloud = util::downsample_cloud(m_pointcloud, m_temp_file_path, 0.08f, m_logger);

    if (m_downsampled_cloud.points.size() == 0)
        return SEGMENT_DOWNSAMPLING_FAILED;

    auto pot_dl = m_config.in_radius / 10.0f;
    m_coarse_cloud = util::downsample_cloud(m_downsampled_cloud, m_temp_file_path, pot_dl, m_logger);

    if (m_coarse_cloud.points.size() == 0)
        return SEGMENT_DOWNSAMPLING_FAILED;

    // Create or load kd-tree
    m_logger->set_progress(0.0f, "Creating or loading kdtree");
    m_downsampled_kd_tree = util::create_or_load_kdtree(m_downsampled_cloud, m_temp_file_path / "kdtree.bin", m_logger);
    m_coarse_kd_tree = util::create_or_load_kdtree(m_coarse_cloud, m_temp_file_path / "coarse_kdtree.bin", m_logger);

    if (!m_downsampled_kd_tree || !m_coarse_kd_tree)
        return SEGMENT_KDTREE_CREATION_FAILED;

    m_potential = torch::randn({ (int64_t)m_coarse_cloud.points.size() }) * 1e-3;
    m_argmin_potential = torch::argmin(m_potential).to(torch::dtype(torch::kInt64));
    m_min_potential = torch::min(m_potential);
    m_argmin_potential.share_memory_();
    m_min_potential.share_memory_();
    m_potential.share_memory_();

    m_downsampled_cloud_points = m_downsampled_cloud.points_to_tensor();

    return SEGMENT_OK;
}

Tensor KPConvDataset::big_neighborhood_filter(Tensor neighbors, int layer) const
{
    if (m_neighborhood_limits.size() > 0) {
        return neighbors.index({ torch::indexing::Slice(), torch::indexing::Slice(0, m_neighborhood_limits[layer]) });
    }
    return neighbors;
}

TensorList KPConvDataset::segmentation_inputs(Tensor stacked_points, Tensor stacked_features, Tensor labels, Tensor stack_lengths) const
{
    // Starting radius of convolutions
    auto r_normal = m_config.first_subsampling_dl * m_config.conv_radius;

    // Starting layer
    std::vector<std::string> layer_blocks {};

    // Lists of inputs
    TensorList input_points {};
    TensorList input_neighbors {};
    TensorList input_pools {};
    TensorList input_upsamples {};
    TensorList input_stack_lengths {};
    std::vector<bool> deform_layers {};

    // Loop over the blocks
    for (int block_i = 0; block_i < m_config.architecture.size(); ++block_i) {
        auto block = m_config.architecture[block_i];

        // Get all blocks of the layer
        if (block.find("pool") == std::string::npos && block.find("strided") == std::string::npos && block.find("global") == std::string::npos && block.find("upsample") == std::string::npos) {
            layer_blocks.push_back(block);
            continue;
        }

        // Convolution neighbors indices
        // *****************************
        Tensor conv_i {};

        bool deform_layer = false;
        // todo: make the functions use tensors
        auto const sp = Vec3f::from_tensor(stacked_points);
        auto const sl = util::from_tensor<int>(stack_lengths);

        if (layer_blocks.empty()) {
            conv_i = torch::zeros({ 0, 1 }, torch::dtype(torch::kInt64));
        } else {
            float r = 0;
            // Convolutions are done in this layer, compute the neighbors with the good radius
            if (std::any_of(layer_blocks.begin(), layer_blocks.end(), [](std::string const& block) { return block.find("deformable") != std::string::npos; })) {
                r = r_normal * m_config.deform_radius / m_config.conv_radius;
                deform_layer = true;
            } else {
                r = r_normal;
            }

            conv_i = util::batch_nanoflann_neighbors(sp, sp, sl, sl, r);
        }
        // print conv_i shape

        // Pooling neighbors indices
        // *************************

        Tensor pool_i {}, pool_p {}, pool_b {}, up_i {};
        // If end of layer is a pooling operation
        if (block.find("pool") != std::string::npos || block.find("strided") != std::string::npos) {
            // New subsampling length
            auto dl = 2 * r_normal / m_config.conv_radius;

            // Subsampled points
            std::vector<int> out_batch_inds {};

            auto result = util::batch_grid_subsampling(stacked_points, sl, dl);

            pool_p = std::get<0>(result);
            pool_b = std::get<1>(result);

            // Radius of pooled neighbors
            if (block.find("deformable") != std::string::npos) {
                r_normal = r_normal * m_config.deform_radius / m_config.conv_radius;
                deform_layer = true;
            }
            // else leave r_normal as it is

            // Subsample indices
            // todo: make the functions use tensors cause this is ugly and slow
            auto pv = Vec3f::from_tensor(pool_p);
            auto pbv = util::from_tensor<int>(pool_b);
            auto sl = util::from_tensor<int>(stack_lengths);
            pool_i = util::batch_nanoflann_neighbors(pv, sp, pbv, sl, r_normal);

            // util::print_shape("pool_i", pool_i);
            // Upsample indices (with the radius of the next layer to keep wanted density)
            // up_i = util::batch_neighbors(stacked_points, pool_p, stacked_lengths, pool_b, 2 * r_normal);
            up_i = util::batch_nanoflann_neighbors(sp, pv, sl, pbv, 2 * r_normal);

            // util::print_shape("up_i", up_i);
        } else {
            // No pooling in the end of this layer, no pooling indices required
            pool_i = torch::zeros({ 0, 1 }, torch::dtype(torch::kInt64));
            pool_p = torch::zeros({ 0, 3 }, torch::dtype(torch::kFloat32));
            pool_b = torch::zeros({ 0 }, torch::dtype(torch::kInt64));
            up_i = torch::zeros({ 0, 1 }, torch::dtype(torch::kInt64));
        }

        // reduce size of neighbors matrices by eliminating furthest point
        conv_i = big_neighborhood_filter(conv_i, input_points.size());

        pool_i = big_neighborhood_filter(pool_i, input_points.size());

        if (up_i.size(0) > 0)
            up_i = big_neighborhood_filter(up_i, input_points.size() + 1);

        // Updating input lists
        input_points.push_back(stacked_points);
        input_neighbors.push_back(conv_i);
        input_pools.push_back(pool_i);
        input_upsamples.push_back(up_i);
        input_stack_lengths.push_back(stack_lengths);
        deform_layers.push_back(deform_layer);

        // New points for next layer
        stacked_points = pool_p;
        stack_lengths = pool_b;

        // Update radius and reset blocks
        r_normal *= 2;
        layer_blocks.clear();

        // Stop when meeting a global pooling or upsampling
        if (block.find("global") != std::string::npos || block.find("upsample") != std::string::npos)
            break;
    }

    // Return inputs
    // *************

    // li = input_points + input_neighbors + input_pools + input_upsamples + input_stack_lengths
    TensorList li {};
    li.reserve(input_points.size() + input_neighbors.size() + input_pools.size() + input_upsamples.size() + input_stack_lengths.size());
    li.insert(li.end(), input_points.begin(), input_points.end());
    li.insert(li.end(), input_neighbors.begin(), input_neighbors.end());
    li.insert(li.end(), input_pools.begin(), input_pools.end());
    li.insert(li.end(), input_upsamples.begin(), input_upsamples.end());
    li.insert(li.end(), input_stack_lengths.begin(), input_stack_lengths.end());

    li.insert(li.end(), { stacked_features, labels });

    return li;
}

TensorList KPConvDataset::fetch_potential_item() const
{
    int failed_attempts = 0;
    int batch_n = 0;
    int i = 0;

    TensorList p_list {}, f_list {}, l_list {}, pi_list {}, i_list {}, ci_list {}, s_list {}, R_list {};
    while (true) {
        auto const cloud_ind = 0;
        auto const point_ind = m_argmin_potential.item<int64_t>();
        auto center_point = m_coarse_cloud.points[point_ind];
        std::vector<nanoflann::ResultItem<uint32_t, float>> pot_inds;

        // add small noise to center point
        center_point += Vec3f::one_from_tensor(torch::randn({ 3 }) * m_config.in_radius / 10);

        m_coarse_kd_tree->radiusSearch(center_point.data, m_config.in_radius * m_config.in_radius, pot_inds);

        Tensor dists = torch::empty({ (signed long)pot_inds.size() });
        Tensor inds = torch::empty({ (signed long)pot_inds.size() }, torch::dtype(torch::kInt64));
        for (size_t i = 0; i < pot_inds.size(); ++i) {
            dists[i] = pot_inds[i].second;
            inds[i] = static_cast<int64_t>(pot_inds[i].first);
        }

        auto d2s = torch::pow(dists, 2);
        auto tukeys = torch::pow(1 - d2s / (m_config.in_radius * m_config.in_radius), 2);

        tukeys.masked_fill_(d2s > (m_config.in_radius * m_config.in_radius), 0);
        m_potential.index_add_(0, inds, tukeys);

        auto min_ind = torch::argmin(m_potential);
        m_min_potential.index_copy_(0, torch::tensor({ 0 }, torch::dtype(torch::kInt64)), m_potential[min_ind]);

        m_argmin_potential.index_copy_(0, torch::tensor({ 0 }, torch::dtype(torch::kInt64)), min_ind);

        std::vector<nanoflann::ResultItem<uint32_t, float>> input_inds_and_dists;
        m_downsampled_kd_tree->radiusSearch(center_point.data, m_config.in_radius * m_config.in_radius, input_inds_and_dists);

        if (input_inds_and_dists.size() < 2) {
            std::cout << "failed\n";
            m_logger->error("Failed to find input points for potential item");
            failed_attempts++;
            if (failed_attempts > 100 * m_config.batch_num) {
                m_logger->critical("It seems this dataset only contains empty input spheres");
                return {};
            }
            continue;
        }

        Tensor input_inds = torch::empty({ (signed long)input_inds_and_dists.size() }, torch::dtype(torch::kInt64));
        for (size_t i = 0; i < input_inds_and_dists.size(); ++i)
            input_inds[i] = static_cast<int64_t>(input_inds_and_dists[i].first);

        auto input_points = m_downsampled_cloud_points.index_select(0, input_inds) - center_point.to_tensor();
        // TODO: colors
        auto input_colors = torch::zeros({ input_points.size(0), 4 }, torch::dtype(torch::kFloat32));
        // input_colors.index_fill_(0, torch::tensor({ 0 }, torch::dtype(torch::kInt64)), 1.0);
        // We don't need labels for inference
        auto input_labels = torch::zeros({ input_points.size(0) }, torch::dtype(torch::kInt64));

        auto [augmented_input_points, scale, R] = augment_points(m_config, input_points, false);

        // input_features = np.hstack((input_colors, input_points[:, 2:] + center_point[:, 2:])).astype(np.float32)
        auto ip_slice = input_points.slice(1, 2);
        auto cp_slice = torch::tensor({ 0.f, 0.f, center_point.z });
        auto height = ip_slice + cp_slice;
        // height = height[:, 2:3]
        height = height.index({ torch::indexing::Slice(), torch::indexing::Slice(2, 3) });
        auto input_features = torch::cat({ input_colors, height }, 1);

        p_list.push_back(augmented_input_points);
        f_list.push_back(input_features);
        l_list.push_back(input_labels);
        pi_list.push_back(input_inds);
        auto tmp = torch::tensor({ point_ind }, torch::dtype(torch::kInt32));
        i_list.push_back(tmp);
        ci_list.push_back(torch::tensor({ 0 }, torch::dtype(torch::kInt64)));
        s_list.push_back(scale);
        R_list.push_back(R);

        batch_n += input_inds_and_dists.size();

        if (batch_n > m_batch_limit)
            break;
    }

    auto stacked_points = torch::cat(p_list, 0);
    auto features = torch::cat(f_list, 0);
    auto labels = torch::cat(l_list, 0);
    auto point_inds = torch::cat(i_list, 0);
    auto cloud_inds = torch::cat(ci_list, 0);
    auto input_inds = torch::cat(pi_list, 0);
    auto stack_lengths = torch::empty({ (signed long)p_list.size() }, torch::dtype(torch::kInt64));

    for (size_t i = 0; i < p_list.size(); ++i)
        stack_lengths[i] = p_list[i].size(0);

    auto scales = torch::cat(s_list, 0);
    auto rots = torch::stack(R_list, 0);

    // Input features
    auto stacked_features = torch::ones({ stacked_points.size(0), 1 }, torch::dtype(torch::kFloat32));

    if (m_config.in_features_dim == 1) {
        // pass
    } else if (m_config.in_features_dim == 4) {
        stacked_features = torch::cat({ stacked_features, features.index({ torch::indexing::Slice(), torch::indexing::Slice(0, 3) }) }, 1);
    } else if (m_config.in_features_dim == 5) {
        stacked_features = torch::cat({ stacked_features, features }, 1);
    } else {
        m_logger->critical("Only accepted input dimensions are 1, 4 and 7 (without and with XYZ)");
        return {};
    }

    // Get the whole input list
    auto input_list = segmentation_inputs(stacked_points, stacked_features, labels, stack_lengths);

    // Add scale and rotation for testing
    input_list.insert(input_list.end(), { scales, rots, cloud_inds, point_inds, input_inds });
    return input_list;
}

TensorList KPConvDataset::fetch_potential_from_queue()
{
    while (true) {
        {
            std::lock_guard<std::mutex> lock(m_item_queue_mutex);
            if (!m_item_queue.empty()) {
                auto item = m_item_queue.front();
                m_item_queue.pop_front();
                return item;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if (m_dataset_error)
            return {};
    }
}

void KPConvDataset::fetch_thread_method(int thread_id)
{
    std::size_t queue_size = 0;
    while (m_data_fetch_threads_running) {
        while (true) {
            m_item_queue_mutex.lock();
            queue_size = m_item_queue.size();
            m_item_queue_mutex.unlock();
            if (queue_size < 10 || !m_data_fetch_threads_running)
                break;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        auto time = util::unix_timestamp();
        auto item = fetch_potential_item();
        if (item.size() > 0) {
            std::lock_guard<std::mutex> lock(m_item_queue_mutex);
            m_item_queue.push_back(item);
        } else {
            m_dataset_error = true;
            break;
        }
    }
}

void KPConvDataset::start_data_fetch_threads()
{
    const int num_threads = std::max(m_config.input_threads, 1);
    if (m_data_fetch_threads_running)
        return;
    m_data_fetch_threads_running = true;
    for (int i = 0; i < num_threads; ++i) {
        m_data_fetch_threads.push_back(std::thread(std::bind(&KPConvDataset::fetch_thread_method, this, i)));
    }
}

void KPConvDataset::stop_data_fetch_threads()
{
    if (!m_data_fetch_threads_running)
        return;
    m_data_fetch_threads_running = false;
    for (auto& thread : m_data_fetch_threads)
        thread.join();

    m_data_fetch_threads.clear();
}

SegmentError KPConvDataset::recalibrate(Path const& batch_lim_file, Path const& neighb_lim_file, float untouched_ratio)
{
    m_logger->set_progress(0.0f, "Recalibrating dataset");
    start_data_fetch_threads();
    const bool verbose = true;
    std::unordered_map<std::string, float> batch_lim_dict {};
    auto hist_n = static_cast<int>(std::ceil(4.0 / 3.0 * M_PI * std::pow(m_config.deform_radius + 1, 3)));
    Tensor neighb_hists = torch::zeros({ m_config.num_layers, hist_n }, torch::dtype(torch::kInt32));

    float estim_b = 0, target_b = m_config.batch_num;
    int expected_N = 10000;

    int low_pass_T = 100;
    int Kp = expected_N / 200;
    float Ki = 0.001 * Kp;
    float Kd = 5 * Kp;
    bool finer = false;
    bool stabilized = false;

    std::vector<float> smooth_errors {};
    float converge_threshold = 0.1;

    // Loop parameters
    double last_display = util::unix_timestamp();
    int i = 0;
    bool breaking = false;
    float error_I = 0;
    float error_D = 0;
    float last_error = 0;

    int sample_batches = 999;
    const int num_epochs = sample_batches / m_config.base.validation_size + 1;

    for (int epoch = 0; epoch < num_epochs && !m_dataset_error; epoch++) {
        for (int batch_step = 0; batch_step < m_config.base.validation_size && !m_dataset_error; batch_step++) {
            float progress = float(epoch * m_config.base.validation_size + batch_step) / float(num_epochs * m_config.base.validation_size);
            m_logger->set_progress(progress, "Recalibrating dataset");
            auto input_list = fetch_potential_from_queue();
            if (input_list.empty())
                break;

            KPConvBatch batch(input_list);

            Tensor hists = torch::zeros({ m_config.num_layers, hist_n }, torch::dtype(torch::kInt32));
            // Update neighborhood histogram
            for (int i = 0; i < batch.neighbors.size(); ++i) {
                auto neighb_mat = batch.neighbors[i];
                auto count = torch::sum(neighb_mat < neighb_mat.size(0), 1);

                auto hist = bincount(count, hist_n);
                hists.index_put_({ i, torch::indexing::Slice(0, hist_n) }, hist);
            }
            neighb_hists += hists.to(torch::dtype(torch::kInt32));

            // batch length
            auto const b = batch.cloud_inds.size(0);

            // Update estim_b (low pass filter)
            estim_b += (float(b) - estim_b) / low_pass_T;

            // Estimate error (noisy)
            auto error = target_b - b;
            error_I += error;
            error_D = error - last_error;
            last_error = error;

            // Save smooth errors for convergene check
            smooth_errors.push_back(target_b - estim_b);
            if (smooth_errors.size() > 30)
                smooth_errors.erase(smooth_errors.begin());

            // Update batch limit with P controller
            m_batch_limit += Kp * error + Ki * error_I + Kd * error_D;

            // Unstability detection
            if (!stabilized && m_batch_limit < 0) {
                Kp *= 0.1;
                Ki *= 0.1;
                Kd *= 0.1;
                stabilized = true;
            }

            // finer low pass filter when closing in
            if (!finer && std::abs(estim_b - target_b) < 1) {
                low_pass_T = 100;
                finer = true;
            }

            // Convergence
            auto max_it = std::max_element(smooth_errors.begin(), smooth_errors.end(),
                [](float a, float b) {
                    return std::abs(a) < std::abs(b);
                });
            assert(max_it != smooth_errors.end());
            if (finer && *max_it < converge_threshold) {
                breaking = true;
                break;
            }

            i += 1;
            auto t = util::unix_timestamp();

            if (verbose && (t - last_display) > 1.0) {
                last_display = t;
                m_logger->info(fmt::format("Step {} estim_b = {} batch_limit = {}", i, estim_b, m_batch_limit));
            }
        }
    }

    if (m_batch_limit < 0 || !breaking) {
        m_logger->critical(fmt::format("Failed to converge when calibrating, m_batch_limit: {}", m_batch_limit));
        return SEGMENT_CALIBRATION_CONVERGENCE_FAILED;
    }

    // Use collected neighbor histogram to get neighbors limit
    auto cumsum = torch::cumsum(neighb_hists.t(), 0);
    auto inner_val = cumsum < (untouched_ratio * cumsum[hist_n - 1]);
    auto percentiles = torch::sum(cumsum < (untouched_ratio * cumsum[hist_n - 1]), 0);

    m_neighborhood_limits.clear(); // todo make this a tensor
    for (int i = 0; i < percentiles.size(0); ++i)
        m_neighborhood_limits.push_back(percentiles[i].item<float>());

    // Save batch_limit dictionary
    std::string sampler_method {};
    if (m_config.use_potentials)
        sampler_method = "potentials";
    else
        sampler_method = "random";

    std::string key = fmt::format("{:s}_{:.3f}_{:.3f}_{:d}", sampler_method, m_config.in_radius, m_config.first_subsampling_dl, m_config.batch_num);

    batch_lim_dict[key] = m_batch_limit;
    util::serialize_map(batch_lim_dict, batch_lim_file);

    // Save neighb_limit dictionary
    std::unordered_map<std::string, int> neighb_lim_dict {};
    for (int layer_ind = 0; layer_ind < m_config.num_layers; ++layer_ind) {
        auto dl = m_config.first_subsampling_dl * std::pow(2, layer_ind);
        float r {};

        if (m_config.deform_layers[layer_ind])
            r = dl * m_config.deform_radius;
        else
            r = dl * m_config.conv_radius;

        auto key = fmt::format("{:.3f}_{:.3f}", dl, r);
        neighb_lim_dict[key] = m_neighborhood_limits[layer_ind];
    }

    util::serialize_map(neighb_lim_dict, neighb_lim_file);

    return SEGMENT_OK;
}

SegmentError KPConvDataset::calibrate_for_pointcloud(bool force_recalibration)
{
    m_logger->set_progress(0.0f, "Calibrating for pointcloud");
    auto stem = m_pointcloud.file_path.filename().stem().u8string();
    auto batch_limit_dict_path = m_temp_file_path / fmt::format("{}_{}", stem, "calibration_dictionary.bin");

    std::unordered_map<std::string, float> calibration_dict {};

    if (std::filesystem::exists(batch_limit_dict_path)) {
        calibration_dict = util::deserialize_map<float>(batch_limit_dict_path);
    }

    bool need_recalibration = force_recalibration;
    std::string key = "";

    if (m_config.use_potentials) {
        key = fmt::format("{:s}_{:.3f}_{:.3f}_{:d}", "potentials", m_config.in_radius, m_config.first_subsampling_dl, m_config.batch_num);
    } else {
        key = fmt::format("{:s}_{:.3f}_{:.3f}_{:d}", "random", m_config.in_radius, m_config.first_subsampling_dl, m_config.batch_num);
    }

    if (!need_recalibration && calibration_dict.find(key) != calibration_dict.end()) {
        m_batch_limit = calibration_dict[key];
    } else {
        need_recalibration = true;
    }

    auto neighb_lim_file = m_temp_file_path / fmt::format("{}_{}", stem, "neighbors_limits.bin");
    std::unordered_map<std::string, int> neighb_lim_dict {};

    if (std::filesystem::exists(neighb_lim_file)) {
        neighb_lim_dict = util::deserialize_map<int>(neighb_lim_file);
    }

    // check if the limit associated with current parameters exists (for each layer)
    std::vector<float> neighb_limits {};

    for (int layer_ind = 0; layer_ind < m_config.num_layers; ++layer_ind) {
        auto dl = m_config.first_subsampling_dl * std::pow(2, layer_ind);
        float r {};

        if (m_config.deform_layers[layer_ind])
            r = dl * m_config.deform_radius;
        else
            r = dl * m_config.conv_radius;

        auto key = fmt::format("{:.3f}_{:.3f}", dl, r);
        if (neighb_lim_dict.find(key) != neighb_lim_dict.end())
            neighb_limits.push_back(neighb_lim_dict[key]);
    }

    if (!need_recalibration && neighb_limits.size() == m_config.num_layers) {
        m_neighborhood_limits = neighb_limits;
    } else
        need_recalibration = true;

    if (need_recalibration)
        return recalibrate(batch_limit_dict_path, neighb_lim_file);
    return SEGMENT_OK;
}
}
