/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <tuple>

#include "kpconv/kpconv.hpp"
#include "kpconv/kpconv_batch.hpp"
#include "kpconv/kpconv_dataset.hpp"
#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "util/fmt.hpp"
#include "util/util.hpp"

namespace segment {

KPConvNetwork::KPConvNetwork(const char* path)
    : m_model_path(path)
{
    m_logger.info("Initializing KPConvNetwork");
}

bool KPConvNetwork::try_load_model()
{
    m_logger.set_progress(0.0f, "Loading model from disk");
    try {
        m_model = torch::jit::load(m_model_path);
    } catch (const c10::Error& e) {
        m_logger.critical(fmt::format("Error loading model from {}", m_model_path));
        return false;
    }
    m_logger.set_progress(1.0f, "Loading model from disk");
    m_loaded = true;
    return true;
}

SegmentError KPConvNetwork::initialize()
{
    if (!try_load_model())
        return SEGMENT_MODEL_LOADING_FAILED;

    m_config.init_defaults();
    return SEGMENT_OK;
}

SegmentError KPConvNetwork::segment_pointcloud(const char* input_cloud_path, const char* output_cloud_folder)
{
    if (!m_loaded)
        return SEGMENT_NO_MODEL_LOADED;

    KPConvDataset dataset(input_cloud_path, m_config, &m_logger);
    SegmentError result = dataset.prepare_pointcloud_data();

    if (result != SEGMENT_OK)
        return result;

    result = dataset.calibrate_for_pointcloud();
    if (result != SEGMENT_OK)
        return result;

    dataset.start_data_fetch_threads();
    result = dataset.prepare_projection();
    if (result != SEGMENT_OK)
        return result;

    util::Pointcloud& output_cloud = dataset.pointcloud();
    result = segment_dataset(dataset, output_cloud);
    if (result == SEGMENT_OK)
        util::save_ply(output_cloud_folder, output_cloud);
    return result;
}

SegmentError KPConvNetwork::segment_points(float const* points, uint64_t num_points, uint8_t** classifications)
{
    if (!m_loaded)
        return SEGMENT_NO_MODEL_LOADED;

    // measure time
    auto start = std::chrono::high_resolution_clock::now();
    KPConvDataset dataset(points, num_points, m_config, &m_logger);
    SegmentError result = dataset.prepare_pointcloud_data();

    m_logger.info(fmt::format("Preparation took: {}", std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start).count()));

    if (result != SEGMENT_OK)
        return result;

    start = std::chrono::high_resolution_clock::now();
    result = dataset.calibrate_for_pointcloud();
    m_logger.info(fmt::format("Calibration took: {}", std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start).count()));
    if (result != SEGMENT_OK)
        return result;

    dataset.start_data_fetch_threads();
    result = dataset.prepare_projection();
    if (result != SEGMENT_OK)
        return result;

    util::Pointcloud& output_cloud = dataset.pointcloud();

    start = std::chrono::high_resolution_clock::now();
    result = segment_dataset(dataset, output_cloud);
    m_logger.info(fmt::format("Segmentation took: {}", std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start).count()));

    if (result == SEGMENT_OK) {
        auto const& labels = output_cloud.labels;
        *classifications = (uint8_t*)malloc(labels.size() * sizeof(uint8_t));
        std::copy(labels.begin(), labels.end(), *classifications);
    }
    return result;
}

SegmentError KPConvNetwork::segment_dataset(KPConvDataset& dataset, util::Pointcloud& result)
{
    /*
        1. Load point cloud & kd trees and downsample
        2. Load calibration or recalibrate
        3. Segment point cloud
            1. iterate for the configured amount of steps
                1. iterate over all batches
                2. feed each batch to the network
                3. retrieve classifications for each point fed to the network
                4. add result to the output cloud
            2. normalize the predicted labels in the output cloud
            3. determine the label for each point in the output cloud based on the highest probability
            4. reproject the downsampled cloud with predictions to the original point cloud
            5. save the output cloud
    */

    auto const num_iterations = m_config.base.num_iterations;
    auto const test_smooth = m_config.test_smooth;
    auto const test_radius_ratio = m_config.test_radius_ratio;
    auto const nc_model = m_config.num_classes;

    auto dev = torch::Device(torch::kCPU);

#if defined(LIBTORCH_CUDA) || defined(LIBTORCH_ROCM)
    if (m_config.base.prefer_gpu_inference) {
        if (segment_is_gpu_available())
            dev = torch::Device(torch::kCUDA);
    }
#endif

    auto softmax = torch::nn::Softmax(1);
    auto test_probs = torch::zeros({ (int64_t)dataset.pointcloud().points.size(), nc_model });
    auto probs = Tensor();
    bool interrupted = false;

    m_model.to(dev);

    m_model.eval();
    torch::NoGradGuard no_grad;
    torch::jit::getProfilingMode() = false;

    auto handle_live_data = [&](int batch_index, KPConvBatch const& batch) {
        m_info_mutex.lock();

        if (m_interrupt_segmentation)
            interrupted = true;

        auto points = dataset.downsampled_points().index({ batch.input_inds });
        m_current_points = Vec3f::from_tensor(points);
        m_current_points_updated = true;
        m_info_mutex.unlock();

        if (batch_index % 10 == 0) {
            m_logger.info("Updating predictions for preview...");
            auto label_values_tensor = torch::from_blob((void*)dataset.label_values().data(), { (int64_t)dataset.label_values().size() }, torch::kInt32);
            auto _a = torch::argmax(probs, 1).to(torch::kCPU).detach().to(torch::kInt32); // find class with highest probability
            auto preds = label_values_tensor.index({ _a }).to(torch::kUInt8);
            auto* data_ptr = preds.data_ptr<uint8_t>();
            auto _classifications = std::vector<uint8_t>(data_ptr, data_ptr + preds.numel());

            m_info_mutex.lock();
            m_current_classifications = std::move(_classifications);
            m_current_classifications_updated = true;
            m_info_mutex.unlock();
        }
    };

    for (int i = 0; i < num_iterations && !interrupted; i++) {
        for (int batch_index = 0; batch_index < m_config.base.validation_size && !interrupted; batch_index++) {
            auto input_list = dataset.fetch_potential_from_queue();

            // can happen if the dataset is empty or some other error occurred
            if (input_list.empty()) {
                interrupted = true;
                break;
            }

            m_logger.info(fmt::format("Iteration: {}/{} batch {}/{}", i, num_iterations, batch_index, m_config.base.validation_size));

            float progress = float(i * m_config.base.validation_size + batch_index) / float(num_iterations * m_config.base.validation_size);
            m_logger.set_progress(progress, "Segmentation");

            auto batch = KPConvBatch(input_list);

            batch.to(dev);
            if (dev.is_cuda())
                torch::cuda::synchronize(0);
            c10::Dict<std::string, TensorList> inputs_dict;

            inputs_dict.insert("points", TensorList { batch.points });
            inputs_dict.insert("features", TensorList { batch.features });
            inputs_dict.insert("pools", TensorList { batch.pools });
            inputs_dict.insert("neighbors", TensorList { batch.neighbors });
            inputs_dict.insert("upsamples", TensorList { batch.upsamples });

            auto output = m_model.forward({ inputs_dict }).toTensor();

            auto stacked_probs = softmax(output).to(torch::kCPU).detach();
            auto s_points = batch.points[0].to(torch::kCPU).detach();
            auto lengths = batch.lengths[0].to(torch::kCPU).detach();
            auto in_inds = batch.input_inds.to(torch::kCPU).detach();
            auto cloud_inds = batch.cloud_inds.to(torch::kCPU).detach();

            if (dev.is_cuda())
                torch::cuda::synchronize(0);

            int i0 = 0;
            for (int b_i = 0; b_i < lengths.size(0); b_i++) {
                auto length = lengths[b_i].item<int>();
                auto points = s_points.index({ torch::indexing::Slice(i0, i0 + length) });
                auto probs = stacked_probs.index({ torch::indexing::Slice(i0, i0 + length) });
                auto inds = in_inds.index({ torch::indexing::Slice(i0, i0 + length) });

                auto c_i = cloud_inds[b_i].item<int>();

                if (0 < test_radius_ratio && test_radius_ratio < 1) {
                    auto mask = torch::sum(points.pow(2), 1) < (test_radius_ratio * m_config.in_radius) * (test_radius_ratio * m_config.in_radius);
                    inds = inds.index({ mask });
                    probs = probs.index({ mask });
                }

                auto indexed_probs = test_probs.index({ inds });

                test_probs.index_put_({ inds }, test_smooth * indexed_probs + (1 - test_smooth) * probs);
                i0 += length;
            }

            probs = test_probs.index({ dataset.projection_indices(), torch::indexing::Slice() });

            // Insert false columns for ignored labels
            auto const& lbl_vals = dataset.label_values();
            auto const& ign_lbls = dataset.ignored_labels();
            for (int l_ind = 0; l_ind < lbl_vals.size(); l_ind++) {
                auto label_value = dataset.label_values()[l_ind];
                // (N, 8) -> (N, 9)
                if (std::find(ign_lbls.begin(), ign_lbls.end(), label_value) != ign_lbls.end()) {
                    auto new_col = torch::zeros({ (int64_t)probs.size(0), 1 }, torch::kFloat32);
                    auto t1 = probs.slice(1, 0, l_ind);
                    auto t2 = probs.slice(1, l_ind);

                    probs = torch::cat({ t1, new_col, t2 }, 1);
                }
            }

            if (m_config.base.enable_live_data)
                handle_live_data(batch_index, batch);
        }

        m_logger.info(fmt::format("Nonzero probs: {} / max val {}", torch::nonzero(probs).size(0), torch::max(probs).item<float>()));
    }

    if (interrupted) {
        m_interrupt_segmentation = false;
        result.labels.clear();
        return SEGMENT_INTERRUPTED;
    }

    auto label_values_tensor = torch::from_blob((void*)dataset.label_values().data(), { (int64_t)dataset.label_values().size() }, torch::kInt32);
    auto _a = torch::argmax(probs, 1).to(torch::kCPU).detach().to(torch::kInt32); // find class with highest probability
    auto preds = label_values_tensor.index({ _a });

    // Save the output cloud with the predicted labels
    result.labels = util::from_tensor<uint8_t>(preds);
    return SEGMENT_OK;
}
}
