/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "util/types.hpp"

namespace segment {

struct KPConvBatch {
    TensorList points {}, neighbors {}, pools {}, upsamples {}, lengths {};
    Tensor features {}, labels {}, scales {}, rots {}, cloud_inds {}, center_inds {}, input_inds {};

    KPConvBatch() = default;

    KPConvBatch(TensorList input_list);

    void print_sizes() const;

    void to(torch::Device device);
};
}