/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <torch/torch.h>

#include "kpconv/kpconv_config.hpp"
#include "util/types.hpp"

namespace segment {

inline std::tuple<Tensor, Tensor, Tensor> augment_points(segment::KPConvConfig const& cfg, Tensor points, bool vertical)
{
    // initialize rotation matrix
    auto R = torch::eye(3);

    // FIXME: for some reason applying the rotation messes up the classification
    // if (points.size(1) == 3) {
    //     if (vertical) {
    //         // create random rotations
    //         auto theta = torch::rand(1).item().toFloat() * 2 * M_PI;
    //         auto c = std::cos(theta);
    //         auto s = std::sin(theta);
    //         R = torch::tensor({ { c, -s, 0 }, { s, c, 0 }, { 0, 0, 1 } }, torch::dtype(torch::kFloat32));
    //     } else {
    //         // choose two random angles for the first vector in polar coordinates
    //         auto theta = torch::rand(1).item().toFloat() * 2 * M_PI;
    //         auto phi = (torch::rand(1).item().toFloat() - 0.5) * M_PI;

    //         // create the first vector in carthesian coordinates
    //         auto u = torch::tensor({ { std::cos(theta) * std::cos(phi), std::sin(theta) * std::cos(phi), std::sin(phi) } }, torch::dtype(torch::kFloat32));

    //         // choose a random rotation angle
    //         auto alpha = torch::rand(1) * 2 * M_PI;

    //         // create the rotation matrix with this vector and angle
    //         R = segment::util::create_3d_rotations(u, alpha)[0]; // TODO: check if the [0] is correct
    //     }
    // }

    // R = R.to(torch::kFloat32);

    // choose random scales for each example
    auto const min_s = cfg.augment_scale_min;
    auto const max_s = cfg.augment_scale_max;
    Tensor scale {};
    if (cfg.augment_scale_anisotropic) {
        scale = torch::rand({ points.size(1) }) * (max_s - min_s) + min_s;
    } else {
        scale = torch::rand(1) * (max_s - min_s) + min_s;
    }

    // add random symmetries to the scale factor
    auto symmetries = cfg.augment_symmetries.clone();

    symmetries *= torch::randint(2, symmetries.sizes(), torch::dtype(torch::kInt64));
    scale = (scale * (1 - symmetries * 2)).to(torch::dtype(torch::kFloat32));

    auto noise = torch::randn({ points.size(0), points.size(1) }) * cfg.augment_noise;

    // apply the scale and rotation

    auto augmented_points = torch::sum(points.unsqueeze(2) * R, 1) * scale + noise;

    return { augmented_points, scale, R };
}

inline Tensor bincount(Tensor x, int64_t minlength)
{
    auto max_value = x.max().item<int64_t>();
    auto size = std::max(max_value + 1, minlength);
    auto output = torch::zeros({ size }, torch::dtype(torch::kInt32));
    for (int i = 0; i < x.size(0); i++) {
        output[x[i].item<int64_t>()] += 1;
    }
    return output;
}

}