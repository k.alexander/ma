/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <mutex>
#include <segment.hpp>
#include <torch/script.h>
#include <torch/torch.h>

#include "kpconv/kpconv_config.hpp"
#include "util/logger.hpp"
#include "util/vec3.hpp"

namespace segment {

class KPConvDataset;

namespace util {
struct Pointcloud;
}

class KPConvNetwork : public INetwork {
    torch::jit::Module m_model;
    std::string m_model_path {};
    bool m_loaded {};
    bool m_calibrated {};
    KPConvConfig m_config {};
    util::Logger m_logger {};

    bool try_load_model();

    SegmentError segment_dataset(KPConvDataset& dataset, util::Pointcloud& result);

    mutable std::mutex m_info_mutex {};
    std::vector<Vec3f> m_current_points {};
    std::vector<uint8_t> m_current_classifications {};
    mutable bool m_current_points_updated {}, m_current_classifications_updated {};

    bool m_interrupt_segmentation {};

public:
    KPConvNetwork(char const* model_path);

    SegmentError segment_pointcloud(char const* filename, char const* output) override;
    SegmentError segment_points(const float* points, uint64_t num_points, uint8_t** classifications) override;

    SegmentError initialize() override;

    void configure(SegmentHandle const config = nullptr) override
    {
        if (config)
            memcpy(&m_config, config, sizeof(SegmentKPConvConfig)); // m_config = *static_cast<KPConvConfig const*>(config);
        else
            m_config.init_defaults();
    }

    std::string fetch_log() const override
    {
        return m_logger.fetch_log();
    }

    std::string fetch_progress(float* progress) const override
    {
        if (progress)
            *progress = m_logger.progress();
        return m_logger.current_step();
    }

    void* get_info_data(SegmentInfoType type, uint64_t* out_info_size) const override
    {
        switch (type) {
        case SEGMENT_INFO_CURRENT_POINTS: {
            std::lock_guard<std::mutex> lock(m_info_mutex);
            *out_info_size = m_current_points.size() * sizeof(Vec3f);
            void* result = malloc(*out_info_size);
            memcpy(result, m_current_points.data(), *out_info_size);
            m_current_points_updated = false;
            return result;
        }
        case SEGMENT_INFO_CURRENT_PROBS: {
            std::lock_guard<std::mutex> lock(m_info_mutex);
            *out_info_size = m_current_classifications.size() * sizeof(uint8_t);
            void* result = malloc(*out_info_size);
            memcpy(result, m_current_classifications.data(), *out_info_size);
            m_current_classifications_updated = false;
            return result;
        }
        default:
            *out_info_size = 0;
            return nullptr;
        }
    }

    bool
    is_info_data_updated(SegmentInfoType type) const override
    {
        std::lock_guard<std::mutex> lock(m_info_mutex);
        switch (type) {
        case SEGMENT_INFO_CURRENT_POINTS:
            return m_current_points_updated;
        case SEGMENT_INFO_CURRENT_PROBS:
            return m_current_classifications_updated;
        default:
            return false;
        }
    }

    void interrupt_segmentation() override
    {
        m_logger.warn("Interrupting segmentation...");
        std::lock_guard<std::mutex> lock(m_info_mutex);
        m_interrupt_segmentation = true;
    }
};
}