/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <segment.hpp>

#include "util/types.hpp"

namespace segment {

struct KPConvConfig : public SegmentKPConvConfig {
    KPConvConfig() { init_defaults(); }
    int num_kernel_points {};
    int in_points_dim {};
    int batch_num {};
    std::string fixed_kernel_points {};
    std::string KP_influence {};
    std::string aggregation_mode {};
    std::string deform_fitting_mode {};
    bool use_potentials {};
    bool use_batch_norm {};
    bool deformable {};
    bool modulated {};

    bool augment_scale_anisotropic {};
    Tensor augment_symmetries {};
    float augment_noise {};

    std::vector<bool> deform_layers {};

    float KP_extent {};
    float conv_radius {};
    float in_radius {};
    float batch_norm_momentum {};
    float deform_lr_factor {};
    float segmentation_ratio {};
    float deform_radius {};
    float augment_scale_min {};
    float augment_scale_max {};

    float repulse_extent {};
    float deform_fitting_power {};
    float first_subsampling_dl {};
    int in_features_dim {};
    int first_features_dim {};
    int num_classes {};
    int input_threads {};
    int num_layers {};
    int n_frames {};
    int max_in_points {};
    int max_val_points {};
    float val_radius {};

    Tensor class_w {};

    std::vector<std::string> architecture {};

 
    SegmentError load(Path const& path);

    void init_defaults();
};

}