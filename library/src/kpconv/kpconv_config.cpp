/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "kpconv/kpconv_config.hpp"
#include "util/util.hpp"
#include <ini.h>

namespace segment {

SegmentError KPConvConfig::load(Path const& path)
{

    auto* config = ini_load(path.u8string().c_str());

    auto get_str = [config](auto section, auto id) {
        const char* _str {};
        ini_sget(config, section, id, "%s", &_str);
        return _str;
    };

    if (!config) {
        init_defaults();
        return SEGMENT_FILE_NOT_FOUND;
    }

    ini_sget(config, "basic", "num_classes", "%i", &num_classes);
    ini_sget(config, "basic", "in_points_dim", "%i", &in_points_dim);
    ini_sget(config, "basic", "in_features_dim", "%i", &in_features_dim);
    ini_sget(config, "basic", "in_radius", "%f", &in_radius);
    ini_sget(config, "basic", "input_threads", "%i", &input_threads);
    ini_sget(config, "basic", "batch_num", "%i", &batch_num);
    ini_sget(config, "basic", "use_potentials", "%i", &use_potentials);

    ini_sget(config, "augmentation", "augment_scale_min", "%f", &augment_scale_min);
    ini_sget(config, "augmentation", "augment_scale_max", "%f", &augment_scale_max);
    ini_sget(config, "augmentation", "augment_scale_anisotropic", "%i", &augment_scale_anisotropic);

    const char* symmetries {};
    ini_sget(config, "augmentation", "augment_symmetries", "%s", &symmetries);
    ini_sget(config, "augmentation", "augment_noise", "%f", &augment_noise);

    if (symmetries) {
        auto sym = std::vector<int> {};
        // split the string by comma
        auto splits = util::split(symmetries, ',');
        for (auto const& split : splits) {
            sym.push_back(std::stoi(split));
        }

        augment_symmetries = torch::tensor(sym, torch::dtype(torch::kInt64));
    }

    const char* architecture_str {};
    ini_sget(config, "architecture", "architecture", "%s", &architecture_str);
    if (architecture_str) {
        architecture.clear();
        auto arch = std::vector<std::string> {};
        auto splits = util::split(architecture_str, ',');
        for (auto const& split : splits) {
            architecture.push_back(split);
        }
    }

    const char* deform_layers_str {};
    ini_sget(config, "architecture", "deform_layers", "%s", &deform_layers_str);
    if (deform_layers_str) {
        deform_layers.clear();
        auto splits = util::split(deform_layers_str, ',');
        for (auto const& split : splits) {
            deform_layers.push_back(split == "true" || split == "1");
        }
    }

    ini_sget(config, "architecture", "num_layers", "%i", &num_layers);
    ini_sget(config, "architecture", "first_features_dim", "%i", &first_features_dim);
    ini_sget(config, "architecture", "use_batch_norm", "%i", &use_batch_norm);
    ini_sget(config, "architecture", "batch_norm_momentum", "%f", &batch_norm_momentum);
    ini_sget(config, "architecture", "num_kernel_points", "%i", &num_kernel_points);

    ini_sget(config, "misc", "segmentation_ratio", "%f", &segmentation_ratio);
    ini_sget(config, "misc", "deform_radius", "%f", &deform_radius);
    ini_sget(config, "misc", "first_subsampling_dl", "%f", &first_subsampling_dl);
    ini_sget(config, "misc", "conv_radius", "%f", &conv_radius);
    ini_sget(config, "misc", "KP_extent", "%f", &KP_extent);
    ini_sget(config, "misc", "modulated", "%i", &modulated);
    ini_sget(config, "misc", "n_frames", "%i", &n_frames);
    ini_sget(config, "misc", "max_in_points", "%i", &max_in_points);
    ini_sget(config, "misc", "max_val_points", "%i", &max_val_points);
    ini_sget(config, "misc", "val_radius", "%f", &val_radius);
    ini_sget(config, "misc", "validation_size", "%i", &base.validation_size);
    fixed_kernel_points = get_str("misc", "fixed_kernel_points");
    KP_influence = get_str("misc", "KP_influence");
    aggregation_mode = get_str("misc", "aggregation_mode");

    ini_free(config);
    return SEGMENT_OK;
}

void KPConvConfig::init_defaults()
{
    num_classes = 8;
    in_points_dim = 3;
    in_features_dim = 4;
    in_radius = 3.0;
    input_threads = 16;
    batch_num = 4;
    use_potentials = true;

    augment_scale_min = 0.9;
    augment_scale_max = 1.1;
    augment_scale_anisotropic = true;
    augment_symmetries = torch::tensor({ 1, 0, 0 }, torch::dtype(torch::kInt64));
    augment_noise = 0.001f;

    architecture = {
        "simple", "resnetb", "resnetb_strided", "resnetb", "resnetb", "resnetb_strided", "resnetb", "resnetb", "resnetb_strided", "resnetb", "resnetb", "resnetb_strided", "resnetb", "resnetb", "nearest_upsample", "unary", "nearest_upsample", "unary", "nearest_upsample", "unary", "nearest_upsample", "unary"
    };

    num_layers = 5;
    first_features_dim = 128;
    use_batch_norm = true;
    batch_norm_momentum = 0.02;

    segmentation_ratio = 1.0f;

    deform_layers = { false, false, false, false, false };

    first_subsampling_dl = 0.08;
    num_kernel_points = 15;
    conv_radius = 2.5;
    deform_radius = 5.0;
    fixed_kernel_points = "center";
    KP_extent = 1.0;
    KP_influence = "linear";
    aggregation_mode = "closest";
    modulated = false;
    n_frames = 1;
    max_in_points = 0;
    max_val_points = 50000;
    val_radius = 51.0;

    // These values can be configured by the user via `configure`
    base.validation_size = 50;
    base.num_iterations = 10;
    base.prefer_gpu_inference = true;
    base.inference_device = 0;
    base.enable_live_data = false;
    base.num_fetch_threads = std::thread::hardware_concurrency();

    test_smooth = 0.95;
    test_radius_ratio = 0.7;
    expected_N = 100000;
    Kp = expected_N / 200;
    Kd = Kp * 5;
    low_pass_T = 100;
    sample_batches = 999;
}
}