/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "kpconv/kpconv_batch.hpp"
#include "util/util.hpp"

namespace segment {

KPConvBatch::KPConvBatch(TensorList input_list)
{
    auto const L = (input_list.size() - 7) / 5;
    auto ind = 0;

    for (auto i = 0; i < L; ++i)
        points.push_back(input_list[ind++]);

    for (auto i = 0; i < L; ++i)
        neighbors.push_back(input_list[ind++]);

    for (auto i = 0; i < L; ++i)
        pools.push_back(input_list[ind++]);

    for (auto i = 0; i < L; ++i)
        upsamples.push_back(input_list[ind++]);

    for (auto i = 0; i < L; ++i)
        lengths.push_back(input_list[ind++]);

    features = input_list[ind++];
    labels = input_list[ind++];
    scales = input_list[ind++];
    rots = input_list[ind++];
    cloud_inds = input_list[ind++];
    center_inds = input_list[ind++];
    input_inds = input_list[ind++];
}

void KPConvBatch::print_sizes() const
{
    std::cout << "Points:\n";
    for (auto const& p : points)
        util::print_shape(p);
    std::cout << "Neighbors:\n";
    for (auto const& p : neighbors)
        util::print_shape(p);
    std::cout << "Pools:\n";
    for (auto const& p : pools)
        util::print_shape(p);
    std::cout << "Upsamples:\n";
    for (auto const& p : upsamples)
        util::print_shape(p);

    util::print_shape(features);
    util::print_shape(labels);
    util::print_shape(scales);
    util::print_shape(rots);
    util::print_shape(cloud_inds);
    util::print_shape(center_inds);
    util::print_shape(input_inds);
}

void KPConvBatch::to(torch::Device device)
{
    for (auto& p : points)
        p.to(device);
    for (auto& p : neighbors)
        p.to(device);
    for (auto& p : pools)
        p.to(device);
    for (auto& p : upsamples)
        p.to(device);
    for (auto& p : lengths)
        p.to(device);

    features.to(device);
    labels.to(device);
    scales.to(device);
    rots.to(device);
    cloud_inds.to(device);
    center_inds.to(device);
    input_inds.to(device);
}

} // namespace segment