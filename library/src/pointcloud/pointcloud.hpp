/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <nanoflann.hpp>

#include "util/logger.hpp"
#include "util/types.hpp"
#include "util/vec3.hpp"
#include "util/vec4.hpp"

namespace segment::util {
struct Pointcloud;

typedef nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<float, Pointcloud>,
    Pointcloud,
    3 /* dim */>
    PointcloudKDTree;

struct Pointcloud {
    Path file_path;
    std::vector<Vec3f> points;
    std::vector<Vec3f> colors;
    std::vector<uint8_t> labels;

    inline size_t kdtree_get_point_count() const { return points.size(); }

    inline float kdtree_get_pt(const size_t idx, int dim) const
    {
        return points[idx].data[dim];
    }

    template<class BBOX>
    bool kdtree_get_bbox(BBOX&) const { return false; }

    Tensor points_to_tensor(bool copy = false) const
    {
        auto result = torch::from_blob((void*)points.data(), { (signed long)points.size(), 3 }, torch::dtype(torch::kFloat32));
        return copy ? result.clone() : result;
    }

    Tensor colors_to_tensor(bool copy = false) const
    {
        auto result = torch::from_blob((void*)colors.data(), { (signed long)colors.size(), 3 }, torch::dtype(torch::kFloat32));
        return copy ? result.clone() : result;
    }
};

Pointcloud downsample_cloud(Pointcloud const& cloud, Path const& output_folder, float voxel_size, util::Logger* logger = nullptr);

std::shared_ptr<PointcloudKDTree> create_or_load_kdtree(Pointcloud const& ply_file, std::filesystem::path const& kd_tree_bin_path, util::Logger* logger = nullptr, int max_leaf = 10);

}