/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <algorithm>
#include <cstdint>
#include <unordered_map>
#include <vector>

#include "util/logger.hpp"
#include "util/types.hpp"
#include "util/vec3.hpp"
namespace segment::util {

using namespace std;

class SampledData {
public:
    int count {};
    Vec3f point {};
    vector<float> features;
    vector<unordered_map<int, int>> labels;

    SampledData() = default;

    SampledData(const size_t fdim, const size_t ldim = 0)
    {
        count = 0;
        point = Vec3f();
        features = vector<float>(fdim);
        labels = vector<unordered_map<int, int>>(ldim);
    }

    // Method Update
    void update_all(const Vec3f p, vector<float>::iterator f_begin, vector<int>::iterator l_begin)
    {
        count += 1;
        point += p;
        transform(features.begin(), features.end(), f_begin, features.begin(), plus<float>());
        int i = 0;
        for (std::vector<int>::iterator it = l_begin; it != l_begin + labels.size(); ++it) {
            labels[i][*it] += 1;
            i++;
        }
        return;
    }

    void update_features(const Vec3f p, vector<float>::iterator f_begin)
    {
        count += 1;
        point += p;
        transform(features.begin(), features.end(), f_begin, features.begin(), plus<float>());
        return;
    }

    void update_classes(const Vec3f p, vector<int>::iterator l_begin)
    {
        count += 1;
        point += p;
        int i = 0;
        for (vector<int>::iterator it = l_begin; it != l_begin + labels.size(); ++it) {
            labels[i][*it] += 1;
            i++;
        }
        return;
    }

    void update_points(const Vec3f p)
    {
        count += 1;
        point += p;
        return;
    }
};

void grid_subsampling(vector<Vec3f> const& original_points,
    vector<Vec3f>& subsampled_points,
    vector<float>& original_features,
    vector<float>& subsampled_features,
    float sampleDl, Logger* logger = nullptr);

void batch_subsample(vector<Vec3f> const& original_points,
    vector<Vec3f>& subsampled_points,
    vector<float> const& original_features,
    vector<float>& subsampled_features,
    vector<int> const& original_batches,
    vector<int>& subsampled_batches,
    float sampleDl,
    int max_p);

/**
 * @brief Create a rotation matrix from a list of axes and angles.
 * @param axis float32[N, 3]
 * @param angle float32[N,]
 * @return float32[N, 3, 3]
 */
Tensor create_3d_rotations(Tensor axis, Tensor angle);

/**
 * @brief Batch grid subsampling (method = barycenter for points and features)
 *
 * @param points (N, 3) matrix of input points
 * @param batches_len
 */
std::tuple<Tensor, Tensor> batch_grid_subsampling(Tensor points, vector<int> const& batches_len, float dl = 0.16, Tensor theta = {}, Tensor phi = {}, Tensor alpha = {});

Tensor create_rotation_from_angles(int B, Tensor theta, Tensor phi, Tensor alpha);

extern Tensor batch_nanoflann_neighbors(vector<Vec3f> const& queries,
    vector<Vec3f> const& supports,
    vector<int> const& q_batches,
    vector<int> const& s_batches,
    float radius);

}