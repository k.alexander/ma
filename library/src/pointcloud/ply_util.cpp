/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define TINYPLY_IMPLEMENTATION
#include <filesystem>
#include <fstream>
#include <happly.h>

#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "util/fmt.hpp"
#include "util/util.hpp"

namespace segment::util {

bool load_ply(Path const& path, Pointcloud& out_file, float color_scale)
{
    happly::PLYData plyIn(path.u8string());

    if (!plyIn.hasElement("vertex")) {
        return false;
    }

    std::vector<std::array<double, 3>> in_pos = plyIn.getVertexPositions();
    std::vector<std::array<uint8_t, 3>> in_col;

    if (plyIn.getElement("vertex").hasProperty("red")) {
        in_col = plyIn.getVertexColors();
        out_file.colors.resize(in_col.size());
    }

    if (plyIn.getElement("vertex").hasProperty("scalar_Label")) {
        auto lbls = plyIn.getElement("vertex").getProperty<float>("scalar_Label");
        out_file.labels.resize(lbls.size());
        for (auto i = 0; i < lbls.size(); ++i) {
            out_file.labels[i] = static_cast<uint8_t>(lbls[i]);
        }
    }

    out_file.points.resize(in_pos.size());

    Vec3f min = { std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max() };
    Vec3f max = { std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min() };

    for (auto i = 0; i < in_pos.size(); ++i) {
        out_file.points[i].data[0] = in_pos[i][0];
        out_file.points[i].data[1] = in_pos[i][1];
        out_file.points[i].data[2] = in_pos[i][2];

        for (auto j = 0; j < 3; ++j) {
            min.data[j] = std::min(min.data[j], out_file.points[i].data[j]);
            max.data[j] = std::max(max.data[j], out_file.points[i].data[j]);
        }
        if (!in_col.empty()) {
            out_file.colors[i].data[0] = in_col[i][0] * color_scale;
            out_file.colors[i].data[1] = in_col[i][1] * color_scale;
            out_file.colors[i].data[2] = in_col[i][2] * color_scale;
        }
    }

    // center the point cloud
    Vec3f center = (min + max) / 2.f;

    for (auto i = 0; i < out_file.points.size(); ++i) {
        //out_file.points[i] -= center;
    }

    out_file.file_path = path;
    return true;
}

bool save_ply(Path const& path, Pointcloud const& file, float color_scale)
{
    // Create an empty object
    happly::PLYData plyOut;

    // Add mesh data (elements are created automatically)
    std::string const vertexName = "vertex";
    size_t const N = file.points.size();

    plyOut.addElement(vertexName, N);
    auto& vertexElement = plyOut.getElement(vertexName);

    // De-interleave
    std::vector<float> xPos(N);
    std::vector<float> yPos(N);
    std::vector<float> zPos(N);
    for (size_t i = 0; i < file.points.size(); i++) {
        xPos[i] = file.points[i][0];
        yPos[i] = file.points[i][1];
        zPos[i] = file.points[i][2];
    }

    if (file.colors.size() > 0) {
        std::vector<uint8_t> r(N);
        std::vector<uint8_t> g(N);
        std::vector<uint8_t> b(N);

        for (size_t i = 0; i < file.colors.size(); i++) {
            r[i] = file.colors[i][0] * color_scale;
            g[i] = file.colors[i][1] * color_scale;
            b[i] = file.colors[i][2] * color_scale;
        }
        vertexElement.addProperty<uint8_t>("red", r);
        vertexElement.addProperty<uint8_t>("green", g);
        vertexElement.addProperty<uint8_t>("blue", b);
    }

    if (file.labels.size() > 0) {
        vertexElement.addProperty<uint8_t>("scalar_Label", file.labels);
    }
    // Store
    vertexElement.addProperty<float>("x", xPos);
    vertexElement.addProperty<float>("y", yPos);
    vertexElement.addProperty<float>("z", zPos);

    plyOut.write(path.u8string(), happly::DataFormat::Binary);

    return true;
}

bool save_tensor_as_ply(Path const& path, Tensor const& tensor)
{
    auto const points = tensor.to(torch::kCPU).contiguous().to(torch::kFloat32);
    auto const N = points.size(0);
    auto const C = points.size(1);
    auto const data = points.data_ptr<float>();

    std::vector<Vec3f> points_v(N);
    for (auto i = 0; i < N; ++i) {
        for (auto j = 0; j < C; ++j) {
            points_v[i].data[j] = data[i * C + j];
        }
    }

    Pointcloud ply_file;
    ply_file.points = points_v;
    return save_ply(path, ply_file);
}

}
