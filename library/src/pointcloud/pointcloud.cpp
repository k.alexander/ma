/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "pointcloud/pointcloud.hpp"
#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "util/fmt.hpp"
#include <fstream>

namespace segment::util {

Pointcloud downsample_cloud(Pointcloud const& cloud, Path const& output_folder, float voxel_size, util::Logger* logger)
{
    // get file name without extension
    std::string file_name = cloud.file_path.stem().string();

    auto path = output_folder / fmt::format("{}_{}.ply", file_name, voxel_size);

    // check if the file exists
    if (std::filesystem::exists(path)) {
        Pointcloud out_file {};
        if (logger)
            logger->set_progress(0.0f, "Loading downsampled cloud");
        load_ply(path, out_file, 1 / 255.f);
        return std::move(out_file);
    }

    Pointcloud out_file {};
    std::vector<float> temp_out {}; // todo the feature vector is never used
    std::vector<float> temp_in {};
    temp_in.resize(cloud.points.size() * 3);
    memcpy(temp_in.data(), cloud.colors.data(), cloud.colors.size() * sizeof(float) * 3);

    grid_subsampling(cloud.points, out_file.points, temp_in, temp_out, voxel_size);

    out_file.colors.resize(out_file.points.size());
    memcpy(out_file.colors.data(), temp_out.data(), out_file.colors.size() * sizeof(Vec3f));

    save_ply(path, out_file, 255);
    out_file.file_path = path;
    return std::move(out_file);
}

std::shared_ptr<PointcloudKDTree> create_or_load_kdtree(Pointcloud const& ply_file, Path const& kd_tree_bin_path, util::Logger* logger, int max_leaf)
{
    // check if the kdtree exists
    Path kd_tree_path(kd_tree_bin_path);
    std::shared_ptr<PointcloudKDTree> kdtree = nullptr;
    if (std::filesystem::exists(kd_tree_path)) {
        if (logger)
            logger->set_progress(0.0f, "Loading KDTree");
        std::ifstream ss(kd_tree_path, std::ios::binary);
        if (ss.fail()) {
            std::cerr << "failed to open " << kd_tree_path.string() << std::endl;
            return nullptr;
        }

        kdtree = std::make_shared<PointcloudKDTree>(3, ply_file, nanoflann::KDTreeSingleIndexAdaptorParams(max_leaf));
        kdtree->loadIndex(ss);
        return kdtree;
    }

    // create the kdtree
    if (logger)
        logger->set_progress(0.0f, "Building KDTree");
    kdtree = std::make_shared<PointcloudKDTree>(3, ply_file, nanoflann::KDTreeSingleIndexAdaptorParams(max_leaf));
    kdtree->buildIndex();

    // save the kdtree
    std::ofstream ss(kd_tree_path, std::ios::binary);
    if (ss.fail()) {
        std::cerr << "failed to open " << kd_tree_path.string() << std::endl;
        return nullptr;
    }

    kdtree->saveIndex(ss);
    return kdtree;
}

}