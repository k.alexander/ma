/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "grid_subsample.hpp"
#include "pointcloud/pointcloud.hpp"

namespace segment::util {
Tensor create_3d_rotations(Tensor axis, Tensor angle)
{
    auto t1 = torch::cos(angle);
    auto t2 = 1 - t1;
    auto t3 = axis.index({ torch::indexing::Slice(), 0 }) * axis.index({ torch::indexing::Slice(), 0 });
    auto t6 = t2 * axis.index({ torch::indexing::Slice(), 0 });
    auto t7 = t6 * axis.index({ torch::indexing::Slice(), 1 });
    auto t8 = torch::sin(angle);
    auto t9 = t8 * axis.index({ torch::indexing::Slice(), 2 });
    auto t11 = t6 * axis.index({ torch::indexing::Slice(), 2 });
    auto t12 = t8 * axis.index({ torch::indexing::Slice(), 1 });
    auto t15 = axis.index({ torch::indexing::Slice(), 1 }) * axis.index({ torch::indexing::Slice(), 1 });
    auto t19 = t2 * axis.index({ torch::indexing::Slice(), 1 }) * axis.index({ torch::indexing::Slice(), 2 });
    auto t20 = t8 * axis.index({ torch::indexing::Slice(), 0 });
    auto t24 = axis.index({ torch::indexing::Slice(), 2 }) * axis.index({ torch::indexing::Slice(), 2 });

    auto R = torch::stack({ t1 + t2 * t3,
                              t7 - t9,
                              t11 + t12,
                              t7 + t9,
                              t1 + t2 * t15,
                              t19 - t20,
                              t11 - t12,
                              t19 + t20,
                              t1 + t2 * t24 },
        1);
    return R.reshape({ -1, 3, 3 });
}

Tensor create_rotation_from_angles(int B, Tensor theta, Tensor phi, Tensor alpha)
{
    // Choose two random angles for the first vector in polar coordinates
    if (theta.numel() == 0)
        theta = torch::rand({ B }, torch::dtype(torch::kFloat64)) * 2 * M_PI;

    if (phi.numel() == 0)
        phi = (torch::rand({ B }, torch::dtype(torch::kFloat64)) - 0.5) * M_PI;

    // Create the first vector in carthesian coordinates
    Tensor u = torch::stack({ theta.cos() * phi.cos(), theta.sin() * phi.cos(), phi.sin() }, 1);

    // Choose a random rotation angle
    if (alpha.numel() == 0)
        alpha = torch::rand({ B }, torch::dtype(torch::kFloat64)) * 2 * M_PI;

    // Create the rotation matrix with this vector and angle
    return create_3d_rotations(u, alpha).to(torch::kFloat32);
}

std::tuple<Tensor, Tensor> batch_grid_subsampling(Tensor points, vector<int> const& batches_len, float dl, Tensor theta, Tensor phi, Tensor alpha)
{
    int B = batches_len.size();

    // Create the rotation matrix with this vector and angle
    auto R = create_rotation_from_angles(B, theta, phi, alpha);

    // Apply rotations
    int i0 = 0;

    points = points.clone();

    // todo: maybe use tensor accessor
    auto points_accessor = points.accessor<float, 2>();

    for (int bi = 0; bi < B; bi++) {
        auto const length = batches_len[bi];
        // points[i0:i0 + length, :] = np.sum(np.expand_dims(points[i0:i0 + length, :], 2) * R[bi], axis=1)
        auto a = points.slice(0, i0, i0 + length);
        auto b = a.unsqueeze(2);
        auto _r = R.index({ bi });
        auto c = torch::sum(b * _r, 1);

        // Apply the rotation
        points.index_put_({ torch::indexing::Slice(i0, i0 + batches_len[bi]) }, c);
        i0 += batches_len[bi];
    }

    // todo: use tensors
    std::vector<Vec3f> out_points;
    std::vector<float> out_features;
    std::vector<int> out_batches_len;
    auto vpoints = Vec3f::from_tensor(points);
    batch_subsample(vpoints, out_points, {}, out_features, batches_len, out_batches_len, dl, 0);

    // convert out_pouts to tensor
    auto s_points = torch::from_blob(out_points.data(), { (int64_t)out_points.size(), 3 }, torch::kFloat32);
    s_points = s_points.clone();
    auto s_len = torch::from_blob(out_batches_len.data(), { (int64_t)out_batches_len.size() }, torch::kInt32);
    s_len = s_len.clone();

    i0 = 0;
    for (int bi = 0; bi < B; bi++) {
        auto const length = out_batches_len[bi];
        // s_points[i0:i0 + length, :]
        auto a = s_points.slice(0, i0, i0 + length);

        // np.expand_dims(s_points[i0:i0 + length, :], 2)
        auto b = a.unsqueeze(2);

        // R[bi].T
        auto _r = R[bi].t();

        // np.sum(b * _r, axis=1)
        auto c = torch::sum(b * _r, 1);

        // s_points[i0:i0 + length, :] = c
        s_points.index_put_({ torch::indexing::Slice(i0, i0 + length) }, c);
        i0 += length;
    }

    return { s_points, s_len };
}

void grid_subsampling(vector<Vec3f> const& original_points,
    vector<Vec3f>& subsampled_points,
    vector<float>& original_features,
    vector<float>& subsampled_features,
    float sampleDl, Logger* logger)
{
    // Initialize variables
    // ******************

    // Number of points in the cloud
    size_t N = original_points.size();

    // Dimension of the features
    size_t fdim = original_features.size() / N;

    // Limits of the cloud
    Vec3f minCorner = segment::min_point(original_points);
    Vec3f maxCorner = segment::max_point(original_points);
    Vec3f originCorner = (minCorner * (1 / sampleDl)).floor() * sampleDl;

    // Dimensions of the grid
    size_t sampleNX = (size_t)floor((maxCorner.x - originCorner.x) / sampleDl) + 1;
    size_t sampleNY = (size_t)floor((maxCorner.y - originCorner.y) / sampleDl) + 1;
    // size_t sampleNZ = (size_t)floor((maxCorner.z - originCorner.z) / sampleDl) + 1;

    // Check if features and classes need to be processed
    bool use_feature = original_features.size() > 0;

    // Create the sampled map
    // **********************

    // Verbose parameters
    int i = 0;
    int nDisp = N / 100;

    // Initialize variables
    size_t iX, iY, iZ, mapIdx;
    unordered_map<size_t, SampledData> data;

    for (auto& p : original_points) {
        // Position of point in sample map
        iX = (size_t)floor((p.x - originCorner.x) / sampleDl);
        iY = (size_t)floor((p.y - originCorner.y) / sampleDl);
        iZ = (size_t)floor((p.z - originCorner.z) / sampleDl);
        mapIdx = iX + sampleNX * iY + sampleNX * sampleNY * iZ;

        // If not already created, create key
        if (data.count(mapIdx) < 1)
            data.emplace(mapIdx, SampledData(fdim));

        // Fill the sample map

        if (use_feature)
            data[mapIdx].update_features(p, original_features.begin() + i * fdim);
        else
            data[mapIdx].update_points(p);

        i++;

        if (logger) {
            float progress = (float)i / (float)N;
            if (logger->progress() - progress > 0.05) {
                logger->set_progress(progress, "Subsampling step 1");
            }
        }
    }
    i = 0;
    // Divide for barycentre and transfer to a vector
    subsampled_points.reserve(data.size());
    if (use_feature)
        subsampled_features.reserve(data.size() * fdim);

    for (auto& v : data) {
        subsampled_points.push_back(v.second.point * (1.0 / v.second.count));
        if (use_feature) {
            float count = (float)v.second.count;
            transform(v.second.features.begin(),
                v.second.features.end(),
                v.second.features.begin(),
                [count](float f) { return f / count; });
            subsampled_features.insert(subsampled_features.end(), v.second.features.begin(), v.second.features.end());
        }

        if (logger) {
            float progress = (float)i / (float)data.size();
            if (logger->progress() - progress > 0.05) {
                logger->set_progress(progress, "Subsampling step 2");
            }
        }
    }
}

void batch_subsample(vector<Vec3f> const& original_points,
    vector<Vec3f>& subsampled_points,
    vector<float> const& original_features,
    vector<float>& subsampled_features,
    vector<int> const& original_batches,
    vector<int>& subsampled_batches,
    float sampleDl,
    int max_p)
{
    // Initialize variables
    // ******************

    int b = 0;
    int sum_b = 0;

    // Number of points in the cloud
    size_t N = original_points.size();

    // Dimension of the features
    size_t fdim = original_features.size() / N;

    // Handle max_p = 0
    if (max_p < 1)
        max_p = N;

    // Loop over batches
    // *****************

    for (b = 0; b < original_batches.size(); b++) {

        // Extract batch points features and labels
        vector<Vec3f> b_o_points = vector<Vec3f>(original_points.begin() + sum_b,
            original_points.begin() + sum_b + original_batches[b]);

        vector<float> b_o_features;
        if (original_features.size() > 0) {
            b_o_features = vector<float>(original_features.begin() + sum_b * fdim,
                original_features.begin() + (sum_b + original_batches[b]) * fdim);
        }

        // Create result containers
        vector<Vec3f> b_s_points;
        vector<float> b_s_features;
        vector<int> b_s_classes;

        // Compute subsampling on current batch
        grid_subsampling(b_o_points,
            b_s_points,
            b_o_features,
            b_s_features,
            sampleDl);

        // Stack batches points features and labels
        // ****************************************

        // If too many points remove some
        if (b_s_points.size() <= max_p) {
            subsampled_points.insert(subsampled_points.end(), b_s_points.begin(), b_s_points.end());

            if (original_features.size() > 0)
                subsampled_features.insert(subsampled_features.end(), b_s_features.begin(), b_s_features.end());

            subsampled_batches.push_back(b_s_points.size());
        } else {
            subsampled_points.insert(subsampled_points.end(), b_s_points.begin(), b_s_points.begin() + max_p);

            if (original_features.size() > 0)
                subsampled_features.insert(subsampled_features.end(), b_s_features.begin(), b_s_features.begin() + max_p * fdim);

            subsampled_batches.push_back(max_p);
        }

        // Stack new batch lengths
        sum_b += original_batches[b];
    }
}

Tensor batch_nanoflann_neighbors(vector<Vec3f> const& queries,
    vector<Vec3f> const& supports,
    vector<int> const& q_batches,
    vector<int> const& s_batches,
    float radius)
{

    // Initialize variables
    // ******************

    // indices
    int i0 = 0;

    // Square radius
    float r2 = radius * radius;

    // Counting vector
    int max_count = 0;
    float d2;
    vector<vector<nanoflann::ResultItem<uint32_t, float>>> all_inds_dists(queries.size());

    // batch index
    int b = 0;
    int sum_qb = 0;
    int sum_sb = 0;

    // Nanoflann related variables
    // ***************************

    // CLoud variable
    Pointcloud current_cloud;

    // Tree parameters
    nanoflann::KDTreeSingleIndexAdaptorParams tree_params(10 /* max leaf */);

    // KDTree type definition
    typedef nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<float, Pointcloud>,
        Pointcloud,
        3>
        my_kd_tree_t;

    // Pointer to trees
    my_kd_tree_t* index;

    // Build KDTree for the first batch element
    current_cloud.points = vector<Vec3f>(supports.begin() + sum_sb, supports.begin() + sum_sb + s_batches[b]);
    index = new my_kd_tree_t(3, current_cloud, tree_params);
    index->buildIndex();

    // Search neigbors indices
    // ***********************

    // Search params
    nanoflann::SearchParameters search_params;
    search_params.sorted = true;

    for (auto& p0 : queries) {

        // Check if we changed batch
        if (i0 == sum_qb + q_batches[b]) {
            sum_qb += q_batches[b];
            sum_sb += s_batches[b];
            b++;

            // Change the points
            current_cloud.points.clear();
            current_cloud.points = vector<Vec3f>(supports.begin() + sum_sb, supports.begin() + sum_sb + s_batches[b]);

            // Build KDTree of the current element of the batch
            delete index;
            index = new my_kd_tree_t(3, current_cloud, tree_params);
            index->buildIndex();
        }

        // Initial guess of neighbors size
        all_inds_dists[i0].reserve(max_count);

        // Find neighbors

        size_t nMatches = index->radiusSearch(p0.data, r2, all_inds_dists[i0], search_params);

        // Update max count
        if (nMatches > max_count)
            max_count = nMatches;

        // Increment query idx
        i0++;
    }

    // Reserve the memory
    // out_neighbors_indices.resize(queries.size() * max_count);
    auto out_neighbors_indices = torch::empty({ (int64_t)queries.size(), (int64_t)max_count }, torch::kInt64);
    auto accessor = out_neighbors_indices.accessor<int64_t, 2>();
    i0 = 0;
    sum_sb = 0;
    sum_qb = 0;
    b = 0;
    for (auto& inds_dists : all_inds_dists) {
        // Check if we changed batch
        if (i0 == sum_qb + q_batches[b]) {
            sum_qb += q_batches[b];
            sum_sb += s_batches[b];
            b++;
        }

        for (int j = 0; j < max_count; j++) {
            if (j < inds_dists.size())
                accessor[i0][j] = (int64_t)inds_dists[j].first + sum_sb;
            else
                accessor[i0][j] = (int64_t)supports.size();
        }
        i0++;
    }

    delete index;
    return out_neighbors_indices;
}

}