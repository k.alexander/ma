/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cstdint>
#include <filesystem>
#include <memory>

#include <torch/torch.h>
#include <vector>

#include "util/types.hpp"
#include "util/vec3.hpp"
#include "util/vec4.hpp"

namespace segment::util {

struct Pointcloud;

bool load_ply(Path const& filename, Pointcloud& ply_file, float color_scale = 1.0f);

bool save_ply(Path const& path, Pointcloud const& file, float color_scale = 1.0f);

bool save_tensor_as_ply(Path const& path, Tensor const& tensor);

}
