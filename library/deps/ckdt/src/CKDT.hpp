#pragma once
#include "ckdtree_decl.h"
#include <cassert>
#include <cstring>
#include <iostream>
#include <mutex>

namespace ckdt {
struct v3 {
    union {
        struct {
            double x, y, z;
        };
        double data[3];
    };

    friend std::ostream& operator<<(std::ostream& os, const v3& v)
    {
        os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
        return os;
    }
};
static_assert(sizeof(v3) == 3 * sizeof(double),
    "v3 size is not 3 * sizeof(double)");

static inline v3 amax(v3 const* points, int n)
{
    v3 max = points[0];
    for (int i = 1; i < n; i++) {
        max.x = std::max(max.x, points[i].x);
        max.y = std::max(max.y, points[i].y);
        max.z = std::max(max.z, points[i].z);
    }
    return max;
}

static inline v3 amin(v3 const* points, int n)
{
    v3 max = points[0];
    for (int i = 1; i < n; i++) {
        max.x = std::min(max.x, points[i].x);
        max.y = std::min(max.y, points[i].y);
        max.z = std::min(max.z, points[i].z);
    }
    return max;
}

static inline ssize_t* arange(ssize_t num, ssize_t start = 0,
    ssize_t end_add = 0)
{
    ssize_t* indices = new ssize_t[num];
    for (ssize_t i = start; i < num + end_add; i++) {
        indices[i - start] = i;
    }
    return indices;
}

static inline bool isfinite(v3 const* points, int n)
{
    for (int i = 0; i < n; i++) {
        if (!std::isfinite(points[i].x) || !std::isfinite(points[i].y) || !std::isfinite(points[i].z)) {
            return false;
        }
    }
    return true;
}

struct tree_settings {
    int m_leafsize = 16;
    bool m_compact_nodes = true;
    bool m_copy_data = false;
    bool m_balanced_tree = true;

    tree_settings() = default;

    tree_settings& leafsize(int leafsize)
    {
        m_leafsize = leafsize;
        return *this;
    }

    tree_settings& compact_nodes(bool compact_nodes = true)
    {
        m_compact_nodes = compact_nodes;
        return *this;
    }

    tree_settings& copy_data(bool copy_data = true)
    {
        m_copy_data = copy_data;
        return *this;
    }

    tree_settings& balanced_tree(bool balanced_tree = true)
    {
        m_balanced_tree = balanced_tree;
        return *this;
    }
};

struct query_result {
    std::vector<double> distances;
    std::vector<ssize_t> indices;
};

class kdtree {
    ckdtree self {};
    v3 const* m_points;
    v3 m_max {};
    v3 m_min {};
    ssize_t* m_indices {};
    bool owns_data {};
    ssize_t m_last_k {};
    ssize_t* m_last_k_values {};
    std::mutex m_mutex {};

    void post_init_traverse(ckdtreenode* node)
    {
        if (node->split_dim == -1) {
            node->less = nullptr;
            node->greater = nullptr;
        } else {
            node->less = self.ctree + node->_less;
            node->greater = self.ctree + node->_greater;
            post_init_traverse(node->less);
            post_init_traverse(node->greater);
        }
    }

public:
    kdtree(double const* points, int num_points, int point_dim = 3,
        tree_settings const& settings = {})
    {
        build(points, num_points, point_dim, settings);
    }

    kdtree() = default;

    ~kdtree() { free(); }

    void build(double const* points, int num_points, int point_dim = 3,
        tree_settings const& settings = {})
    {
        free();
        assert(points != nullptr);
        assert(num_points > 0);
        assert(settings.m_leafsize > 0);
        assert(isfinite(reinterpret_cast<v3 const*>(points),
            num_points * std::min(point_dim, 1)));

        owns_data = settings.m_copy_data;
        if (settings.m_copy_data) {
            self.raw_data = new double[num_points * point_dim];
            memcpy((void*)self.raw_data, points, num_points * point_dim * sizeof(double));
        } else {
            self.raw_data = points;
        }

        m_points = reinterpret_cast<v3 const*>(self.raw_data);
        m_max = amax(m_points, num_points);
        m_min = amin(m_points, num_points);
        m_indices = arange(num_points);

        self.n = num_points;
        self.m = point_dim;
        self.leafsize = settings.m_leafsize;
        self.raw_maxes = m_max.data;
        self.raw_mins = m_min.data;
        self.raw_indices = m_indices;
        self.tree_buffer = new std::vector<ckdtreenode>();

        v3 tmp_max = m_max;
        v3 tmp_min = m_min;

        build_ckdtree(&self, 0, self.n, tmp_max.data, tmp_min.data,
            settings.m_balanced_tree, settings.m_compact_nodes);
        self.ctree = self.tree_buffer->data();
        self.size = self.tree_buffer->size();

        post_init_traverse(self.ctree);
    }

    void free()
    {
        if (owns_data) {
            delete[] self.raw_data;
            owns_data = false;
        }
        delete[] m_indices;
        delete[] m_last_k_values;
        delete self.tree_buffer;
        m_last_k = -1;
        m_indices = nullptr;
        m_last_k_values = nullptr;
        self = {};
    }

    query_result
    knn(v3* points, int n, int k = 1, double eps = 0, double p = 2,
        double distance_upper_bound = std::numeric_limits<double>::infinity())
    {
        assert(points != nullptr);
        assert(n > 0);
        assert(isfinite(points, n));
        m_mutex.lock();
        if (m_last_k != k) {
            delete[] m_last_k_values;
            m_last_k_values = arange(k, 1, 1);
            m_last_k = k;
        }

        query_result result {};
        result.distances.resize(n * k, 0);
        result.indices.resize(n * k, 0);

        query_knn(&self, result.distances.data(), result.indices.data(),
            &points[0].data[0], n, m_last_k_values, k, k, eps, p,
            distance_upper_bound);
        m_mutex.unlock();
        return std::move(result);
    }
};
} // namespace ckdt