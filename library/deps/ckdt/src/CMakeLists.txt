add_library(ckdt STATIC)

target_sources(ckdt PRIVATE
    build.cxx
    query_ball_point.cxx
    query_ball_tree.cxx
    query_pairs.cxx
    query.cxx
    sparse_distances.cxx
    count_neighbors.cxx

    ckdtree_decl.h
    coo_entries.h
    distance_base.h
    distance.h
    ordered_pair.h
    rectangle.h
)

target_include_directories(ckdt PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
)