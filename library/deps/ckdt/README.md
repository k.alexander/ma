### ckdt

[scipy](https://github.com/scipy/scipy/tree/main/scipy/spatial/ckdtree/src) ckdtrees as a standalone c++ library.

### Usage

See [example.cpp](./example/main.cpp) for a simple example or here:

```cpp
#include <CKDT.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
    ckdt::v3 points[3] = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
    ckdt::kdtree tree(&points->data[0], 3);

    ckdt::v3 query = {1, 1, 1};
    auto result = tree.knn(&query, 1, 2);

    // The first result is invalid, i.e. the query point itself, not sure if that is intended behavior
    std::cout << "Nearest neighbor for " << query << ": " << points[result.indices[1]] << " with a distance of " << result.distances[1] << std::endl;
    return 0;
}
```
