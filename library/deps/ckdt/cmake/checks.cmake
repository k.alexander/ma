include(CheckCXXSourceCompiles)

check_cxx_source_compiles("
#include <xmmintrin.h>
int main() {
    int* p = 0;
    __builtin_prefetch(p, _MM_HINT_NTA);
    return 0;
}
" HAVE_BUILTIN_PREFETCH)

if(HAVE_BUILTIN_PREFETCH)
    message(STATUS "The compiler does support __builtin_prefetch")
    add_definitions(-DHAVE___BUILTIN_PREFETCH=1)
else()
    message(STATUS "The compiler does not support __builtin_prefetch")
endif()

check_cxx_source_compiles("
#include <xmmintrin.h>
int main() {
    __m128 a;
    a = _mm_set1_ps(0);
    return 0;
}
" HAVE_SSE)

if(HAVE_SSE)
    message(STATUS "The compiler does support SSE")
    add_definitions(-DNPY_HAVE_SSE=1)
else()
    message(STATUS "The compiler does not support SSE")
endif()

check_cxx_source_compiles("
int main() {
    int a = 0;
    __builtin_expect(a, 0); // i don't know why they use __builtin_expect like that
    return 0;
}
" HAVE_BUILTIN_EXPECT)

if(HAVE_BUILTIN_EXPECT)
    message(STATUS "The compiler does support __builtin_expect")
    add_definitions(-DHAVE___BUILTIN_EXPECT=1)
else()
    message(STATUS "The compiler does not support __builtin_expect")
endif()