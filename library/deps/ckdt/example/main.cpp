#include <CKDT.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
    ckdt::v3 points[3] = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
    ckdt::kdtree tree(&points->data[0], 3);

    ckdt::v3 query = {1, 1, 1};
    auto result = tree.knn(&query, 1, 2);

    // For some reason the first result is invalid, i.e. the query point itself
    std::cout << "Nearest neighbor for " << query << ": " << points[result.indices[1]] << " with a distance of " << result.distances[1] << std::endl;
    return 0;
}