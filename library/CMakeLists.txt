add_library(segment SHARED)
add_library(lib::segment ALIAS segment)

add_subdirectory(src)
add_subdirectory(deps)

set_target_properties(segment PROPERTIES BUILD_RPATH "$ORIGIN")

set_target_properties(segment PROPERTIES BUILD_WITH_INSTALL_RPATH TRUE
                                         INSTALL_RPATH "$ORIGIN")
option(BUILD_TESTS "Build tests" ON)

include(${CMAKE_SOURCE_DIR}/cmake/torch.cmake)

if(UNIX
   AND NOT APPLE
   AND LIBTORCH_COMPUTE_PLATFORM STREQUAL "rocm")
  # target_link_libraries(
  #   segment
  #   PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/deps/torch/${LIBTORCH_COMPUTE_PLATFORM}/${TORCH_BUILD_TYPE}/libtorch/lib/libroctx64.so"
  # )
elseif(MSVC)
  target_compile_options(
    segment PRIVATE /wd4267 /wd4244) # disable some warnings the ATen library
                                     # produces
endif()


set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=mold")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fuse-ld=mold")

if (LIBTORCH_COMPUTE_PLATFORM STREQUAL "rocm")
  target_compile_definitions(segment PRIVATE LIBTORCH_ROCM)
  find_package(HIP REQUIRED)
  target_include_directories(segment PRIVATE ${HIP_INCLUDE_DIRS})
elseif (LIBTORCH_COMPUTE_PLATFORM STREQUAL "cuda")
  target_compile_definitions(segment PRIVATE LIBTORCH_CUDA)
else()
  target_compile_definitions(segment PRIVATE LIBTORCH_CPU)
endif()

# Add version as a compile definition
target_compile_definitions(segment PRIVATE SEGMENT_VERSION_MAJOR=${PROJECT_VERSION_MAJOR})
target_compile_definitions(segment PRIVATE SEGMENT_VERSION_MINOR=${PROJECT_VERSION_MINOR})
target_compile_definitions(segment PRIVATE SEGMENT_VERSION_PATCH=${PROJECT_VERSION_PATCH})

if(BUILD_TESTS)
  add_subdirectory(tests)
endif()
if (BUILD_TESTS)
  target_compile_definitions(segment PUBLIC WITH_TESTS=1)

endif()

message(STATUS "Using ${LIBTORCH_DIR}")

# find LibTorch
find_package(Torch REQUIRED)
target_link_libraries(segment PRIVATE ${TORCH_LIBRARIES})
set_property(TARGET segment PROPERTY CXX_STANDARD 17)

target_compile_definitions(segment PRIVATE SEGMENT_COMPILE_LIBRARY)

target_include_directories(
  segment
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
         ${CMAKE_CURRENT_SOURCE_DIR}/deps/nanoflann ${TORCH_INCLUDE_DIRS})

target_include_directories(
  segment
  PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
          ${CMAKE_CURRENT_SOURCE_DIR}/deps/happly
          ${CMAKE_SOURCE_DIR}/library/deps/ini/include)

target_sources(segment PRIVATE ${CMAKE_SOURCE_DIR}/library/deps/ini/src/ini.c)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${TORCH_CXX_FLAGS}")

install(DIRECTORY include/ DESTINATION include)

install(
  TARGETS segment
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES
  DESTINATION include)

if (MSVC)
  install(
    FILES $<TARGET_PDB_FILE:segment>
    DESTINATION bin
    OPTIONAL)
    if (BUILD_TESTS)
      add_custom_command(
      TARGET segment
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:segment>
            ${CMAKE_BINARY_DIR}/library/tests)
    endif()

endif()

install(FILES ${CMAKE_SOURCE_DIR}/COPYING ${CMAKE_SOURCE_DIR}/DEPENDENCIES
        DESTINATION .)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/data DESTINATION .)
install(FILES ${TORCH_DLLS} DESTINATION bin)

