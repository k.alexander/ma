/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "test.hpp"

namespace segment::tests {
void test_ply(TestReports& reports)
{
    Test test("Ply util", reports);

    segment::util::Pointcloud file;

    _T(test, segment::util::load_ply(TEST_DATA_PATH "/l001_cutout.ply", file));
    _T(test, file.points.size() == 1004490);

    auto downsampled_cloud = segment::util::downsample_cloud(file, TEST_DATA_PATH, 0.08f);
    _T(test, downsampled_cloud.points.size() <= 215678);

    _T(test, segment::util::save_ply(TEST_DATA_PATH "/test_downsampled.ply", downsampled_cloud));

    auto kd_tree = segment::util::create_or_load_kdtree(downsampled_cloud, TEST_DATA_PATH "/downsampled_kd_tree.bin");
    _T(test, kd_tree != nullptr);

    // Remove temporary files
    std::filesystem::remove(TEST_DATA_PATH "/test_downsampled.ply");
    std::filesystem::remove(downsampled_cloud.file_path);
    std::filesystem::remove(TEST_DATA_PATH "/downsampled_kd_tree.bin");
}
}