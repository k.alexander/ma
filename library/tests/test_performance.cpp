/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define ANKERL_NANOBENCH_IMPLEMENT
#include <CKDT.hpp>
#include <nanobench.h>
#include <nanoflann.hpp>

#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "test.hpp"
#include "util/vec3.hpp"

using namespace segment;

Tensor create_3d_rotations_(Tensor axis, Tensor angle)
{
    auto t1 = torch::cos(angle);
    auto t2 = 1 - t1;
    auto t3 = axis.index({ torch::indexing::Slice(), 0 }) * axis.index({ torch::indexing::Slice(), 0 });
    auto t6 = t2 * axis.index({ torch::indexing::Slice(), 0 });
    auto t7 = t6 * axis.index({ torch::indexing::Slice(), 1 });
    auto t8 = torch::sin(angle);
    auto t9 = t8 * axis.index({ torch::indexing::Slice(), 2 });
    auto t11 = t6 * axis.index({ torch::indexing::Slice(), 2 });
    auto t12 = t8 * axis.index({ torch::indexing::Slice(), 1 });
    auto t15 = axis.index({ torch::indexing::Slice(), 1 }) * axis.index({ torch::indexing::Slice(), 1 });
    auto t19 = t2 * axis.index({ torch::indexing::Slice(), 1 }) * axis.index({ torch::indexing::Slice(), 2 });
    auto t20 = t8 * axis.index({ torch::indexing::Slice(), 0 });
    auto t24 = axis.index({ torch::indexing::Slice(), 2 }) * axis.index({ torch::indexing::Slice(), 2 });

    auto R = torch::stack({ t1 + t2 * t3,
                              t7 - t9,
                              t11 + t12,
                              t7 + t9,
                              t1 + t2 * t15,
                              t19 - t20,
                              t11 - t12,
                              t19 + t20,
                              t1 + t2 * t24 },
        1);

    return R.reshape({ -1, 3, 3 });
}

Tensor create_3d_rotations_accessor(Tensor axis, Tensor angle)
{
    auto accessor = axis.accessor<float, 2>();
    auto t1 = torch::cos(angle);
    auto t2 = 1 - t1;
    auto t3 = accessor[0][0] * accessor[0][0];
    auto t6 = t2 * accessor[0][0];
    auto t7 = t6 * accessor[0][1];
    auto t8 = torch::sin(angle);
    auto t9 = t8 * accessor[0][2];
    auto t11 = t6 * accessor[0][2];
    auto t12 = t8 * accessor[0][1];
    auto t15 = accessor[0][1] * accessor[0][1];
    auto t19 = t2 * accessor[0][1] * accessor[0][2];
    auto t20 = t8 * accessor[0][0];
    auto t24 = accessor[0][2] * accessor[0][2];
    auto R = torch::stack({ t1 + t2 * t3,
                              t7 - t9,
                              t11 + t12,
                              t7 + t9,
                              t1 + t2 * t15,
                              t19 - t20,
                              t11 - t12,
                              t19 + t20,
                              t1 + t2 * t24 },
        1);

    return R.reshape({ -1, 3, 3 });
}

void test_kd_trees_building(Test& test)
{
    util::Pointcloud input;
    util::load_ply(TEST_DATA_PATH "/birmingham_block_12 - origin.ply", input);

    ankerl::nanobench::Bench bench;
    bench.minEpochIterations(5);
    bench.run("Nanoflann index building", [&] {
        auto kdtree = util::PointcloudKDTree(3, input, nanoflann::KDTreeSingleIndexAdaptorParams(10, nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex, 2));
        kdtree.buildIndex();
        ankerl::nanobench::doNotOptimizeAway(&kdtree);
    });

    std::vector<ckdt::v3> points;

    for (int i = 0; i < input.points.size(); i++) {
        auto p = input.points[i];
        points.push_back(ckdt::v3 { p.x, p.y, p.z });
    }
    ckdt::tree_settings settings {};
    settings.m_leafsize = 10;
    settings.m_compact_nodes = false;
    settings.m_balanced_tree = false;
    settings.m_copy_data = false;

    bench.run("CKDT index building", [&] {
        auto kdtree = ckdt::kdtree(points[0].data, points.size(), 3, settings);
        ankerl::nanobench::doNotOptimizeAway(kdtree);
    });
}

inline int get_random_int(int min, int max)
{
    return rand() % (max - min + 1) + min;
}

void test_kd_trees_querying(Test& test)
{
    srand(42);

    util::Pointcloud input;
    util::load_ply(TEST_DATA_PATH "/birmingham_block_12 - origin.ply", input);
    auto nf_tree = util::PointcloudKDTree(3, input, nanoflann::KDTreeSingleIndexAdaptorParams(10, nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex, 6));
    nf_tree.buildIndex();
    ankerl::nanobench::Bench bench;
    bench.minEpochIterations(907066);

    bench.run("Nanoflann knn search, k = 1", [&] {
        auto indices = std::vector<uint32_t>(6);
        auto distances = std::vector<float>(6);
        auto index = get_random_int(0, input.points.size() - 1);
        auto result = nf_tree.knnSearch(input.points[index].data, 6, indices.data(), distances.data());
        ankerl::nanobench::doNotOptimizeAway(result);
    });

    std::vector<ckdt::v3> points;

    for (int i = 0; i < input.points.size(); i++) {
        auto p = input.points[i];
        points.push_back(ckdt::v3 { p.x, p.y, p.z });
    }
    ckdt::tree_settings settings {};
    settings.m_leafsize = 8;
    settings.m_compact_nodes = true;
    settings.m_balanced_tree = false;
    settings.m_copy_data = false;
    auto ckdt_tree = ckdt::kdtree(points[0].data, points.size(), 3, settings);

    bench.run("CKDT knn search, k = 1", [&] {
        auto index = get_random_int(0, input.points.size() - 1);
        auto result = ckdt_tree.knn(&points[index], 1, 6);
        ankerl::nanobench::doNotOptimizeAway(result);
    });
}

void test_kd_trees_querying_n_40000(Test& test)
{
    srand(42);

    util::Pointcloud input;
    util::load_ply(TEST_DATA_PATH "/birmingham_block_12 - origin.ply", input);
    auto nf_tree = util::PointcloudKDTree(3, input, nanoflann::KDTreeSingleIndexAdaptorParams(10, nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex, 6));
    nf_tree.buildIndex();
    ankerl::nanobench::Bench bench;
    bench.minEpochIterations(109);

    bench.run("Nanoflann knn search, k = 40000", [&] {
        auto indices = std::vector<uint32_t>(40000);
        auto distances = std::vector<float>(40000);
        auto index = get_random_int(0, input.points.size() - 1);
        auto result = nf_tree.knnSearch(input.points[index].data, 40000, indices.data(), distances.data());
        ankerl::nanobench::doNotOptimizeAway(result);
    });

    std::vector<ckdt::v3> points;

    for (int i = 0; i < input.points.size(); i++) {
        auto p = input.points[i];
        points.push_back(ckdt::v3 { p.x, p.y, p.z });
    }
    ckdt::tree_settings settings {};
    settings.m_leafsize = 10;
    settings.m_compact_nodes = true;
    settings.m_balanced_tree = true;
    settings.m_copy_data = false;
    auto ckdt_tree = ckdt::kdtree(points[0].data, points.size(), 3, settings);

    bench.run("CKDT knn search, k = 40000", [&] {
        auto index = get_random_int(0, input.points.size() - 1);
        auto result = ckdt_tree.knn(&points[index], 1, 40000);
        ankerl::nanobench::doNotOptimizeAway(result);
    });
}

namespace segment::tests {
void test_performance(TestReports& reports)
{
    Test test("Performance tests", reports);
    const int max_count = 14000, query_size = 562;
    test_kd_trees_querying_n_40000(test);
    return;

    ankerl::nanobench::Bench neighbor_bench;
    neighbor_bench.run("Populate neighbor indices with a tensor accessor", [&] {
        auto out_neighbors_indices = torch::empty({ (int64_t)query_size, (int64_t)max_count }, torch::kInt64);
        auto tensor_accessor = out_neighbors_indices.accessor<int64_t, 2>();

        for (int j = 0; j < max_count; j++)
            tensor_accessor[0][j] = (int64_t)1;

        ankerl::nanobench::doNotOptimizeAway(out_neighbors_indices);
    });

    neighbor_bench.run("Populate neighbor indices with index_put", [&] {
        auto out_neighbors_indices = torch::empty({ (int64_t)query_size, (int64_t)max_count }, torch::kInt64);

        for (int j = 0; j < max_count; j++)
            out_neighbors_indices.index_put_({ 0, j }, (int64_t)1);

        ankerl::nanobench::doNotOptimizeAway(out_neighbors_indices);
    });

    test.section("Create 3D rotations");
    ankerl::nanobench::Bench rotation_bench;

    rotation_bench.run("Create 3D rotations with tensor accessor", [&] {
        auto axis = torch::rand({ 1, 3 });
        auto angle = torch::rand({ 1 });
        auto R = create_3d_rotations_accessor(axis, angle);
        ankerl::nanobench::doNotOptimizeAway(R);
    });

    rotation_bench.run("Create 3D rotations without accessor", [&] {
        auto axis = torch::rand({ 1, 3 });
        auto angle = torch::rand({ 1 });
        auto R = create_3d_rotations_(axis, angle);
        ankerl::nanobench::doNotOptimizeAway(R);
    });

    // todo: test performanc torch::tensor::slice
}
}