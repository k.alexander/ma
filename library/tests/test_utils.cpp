/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "test.hpp"
#include "util/types.hpp"
#include "util/util.hpp"
#include <segment.hpp>

namespace segment::tests {
void test_utils(TestReports& reports)
{
    Test test("Utility functions", reports);
    _T(test, segment::is_gpu_available() == true);

    // Convert tensor to vector
    std::vector<int> int_vector = { 1, 2, 3, 4, 5 };
    Tensor test_tensor = torch::tensor(int_vector);
    std::vector<int> int_vector_from_tensor = segment::util::from_tensor<int>(test_tensor);
    _T(test, int_vector == int_vector_from_tensor);

    std::vector<float> float_vector = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };
    test_tensor = torch::tensor(float_vector);
    std::vector<float> float_vector_from_tensor = segment::util::from_tensor<float>(test_tensor);
    _T(test, float_vector == float_vector_from_tensor);

    // Map serialization and deserialization
    std::unordered_map<std::string, int> test_int_map = { { "a", 1 }, { "b", 2 }, { "c", 3 } };

    segment::util::serialize_map<int>(test_int_map, TEST_DATA_PATH "/test_map.bin");
    auto deserialized_int_map = segment::util::deserialize_map<int>(TEST_DATA_PATH "/test_map.bin");

    _T(test, test_int_map == deserialized_int_map);

    std::unordered_map<std::string, float> test_float_map = { { "a", 1 }, { "b", 2 }, { "c", 3 } };

    segment::util::serialize_map<float>(test_float_map, TEST_DATA_PATH "/test_float_map.bin");
    auto deserialized_float_map = segment::util::deserialize_map<float>(TEST_DATA_PATH "/test_float_map.bin");

    _T(test, test_float_map == deserialized_float_map);

    // Remove temporary files
    std::filesystem::remove(TEST_DATA_PATH "/test_map.bin");
    std::filesystem::remove(TEST_DATA_PATH "/test_float_map.bin");
}
}