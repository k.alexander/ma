/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <segment.hpp>

#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "test.hpp"

namespace segment::tests {
void test_kpconv(TestReports& reports)
{
    Test test("KPConv cloud segmentation", reports);

    auto* kpconv = segment::create_kpconv_network("/run/media/office/Storage/git/KPConv-PyTorch/kpconv_jit.pt");

    _T(test, kpconv->initialize() == SEGMENT_OK);

    kpconv->configure(nullptr);

    util::Pointcloud input;
    _T(test, util::load_ply(TEST_DATA_PATH "/l001_cutout.ply", input));

    uint8_t* classifications;
    _T(test, kpconv->segment_points((float*)input.points.data(), input.points.size(), &classifications) == SEGMENT_OK);

    std::vector<uint8_t> classifications_vec(classifications, classifications + input.points.size());

    input.labels = classifications_vec;

    segment_free(classifications);

    _T(test, util::save_ply(TEST_DATA_PATH "/l001_cutout_segmented.ply", input));

    //_T(test, kpconv->segment_pointcloud(TEST_DATA_PATH "/l001_cutout.ply", TEST_DATA_PATH "/l001_cutout_segmented.ply") == SEGMENT_OK);
}
}