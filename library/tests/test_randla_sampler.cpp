/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <segment.hpp>
#include <torch/script.h>

#include "randlanet/randlanet_config.hpp"
#include "randlanet/randlanet_dataset.hpp"
#include "randlanet/randlanet_sampler.hpp"
#include "test.hpp"
#include "util/logger.hpp"

namespace segment::tests {

void test_randla_sampler2(TestReports& reports)
{
    Test test("RandLA Sampler", reports);
    torch::jit::script::Module sampler_data = torch::jit::load(TEST_DATA_PATH "/randla_sampler/py_sampler_input.pt");

    Tensor possibility = sampler_data.attr("possibility").toTensor().to(torch::kFloat32);
    Tensor center_points = sampler_data.attr("pick_points").toTensor().to(torch::kFloat32);
    Tensor indices = sampler_data.attr("pick_idx").toTensor().to(torch::kInt64);
    Tensor noise = sampler_data.attr("noise").toTensor().to(torch::kInt64);

    center_points -= noise;

    util::Logger logger {};
    RandLANetConfig config {};
    segment::Path path = TEST_DATA_PATH "/birmingham_block_12_py_ss.ply";
    config.init_defaults();
    RandLANetDataset dataset(path, config, &logger);

    dataset.prepare_pointcloud_data();

    RandLANetSampler sampler(&dataset);
    sampler.set_possibility(possibility);

    bool failure = false;
    for (int i = 0; i < center_points.size(0) && !failure; i++) {
        auto point = sampler.sample_center_point();
        auto _point = center_points[i];

        failure |= !torch::allclose(point, _point, 1e-5);
        if (failure) {
            std::cout << "Point: " << point << std::endl;
            std::cout << "Expected: " << _point << std::endl;
        }
    }

    _T(test, failure == false);
}

void test_randla_sampler(TestReports& reports)
{
    Test test("RandLA Sampler", reports);
    torch::jit::script::Module sampler_data = torch::jit::load(TEST_DATA_PATH "/randla_sampler/py_sampler_input.pt");

    Tensor possibility = sampler_data.attr("possibility").toTensor().to(torch::kFloat32);
    Tensor center_points = sampler_data.attr("pick_points").toTensor().to(torch::kFloat32);
    Tensor indices = sampler_data.attr("pick_idx").toTensor().to(torch::kInt64);
    Tensor noise = sampler_data.attr("noise").toTensor().to(torch::kInt64);
    center_points -= noise;

    util::Logger logger {};
    RandLANetConfig config {};
    segment::Path path = TEST_DATA_PATH "/randla_sampler/birmingham_block_12_py_ss.ply";
    config.init_defaults();
    RandLANetDataset dataset(path, config, &logger);

    dataset.prepare_pointcloud_data();

    RandLANetSampler sampler(&dataset);
    sampler.set_possibility(possibility);

    bool failure = false;
    for (int i = 0; i < center_points.size(0) && !failure; i++) {
        auto point = sampler.sample_center_point();
        auto _point = center_points[i];

        failure |= !torch::allclose(point, _point, 1e-5);
        if (failure) {
            std::cout << "Point: " << point << std::endl;
            std::cout << "Expected: " << _point << std::endl;
        }
    }

    _T(test, failure == false);
}
}