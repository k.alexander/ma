/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "test.hpp"
#include <segment.hpp>
#include <torch/torch.h>

namespace segment::tests {
extern void test_utils(TestReports&);
extern void test_ply(TestReports&);
extern void test_vectors(TestReports& reports);
extern void test_kpconv(TestReports& reports);
extern void test_performance(TestReports& reports);
extern void test_misc(TestReports& reports);
extern void test_randla_sampler(TestReports& reports);
extern void test_randlanet(TestReports& reports);
extern void test_randlanet_batch(TestReports& reports);
extern void test_randlanet_build_input(TestReports& reports);
extern void test_randla_augmentation(TestReports& reports);

void run()
{
    torch::manual_seed(42);
    TestReports reports;

    // test_utils(reports);
    // test_ply(reports);
    // test_vectors(reports);
    // test_kpconv(reports);
    // test_performance(reports);
    // test_misc(reports);
    // test_randla_sampler(reports);
    test_randlanet(reports);
    // test_randlanet_build_input(reports);
    // test_randlanet_batch(reports);
    // test_randla_augmentation(reports);
    print_report(reports);
}
}