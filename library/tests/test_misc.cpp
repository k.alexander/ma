/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <torch/script.h>

#include "kpconv/kpconv.hpp"
#include "kpconv/kpconv_dataset.hpp"
#include "pointcloud/grid_subsample.hpp"
#include "pointcloud/ply_util.hpp"
#include "test.hpp"
#include "util/types.hpp"
#include "util/util.hpp"
#include "util/logger.hpp"

using namespace segment;

std::tuple<Tensor, Tensor, Tensor> _augment_points(segment::KPConvConfig const& cfg, Tensor const points, double theta = 0, Tensor scale = {}, Tensor noise = {}, bool vertical = true)
{
    // initialize rotation matrix
    auto R = torch::eye(3);

    if (points.size(1) == 3) {
        if (vertical) {
            // create random rotations
            if (theta == 0)
                theta = torch::rand(1).item().toFloat() * 2 * M_PI;
            auto c = std::cos(theta);
            auto s = std::sin(theta);
            R = torch::tensor({ { c, -s, 0. }, { s, c, 0. }, { 0., 0., 1. } }, torch::dtype(torch::kFloat64));
        } else {
            // choose two random angles for the first vector in polar coordinates
            auto theta = torch::rand(1).item().toFloat() * 2 * M_PI;
            auto phi = (torch::rand(1).item().toFloat() - 0.5) * M_PI;

            // create the first vector in carthesian coordinates
            auto u = torch::tensor({ { std::cos(theta) * std::cos(phi), std::sin(theta) * std::cos(phi), std::sin(phi) } }, torch::dtype(torch::kFloat32));

            // choose a random rotation angle
            auto alpha = torch::rand(1) * 2 * M_PI;

            // create the rotation matrix with this vector and angle
            R = segment::util::create_3d_rotations(u, alpha)[0]; // TODO: check if the [0] is correct
        }
    }

    R = R.to(torch::kFloat32);

    // choose random scales for each example
    auto const min_s = cfg.augment_scale_min;
    auto const max_s = cfg.augment_scale_max;

    if (scale.size(0) == 0) {
        if (cfg.augment_scale_anisotropic) {
            scale = torch::rand({ points.size(1) }) * (max_s - min_s) + min_s;
        } else {
            scale = torch::rand(1) * (max_s - min_s) + min_s;
        }
        // add random symmetries to the scale factor
        auto symmetries = cfg.augment_symmetries.clone();

        symmetries *= torch::randint(2, symmetries.sizes(), torch::dtype(torch::kInt64));
        scale = (scale * (1 - symmetries * 2)).to(torch::dtype(torch::kFloat32));
    }

    if (noise.size(0) == 0)
        noise = torch::randn({ points.size(0), points.size(1) }) * cfg.augment_noise;

    // apply the scale and rotation
    // augmented_points = np.sum(np.expand_dims(points, 2) * R, axis=1) * scale + noise
    // R = torch::eye(3);
    auto augmented_points = torch::sum(points.unsqueeze(2) * R, 1) * scale + noise;

    return { augmented_points, scale, R };
}

namespace segment::tests {

inline void test_augmentation(Test& test)
{
    torch::jit::script::Module augmentation_data = torch::jit::load(TEST_DATA_PATH "/augmentation/input_output_dict.pt");
    KPConvConfig cfg {};

    auto input_points = augmentation_data.attr("input_points").toTensor();
    auto theta = (float)augmentation_data.attr("theta").toDouble();
    auto input_scale = augmentation_data.attr("scale").toTensor();
    auto noise = augmentation_data.attr("noise").toTensor();

    auto augmented_points = augmentation_data.attr("output_points").toTensor();
    auto scale = augmentation_data.attr("output_scale").toTensor();
    auto R = augmentation_data.attr("R").toTensor();

    auto [_augmented_points, _scale, _R] = _augment_points(cfg, input_points, theta, input_scale, noise, true);

    // check if the output is the same as the expected output
    test.check("augment_points", torch::allclose(_augmented_points, augmented_points, 1e-4));
    test.check("augment_scale", torch::allclose(_scale, scale, 1e-6));
    test.check("augment_R", torch::allclose(_R, R, 1e-6));

    util::save_tensor_as_ply(TEST_DATA_PATH "/augmentation/input_points.ply", input_points);
    util::save_tensor_as_ply(TEST_DATA_PATH "/augmentation/augmented_points.ply", augmented_points);
    util::save_tensor_as_ply(TEST_DATA_PATH "/augmentation/_augmented_points.ply", _augmented_points);
}

inline void test_rotation(Test& test)
{
    std::vector<int32_t> sl = { 847, 962, 703, 738 };

    // load known good values from python
    torch::jit::script::Module tensors = torch::jit::load(TEST_DATA_PATH "/subsampling/pools_and_points.pt");
    torch::jit::script::Module rotation = torch::jit::load(TEST_DATA_PATH "/subsampling/R.pt");

    Tensor stacked_points = tensors.attr("stacked_points").toTensor();
    Tensor pool_b = tensors.attr("pool_b").toTensor();
    Tensor pool_p = tensors.attr("pool_p").toTensor();
    Tensor R = rotation.attr("R").toTensor();
    Tensor alpha = rotation.attr("alpha").toTensor();
    Tensor phi = rotation.attr("phi").toTensor();
    Tensor theta = rotation.attr("theta").toTensor();

    const int B = sl.size();
    // Choose two random angles for the first vector in polar coordinates

    auto _R = util::create_rotation_from_angles(B, theta, phi, alpha);
    test.check("create_rotation_from_angles", torch::allclose(_R, R, 1e-6));

    auto const dl = 0.16f;
    auto result = util::batch_grid_subsampling(stacked_points, sl, dl, theta, phi, alpha);

    auto _pool_p = std::get<0>(result);
    auto _pool_b = std::get<1>(result);

    test.check("batch_grid_subsampling pool_p", torch::allclose(_pool_p, pool_p, 1e-6));
    test.check("batch_grid_subsampling pool_b", torch::allclose(_pool_b, pool_b, 1e-6));
}

inline void test_nanoflann_neighors(Test& test)
{
    torch::jit::script::Module nanoflann_neighbors = torch::jit::load(TEST_DATA_PATH "/subsampling/batch_nanoflann_neighbors.pt");

    auto stacked_points = nanoflann_neighbors.attr("stacked_points").toTensor();
    auto stack_lengths = nanoflann_neighbors.attr("stack_lengths").toTensor();
    auto conv_i = nanoflann_neighbors.attr("conv_i").toTensor().to(torch::kInt64);

    auto const sp = Vec3f::from_tensor(stacked_points);
    auto const sl = util::from_tensor<int>(stack_lengths);
    auto r = 0.2f;
    auto _conv_i = util::batch_nanoflann_neighbors(sp, sp, sl, sl, r);

    test.check("batch_nanoflann_neighbors", torch::allclose(_conv_i, conv_i, 1e-6));
}

inline void test_segmentation_inputs(Test& test)
{
    torch::jit::script::Module segmentation_inputs = torch::jit::load(TEST_DATA_PATH "/subsampling/segmentation_inputs_outputs.pt");

    auto stacked_points = segmentation_inputs.attr("stacked_points").toTensor();
    auto stacked_features = segmentation_inputs.attr("stacked_features").toTensor();
    auto stack_lengths = segmentation_inputs.attr("stack_lengths").toTensor();
    auto labels = segmentation_inputs.attr("labels").toTensor();

    TensorList input_list;
    for (int i = 0; i < 27; i++) {
        input_list.push_back(segmentation_inputs.attr("input_list_" + std::to_string(i)).toTensor());
    }

    KPConvConfig cfg {};
    cfg.load("");
    util::Logger log;
    KPConvDataset dataset(TEST_DATA_PATH "/../../../datasets/birmingham_block_12_test/birmingham_block_12 - Cloud.ply", cfg, &log);
    dataset.prepare_pointcloud_data();
    dataset.calibrate_for_pointcloud();
    dataset.prepare_projection();
    auto _input_list = dataset.segmentation_inputs(stacked_points, stacked_features, labels, stack_lengths);
}

void test_misc(TestReports& reports)
{
    Test test("misc functions", reports);
    test_rotation(test);
    test_nanoflann_neighors(test);
    // test_segmentation_inputs(test);
    test_augmentation(test);
}
}