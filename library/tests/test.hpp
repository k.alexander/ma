/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#define _T(t, what) t.check(#what, what)

constexpr unsigned int HeaderLength { 48 };

struct TestReport {
    std::string report_name;
    std::vector<std::string> failed_tests;
    int total { 0 };
    int failed { 0 };
};

using TestReports = std::vector<TestReport>;

inline void print_report(const TestReports& reports)
{
    std::cout << "Final report\n";
    int total { 0 }, total_failed { 0 };
    for (const auto& report : reports) {
        if (report.failed > 0) {
            std::cout << report.report_name.c_str() << ":\n";
            for (const auto& failed : report.failed_tests) {
                std::cout << " [!] " << failed << "\n";
            }
            std::cout << " " << report.failed << " out of " << report.total << " failed\n";
        }
        total += report.total;
        total_failed += report.failed;
    }
    std::cout << total << " tests run in total\n";
    if (total_failed > 0)
        std::cout << total_failed << " failed in total\n";
    else
        std::cout << "All tests passed!\n";
}

class Test {
    std::string m_name;
    int m_total_tests { 0 }, m_failed { 0 }, m_passed { 0 };
    std::vector<std::string> m_failed_tests;
    TestReports& m_reports;
    bool m_is_silent {};

public:
    explicit Test(const char* name, TestReports& reports, bool is_silent = true)
        : m_name(name + std::string(" Test"))
        , m_reports(reports)
        , m_is_silent(is_silent)
    {
        section("Begin " + m_name);
    }

    ~Test()
    {
        std::cout << m_total_tests << " tests run, " << m_passed << " passed, " << m_failed << " failed\n";
        section("End " + m_name);
        submit_report();
    }

    void check(const char* Name, bool bWhat)
    {
        if (!bWhat)
            std::cout << " [!] " << Name << "\n";
        else if (!m_is_silent)
            std::cout << " [+] " << Name << "\n";
        check(bWhat);
        if (!bWhat)
            m_failed_tests.emplace_back(Name);
    }

    void check(bool bWhat)
    {
        m_total_tests++;
        bWhat ? m_passed++ : m_failed++;
    }

    static void section(const std::string& SectionName)
    {
        int LengthLeft = int(roundf(float(HeaderLength - SectionName.length()) / 2.f));
        int LengthRight = int(HeaderLength - SectionName.length() - LengthLeft);

        std::string Header;
        for (int i = 0; i < LengthLeft; i++)
            std::cout << "=";
        std::cout << " " << SectionName.c_str() << " ";
        for (int i = 0; i < LengthRight; i++)
            std::cout << "=";
        std::cout << "\n";
    }

    void submit_report()
    {
        TestReport reports;
        reports.failed = m_failed;
        reports.total = m_total_tests;
        reports.report_name = m_name;
        reports.failed_tests = m_failed_tests;
        m_reports.emplace_back(reports);
    }
};
