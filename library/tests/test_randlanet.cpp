/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <segment.hpp>

#include <torch/script.h>

#include "pointcloud/ply_util.hpp"
#include "pointcloud/pointcloud.hpp"
#include "randlanet/randlanet_batch.hpp"
#include "randlanet/randlanet_config.hpp"
#include "randlanet/randlanet_dataset.hpp"
#include "test.hpp"

namespace segment::tests {

void test_randlanet_build_input(TestReports& reports)
{
    Test test("RandLANet build_input", reports);
    util::Logger logger {};
    RandLANetConfig config {};
    segment::Path path = TEST_DATA_PATH "/birmingham_block_12_py_ss.ply";
    config.init_defaults();
    RandLANetDataset dataset(path, config, &logger);

    torch::jit::script::Module tensors = torch::jit::load(TEST_DATA_PATH "/randla_sampler/py_build_input.pt");

    auto xyz = tensors.attr("xyz").toTensor().to(torch::kFloat32);
    auto rgb = tensors.attr("rgb").toTensor().to(torch::kFloat32);
    auto query_idx = tensors.attr("query_idx").toTensor().to(torch::kInt64);
    auto input_list = tensors.attr("input_list").toTensorVector();
    auto center_points = tensors.attr("center_points").toTensor().to(torch::kFloat32);
    auto input_list_ = dataset.build_input(xyz, rgb, query_idx);

    _T(test, input_list.size() == input_list_.size());

    bool equal = true;

    for (int i = 0; i < input_list.size(); i++) {
        auto a = input_list[i];
        auto b = input_list_[i];

        // sort the tensors
        auto sorted_result = torch::sort(a);
        a = std::get<0>(sorted_result);
        sorted_result = torch::sort(b);
        b = std::get<0>(sorted_result);

        if (a.dtype() != b.dtype()) {
            a = a.to(b.dtype());
        }

        equal &= torch::allclose(a, b, 1e-5);

        if (!equal) {
            std::cout << "Input list " << i << " is not equal" << std::endl;
            std::cout << a << std::endl;
            std::cout << b << std::endl;
        }
    }
}

void test_randlanet_batch(TestReports& reports)
{
    Test test("RandLANet batch building", reports);

    torch::jit::script::Module tensors = torch::jit::load(TEST_DATA_PATH "/randla_sampler/py_input.pt");
    auto input_lists = tensors.attr("input_lists").toList();
    auto coallessced_input_list_python = tensors.attr("input_list").toTensorVector();
    TensorList coallesced_input_list;

    RandLANetConfig cfg {};
    cfg.init_defaults();

    for (int batch_index = 0; batch_index < 16; batch_index++) {
        auto input_list = input_lists.get(batch_index).toTensorVector();

        // can happen if the dataset is empty or some other error occurred
        if (input_list.empty()) {
            break;
        }
        auto batch = segment::RandLANetBatch(input_list, cfg.num_layers);
        batch.to_coallesced_list(coallesced_input_list, batch_index, cfg);
    }

    _T(test, coallessced_input_list_python[0].dim() == coallesced_input_list[0].dim());

    // compare the two lists
    for (int i = 0; i < coallesced_input_list.size(); i++) {
        auto a = coallesced_input_list[i];
        auto b = coallessced_input_list_python[i];
        _T(test, torch::allclose(a, b, 1e-5));
    }

    // Compare batches
    RandLANetBatch python_batch(coallessced_input_list_python, cfg.num_layers);
    RandLANetBatch cpp_batch(coallesced_input_list, cfg.num_layers);

    _T(test, python_batch.is_equal(cpp_batch));
}

void test_randlanet(TestReports& reports)
{
    at::init_num_threads();
    Test test("RandLANet cloud segmentation", reports);
    segment::INetwork* randla {};

    SegmentVersionInfo info {};
    segment_get_version_info(&info);

    SegmentRandLANetConfig config {};

    config.base.num_iterations = 4;
    config.base.validation_size = 4;
    config.base.num_fetch_threads = 1;
    config.base.prefer_gpu_inference = false;


    if (info.compute_platform == SEGMENT_COMPUTE_CPU || !config.base.prefer_gpu_inference)
        randla = segment::create_randla_network(TEST_DATA_PATH "/../../../data/randlanet_toronto3d_jit_cpu.pt");
    else
        randla = segment::create_randla_network(TEST_DATA_PATH "/../../../data/randlanet_toronto3d_jit.pt");

    _T(test, randla->initialize() == SEGMENT_OK);

    randla->configure(&config);

    util::Pointcloud input;
    _T(test, util::load_ply(TEST_DATA_PATH "/randla_test/birmingham_block_12.ply", input, 1 / 255.f));

    uint8_t* classifications;
    _T(test, randla->segment_points((float*)input.points.data(), (float*)input.colors.data(), input.points.size(), &classifications) == SEGMENT_OK);

    std::vector<uint8_t> classifications_vec(classifications, classifications + input.points.size());

    input.labels = classifications_vec;
    _T(test, util::save_ply(TEST_DATA_PATH "/birmingham_block_12_segmented.ply", input, 255.f));
    segment_free(classifications);
    segment_free_network(randla);
}
}
