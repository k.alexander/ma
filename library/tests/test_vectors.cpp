/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "test.hpp"
#include "util/types.hpp"
#include "util/vec3.hpp"
#include "util/vec4.hpp"

namespace segment::tests {
void test_vectors(TestReports& reports)
{
    Test test("Vector functions", reports);

    Vec3f vec3f(1.0f, 2.0f, 3.0f);
    Vec3f vec3f2(1.0f, 2.0f, 3.0f);
    Vec3f zero_vec3f(0.0f, 0.0f, 0.0f);

    _T(test, vec3f == vec3f2);
    _T(test, vec3f - vec3f2 == zero_vec3f);
    _T(test, vec3f + vec3f2 == Vec3f(2.0f, 4.0f, 6.0f));
    _T(test, vec3f * 0.0f == zero_vec3f);

    Vec4f vec4f(1.0f, 2.0f, 3.0f, 4.0f);
    Vec4f vec4f2(1.0f, 2.0f, 3.0f, 4.0f);
    Vec4f zero_vec4f(0.0f, 0.0f, 0.0f, 0.0f);

    _T(test, vec4f == vec4f2);
    _T(test, vec4f - vec4f2 == zero_vec4f);
    _T(test, vec4f + vec4f2 == Vec4f(2.0f, 4.0f, 6.0f, 8.0f));
    _T(test, vec4f * 0.0f == zero_vec4f);

    // Tensor to vector
    Tensor tensor = torch::tensor({ { 1.0f, 2.0f, 3.0f }, { 4.0f, 5.0f, 6.0f } });
    std::vector<Vec3f> vec3f_vector = Vec3f::from_tensor(tensor);

    _T(test, vec3f_vector[0] == Vec3f(1.0f, 2.0f, 3.0f));
    _T(test, vec3f_vector[1] == Vec3f(4.0f, 5.0f, 6.0f));
}
}