/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <torch/script.h>
#include <torch/torch.h>

#include "randlanet/randlanet_config.hpp"
#include "test.hpp"
#include "util/util.hpp"

namespace segment::tests {

Tensor augment_input(Tensor xyz, Tensor rgb, Tensor theta, Tensor scales, Tensor noise, RandLANetConfig const& cfg)
{
    auto transformed_xyz = util::rotate(xyz.clone(), 0.f, 0.f, theta.item<float>());

    auto min_s = cfg.augment_scale_min;
    auto max_s = cfg.augment_scale_max;

    transformed_xyz *= scales;

    transformed_xyz = transformed_xyz + noise;

    return torch::cat({ transformed_xyz, rgb }, -1);
}

void test_randla_augmentation(TestReports& reports)
{
    Test test("RandLA augementation", reports);
    RandLANetConfig cfg;
    cfg.init_defaults();

    torch::jit::script::Module rotate_data = torch::jit::load(TEST_DATA_PATH "/augmentation/randla_py_rotate.pt");

    Tensor xyz = rotate_data.attr("xyz").toTensor().to(torch::kFloat32);
    Tensor angles = rotate_data.attr("angles").toTensor().to(torch::kFloat32);
    Tensor rotated = rotate_data.attr("rotated").toTensor().to(torch::kFloat32);

    auto yaw = angles[0].item<float>();
    auto pitch = angles[1].item<float>();
    auto roll = angles[2].item<float>();
    auto _rotated = util::rotate(xyz, yaw, pitch, roll);

    _T(test, !torch::allclose(rotated, _rotated, 1e-5));

    torch::jit::script::Module augment_data = torch::jit::load(TEST_DATA_PATH "/augmentation/randla_py_augment.pt");

    xyz = augment_data.attr("xyz").toTensor().to(torch::kFloat32);
    Tensor theta = augment_data.attr("theta").toTensor().to(torch::kFloat32);
    Tensor scales = augment_data.attr("scales").toTensor().to(torch::kFloat32);
    Tensor noise = augment_data.attr("noise").toTensor().to(torch::kFloat32);
    Tensor rgb = augment_data.attr("rgb").toTensor().to(torch::kFloat32);
    Tensor result = augment_data.attr("features").toTensor().to(torch::kFloat32);

    auto _result = augment_input(xyz, rgb, theta, scales, noise, cfg);

    _T(test, !torch::allclose(result, _result, 1e-5));
}

}