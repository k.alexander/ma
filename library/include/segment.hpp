/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include "segment.h"
#include <cstdint>
#include <string>

namespace segment {

/**
 * @brief Interface for a network that segments point clouds
 */
class INetwork {
public:
    INetwork() = default;
    virtual ~INetwork() = default;

    /**
     * @brief Initialize the network, i.e. load the model
     * @return SegmentError::OK if successful
     */
    virtual SegmentError initialize() { return SEGMENT_NOT_IMPLEMENTED; }

    /**
     * @brief Segment a point cloud
     * @param input_cloud_path Path to the input point cloud (PLY file)
     * @param output_cloud_folder Path to the output folder
     * @return SegmentError::OK if successful
     */
    virtual SegmentError segment_pointcloud(char const* input_cloud_path, char const* output_cloud_folder) { return SEGMENT_NOT_IMPLEMENTED; }

    /**
     * @brief Segments points directly from and to memory
     * @param points Array of points
     * @param num_points Number of points
     * @param classifications Array of classifications, free with segment_free
     * @param count Number of classifications
     * @return SegmentError::OK if successful
     */
    virtual SegmentError segment_points(float const* points, uint64_t num_points, uint8_t** classifications) { return SEGMENT_NOT_IMPLEMENTED; }

    /**
     * @brief Segments points directly from and to memory
     * @param points Array of points
     * @param colors Array of colors (RGB)
     * @param num_points Number of points
     * @param classifications Array of classifications, free with segment_free
     * @param count Number of classifications
     * @return SegmentError::OK if successful
     */
    virtual SegmentError segment_points(float const* points, float const* colors, uint64_t num_points, uint8_t** classifications) { return SEGMENT_NOT_IMPLEMENTED; }

    virtual void configure(SegmentHandle const config = nullptr) { }

    virtual std::string fetch_log() const { return ""; }

    virtual std::string fetch_progress(float* progress) const { return ""; }

    virtual void* get_info_data(SegmentInfoType type, uint64_t* out_info_size) const { return nullptr; }

    virtual bool is_info_data_updated(SegmentInfoType type) const { return false; }

    virtual void interrupt_segmentation() {};
};

inline INetwork* create_kpconv_network(const char* model_path)
{
    return static_cast<INetwork*>(segment_create_kpconv_network(model_path));
}

inline INetwork* create_randla_network(const char* model_path)
{
    return static_cast<INetwork*>(segment_create_randla_network(model_path));
}

inline bool is_gpu_available()
{
    return segment_is_gpu_available();
}

#if defined(WITH_TESTS)
namespace tests {
EXPORT void run();
}
#endif

}