/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "wrapper/window.hpp"

int main(int, const char**)
{
    RL::Window().Run();
    return 0;
}