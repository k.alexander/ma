/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <atomic>
#include <raylib.h>
#include <string>
#include <vector>

#include "visualization/data_source.hpp"
#include "wrapper/window.hpp"

using namespace std::filesystem;
namespace Viz {
class PLYSource : public IDataSource {

    std::string m_file_path = "C:\\Users\\Alexander Kozel\\Documents\\repos\\magit\\library\\tests\\data\\randla_test\\birmingham_block_12.ply";

    PointCloud m_point_cloud {};

    void load_ply_file();
    void save_ply_file();
    int m_start {}, m_end { 1 };

    bool m_convert_coordinate_system { true };
    Vector3 m_scale { 1.f, 1.f, 1.f };
    Vector3 m_offset {
        0,
        0,
    };

    std::vector<Vector3> m_positions;
    std::vector<Vector3> m_colors;
    std::vector<Vector3> m_classes;
    std::vector<uint8_t> m_prediction_classes;
    std::vector<Vector3> m_predictions;

    std::atomic<bool> m_updating_predictions { false };

    int m_point_size { 1 };
    bool m_uniform_scale { true }, m_show_classes {};
    Shader m_pc_shader, m_pc_vertex_color_shader;

    int m_current_display_option { 1 };

public:
    PLYSource()
    {
        m_pc_shader = GetDefaultPointCloudShader(false);
        m_pc_vertex_color_shader = GetDefaultPointCloudShader(true);
        load_ply_file();
    }

    ~PLYSource()
    {
        UnloadShader(m_pc_shader);
        UnloadShader(m_pc_vertex_color_shader);
        UnloadPointCloud(m_point_cloud);
    }

    const char* name() override { return "PLY File"; }

    void draw(RL::Window*) override;
    void draw_gui(RL::Window*) override;

    std::vector<Vector3> const& get_points() const override { return m_positions; }
    std::vector<Vector3> const& get_colors() const override { return m_colors; }
    void set_predictions(uint8_t const*, size_t num) override;
    void update_point_cloud_predictions() override;

    int get_point_size() const override { return m_point_size; }
};
}
