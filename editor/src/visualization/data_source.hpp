/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cstdint>
#include <raylib.h>
#include <vector>

namespace RL {
class Window;
}

namespace Viz {

/**
 * Interface for loading and visualizing point cloud data.
 */
class IDataSource {
public:
    virtual ~IDataSource() = default;

    virtual const char* name() = 0;

    virtual void draw(RL::Window*) = 0;
    virtual void draw_gui(RL::Window*) = 0;
    virtual std::vector<Vector3> const& get_points() const = 0;
    virtual std::vector<Vector3> const& get_colors() const = 0;
    virtual void set_predictions(uint8_t const*, size_t num) { }
    virtual void update_point_cloud_predictions() = 0;
    virtual int get_point_size() const { return 1; }
};
}
