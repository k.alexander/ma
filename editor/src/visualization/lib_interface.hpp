/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <atomic>
#include <filesystem>
#include <imgui.h>
#include <mutex>
#include <segment.h>
#include <string>
#include <thread>

namespace RL {
class Window;
}

namespace lib {

class Interface {
    enum async_state {
        IDLING,
        PROCESSING,
        DONE,
        STOPPING
    };

    char m_library_path[1024] {};

    static constexpr int MAX_PREVIEW_POINTS = 14000;
    segment_is_gpu_available_t _is_gpu_available {};
    segment_free_network_t _free_network {};
    segment_configure_kpconv_network_t _configure_kpconv_network {};
    segment_create_kpconv_network_t _create_kpconv_network {};
    segment_configure_randla_network_t _configure_randla_network {};
    segment_create_randla_network_t _create_randla_network {};
    segment_initialize_t _initialize_network {};
    segment_segment_points_t _segment_points {};
    segment_segment_points_with_rgb_t _segment_points_with_rgb {};
    segment_fetch_log_t _fetch_log {};
    segment_fetch_progress_t _fetch_progress {};
    segment_get_info_data_t _get_info_data {};
    segment_is_info_data_updated_t _is_info_data_updated {};
    segment_interrupt_segmentation_t _interrupt_segmentation {};
    segment_set_torch_seed_t _set_torch_seed {};
    segment_free_t _free {};
    segment_get_version_info_t _get_version_info {};

    SegmentNetworkConfig m_network_config {};
    SegmentKPConvConfig m_kpconv_config {};
    SegmentRandLANetConfig m_randla_config {};
    std::string m_version_info {};

    void* m_library_handle {};
    bool m_gpu_available {};
    ImGuiTextBuffer m_log;
    std::string m_progress_state {};
    float m_progress {};
    bool m_is_loaded_model_for_gpu {};

    double m_last_log_refresh {};

    int m_network_type { 1 };

    bool m_prefer_gpu_inference { true };

    uint8_t* m_segment_results {};
    SegmentError m_segment_result { SEGMENT_INVALID };

    std::thread m_segment_thread {}, m_update_probabilities_thread {};
    std::atomic<async_state> m_segmentation_state {}, m_update_probabilities_state {};

    PointCloud m_active_points {};
    Shader m_pointcloud_shader {};

    SegmentHandle m_network {};

    bool prepare_network();

    void start_segmentation();

    void segmentation_thread_method();

    void update(RL::Window* window);

    Viz::IDataSource* m_data_source {};

public:
    Interface();

    ~Interface()
    {
        unload();
    }

    bool load(const std::filesystem::path& path);

    void unload();

    bool is_gpu_available() const;

    void draw_gui(RL::Window*);

    void draw_points(RL::Window*);
};
}
