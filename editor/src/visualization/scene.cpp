/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <imgui.h>
#include <implot.h>
#include <sstream>

#include "visualization/data_source.hpp"
#include "visualization/ply_source.hpp"
#include "visualization/scene.hpp"
#include "wrapper/viewport.hpp"
#include "wrapper/window.hpp"

namespace Viz {

void Scene::draw(RL::Window* w)
{
    DrawGrid(10, 1.0f);

    DrawLine3D({ 0, 0.1, 0 }, { 1, 0.1, 0 }, RED);
    DrawLine3D({ 0, 0.1, 0 }, { 0, 1.1, 0 }, GREEN);
    DrawLine3D({ 0, 0.1, 0 }, { 0, 0.1, 1 }, BLUE);

    if (m_active_data_source)
        m_active_data_source->draw(w);
    m_lib_interface.draw_points(w);
}

void Scene::tick(RL::Window* w)
{
    m_tick_count++;
}

void Scene::draw_data_gui(RL::Window* w)
{
    if (!ImGui::Begin("Settings", nullptr))
        return;

    if (ImGui::BeginCombo("Data Source", m_data_source.c_str())) {
        if (ImGui::Selectable("PLY file")) {
            set_data_source<PLYSource>();
        }
        ImGui::EndCombo();
    }

    if (m_active_data_source)
        m_active_data_source->draw_gui(w);

    ImGui::End();

    m_lib_interface.draw_gui(w);
}

void Scene::pause()
{
    m_tick_count = 0;
}

}
