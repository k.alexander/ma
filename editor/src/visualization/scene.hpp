/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <memory>
#include <raylib.h>
#include <string>
#include <vector>

#include "visualization/data_source.hpp"
#include "visualization/lib_interface.hpp"
#include "wrapper/types.hpp"

namespace RL {
class Window;
}

namespace Viz {
class Scene {
    std::unique_ptr<Viz::IDataSource> m_active_data_source { nullptr };
    int m_tick_count = 0; // Ticks since play was pressed

    std::string m_data_source {};

    lib::Interface m_lib_interface;

public:
    template<class T, typename... Args>
    void set_data_source(Args&&... args)
    {
        m_active_data_source = nullptr;
        m_active_data_source = std::make_unique<T>(std::forward<Args>(args)...);
        m_data_source = m_active_data_source->name();
    }

    void draw(RL::Window* w);

    void draw_data_gui(RL::Window* w);

    void tick(RL::Window* w);

    int get_tick_count() { return m_tick_count; }
    void pause();

    Viz::IDataSource* get_data_source() { return m_active_data_source.get(); }
};
}
