/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <functional>
#include <happly.h>
#include <raymath.h>

#include "util/dll.hpp"
#include "visualization/data_source.hpp"
#include "visualization/lib_interface.hpp"
#include "wrapper/viewport.hpp"
#include "wrapper/window.hpp"

#if defined(_WIN32)
#    define LIB_NAME "segment.dll"
#elif defined(__linux__)
#    define LIB_NAME "libsegment.so"
#else
#    error "Unsupported platform"
#endif

namespace lib {

Interface::Interface()
{
    std::string tmp(LIB_FOLDER "/" LIB_NAME);
    memcpy(m_library_path, LIB_FOLDER "/" LIB_NAME, tmp.length());
}

void Interface::draw_points(RL::Window* window)
{
    if (m_active_points.pointCount > 0) {
        int point_size = 2;
        if (m_data_source)
            point_size = m_data_source->get_point_size() + 1;
        DrawPoints3D(m_active_points, point_size, MatrixRotateX(-90 * DEG2RAD));
    }
}

bool Interface::load(const std::filesystem::path& path)
{
    m_library_handle = util::dll::load(path);
    if (!m_library_handle)
        return false;

    _is_gpu_available = util::dll::sym<segment_is_gpu_available_t>(m_library_handle, "segment_is_gpu_available");
    _free_network = util::dll::sym<segment_free_network_t>(m_library_handle, "segment_free_network");
    _configure_kpconv_network = util::dll::sym<segment_configure_kpconv_network_t>(m_library_handle, "segment_configure_kpconv_network");
    _create_kpconv_network = util::dll::sym<segment_create_kpconv_network_t>(m_library_handle, "segment_create_kpconv_network");
    _configure_randla_network = util::dll::sym<segment_configure_randla_network_t>(m_library_handle, "segment_configure_randla_network");
    _create_randla_network = util::dll::sym<segment_create_randla_network_t>(m_library_handle, "segment_create_randla_network");
    _initialize_network = util::dll::sym<segment_initialize_t>(m_library_handle, "segment_initialize_network");
    _fetch_log = util::dll::sym<segment_fetch_log_t>(m_library_handle, "segment_fetch_log");
    _free = util::dll::sym<segment_free_t>(m_library_handle, "segment_free");
    _get_version_info = util::dll::sym<segment_get_version_info_t>(m_library_handle, "segment_get_version_info");
    _segment_points = util::dll::sym<segment_segment_points_t>(m_library_handle, "segment_segment_points");
    _segment_points_with_rgb = util::dll::sym<segment_segment_points_with_rgb_t>(m_library_handle, "segment_segment_points_with_rgb");
    _fetch_progress = util::dll::sym<segment_fetch_progress_t>(m_library_handle, "segment_fetch_progress");
    _get_info_data = util::dll::sym<segment_get_info_data_t>(m_library_handle, "segment_get_info_data");
    _is_info_data_updated = util::dll::sym<segment_is_info_data_updated_t>(m_library_handle, "segment_is_info_data_updated");
    _interrupt_segmentation = util::dll::sym<segment_interrupt_segmentation_t>(m_library_handle, "segment_interrupt_segmentation");
    _set_torch_seed = util::dll::sym<segment_set_torch_seed_t>(m_library_handle, "segment_set_torch_seed");

    m_kpconv_config.base.num_iterations = 10;
    m_kpconv_config.base.num_fetch_threads = std::thread::hardware_concurrency();
    m_kpconv_config.base.validation_size = 50;
    m_kpconv_config.test_smooth = 0.95f;
    m_kpconv_config.test_radius_ratio = 0.7f;
    m_kpconv_config.expected_N = 100000;
    m_kpconv_config.Kp = m_kpconv_config.expected_N / 200;
    m_kpconv_config.Kd = m_kpconv_config.Kp * 5;
    m_kpconv_config.low_pass_T = 100;
    m_kpconv_config.sample_batches = 999;
    m_kpconv_config.base.enable_live_data = false;

    m_gpu_available = is_gpu_available();
    m_pointcloud_shader = GetDefaultPointCloudShader(false);

    // create a dummy point cloud to display
    void* data = malloc(MAX_PREVIEW_POINTS * sizeof(Vector3));
    memset(data, 0, MAX_PREVIEW_POINTS * sizeof(Vector3));
    m_active_points = LoadPointCloud((Vector3*)data, nullptr, m_pointcloud_shader, MAX_PREVIEW_POINTS);
    m_active_points.color = { 1.0f, 0.0f, 0.8f, 1.0f };
    free(data);

    SegmentVersionInfo info {};
    _get_version_info(&info);
    m_version_info = fmt::format("Library v{}.{}.{}, LibTorch v{}.{}.{}",
        info.library_version.major, info.library_version.minor, info.library_version.patch,
        info.torch_version.major, info.torch_version.minor, info.torch_version.patch);

    switch (info.compute_platform) {
    case SEGMENT_COMPUTE_CUDA:
        m_version_info += ", CUDA ";
        break;
    case SEGMENT_COMPUTE_ROCM:
        m_version_info += ", HIP runtime ";
        break;
    default:
        m_version_info += ", CPU ";
        break;
    }

    m_version_info += fmt::format("v{}.{}.{}", info.compute_version.major, info.compute_version.minor, info.compute_version.patch);

    m_network_config.num_fetch_threads = 4;
    m_network_config.prefer_gpu_inference = info.compute_platform != SEGMENT_COMPUTE_CPU;
    m_network_config.num_iterations = 10;
    m_network_config.validation_size = 50;
    //_set_torch_seed(42);
    return true;
}

void Interface::unload()
{
    if (m_segment_thread.joinable()) {
        TraceLog(LOG_WARNING, "Segmentation thread still running, waiting for it to finish");
        m_segment_thread.join();
    }
    UnloadShader(m_pointcloud_shader);
    UnloadPointCloud(m_active_points);
    m_pointcloud_shader = {};
    m_active_points = {};
    if (_free_network)
        _free_network(m_network);
    if (_free)
        _free(m_segment_results);
    m_network = nullptr;
    m_segment_results = nullptr;
    util::dll::unload(m_library_handle);
    m_library_handle = nullptr;
    _is_gpu_available = nullptr;
    _free_network = nullptr;
    _configure_kpconv_network = nullptr;
    _create_kpconv_network = nullptr;
    _initialize_network = nullptr;
    _fetch_log = nullptr;
    _free = nullptr;
    _get_version_info = nullptr;
    _segment_points = nullptr;
    _fetch_progress = nullptr;
    _get_info_data = nullptr;
    _is_info_data_updated = nullptr;
    _interrupt_segmentation = nullptr;
    _set_torch_seed = nullptr;
}

bool Interface::is_gpu_available() const
{
    if (!_is_gpu_available)
        return false;

    return _is_gpu_available();
}

bool Interface::prepare_network()
{
    // unload the model if the preferred inference device changed
    if (m_network) {
        if (m_network_type == 1) {
            if (m_is_loaded_model_for_gpu != m_randla_config.base.prefer_gpu_inference) {
                _free_network(m_network);
                m_network = nullptr;
            }
        }
    }
    
    if (!m_network) {
        if (m_network_type == 0) {
            m_network = _create_kpconv_network(DATA_FOLDER "/data/kpconv_toronto3d_jit.pt");
        } else {
            if (m_network_config.prefer_gpu_inference)
                m_network = _create_randla_network(DATA_FOLDER "/data/randlanet_toronto3d_jit.pt");
            else
                m_network = _create_randla_network(DATA_FOLDER "/data/randlanet_toronto3d_jit_cpu.pt");
        }
        if (!m_network)
            return false;
    }

    auto err = _initialize_network(m_network);
    if (err != SEGMENT_OK) {
        TraceLog(LOG_ERROR, "Failed to initialize network, error code : %d", err);
        _free_network(m_network);
        m_network = nullptr;
        return false;
    }

    return true;
}

void Interface::segmentation_thread_method()
{
    auto const& points = m_data_source->get_points();
    auto const& colors = m_data_source->get_colors();
    if (m_network_type == 0)
        m_segment_result = _segment_points(m_network, (float*)points.data(), points.size(), &m_segment_results);
    else
        m_segment_result = _segment_points_with_rgb(m_network, (float*)points.data(), (float*)colors.data(), points.size(), &m_segment_results);
    m_segmentation_state = DONE;
}

void Interface::start_segmentation()
{
    m_segmentation_state = PROCESSING;
    m_segment_result = SEGMENT_INVALID;
    _free(m_segment_results);
    m_segment_results = nullptr;

    m_segment_thread = std::thread(std::bind(&Interface::segmentation_thread_method, this));
}

void Interface::draw_gui(RL::Window* window)
{

#define TOOLTIP(x)              \
    ImGui::SameLine();          \
    ImGui::TextDisabled("(?)"); \
    if (ImGui::IsItemHovered()) \
    ImGui::SetTooltip(x)

    if (!ImGui::Begin("Segmentation", nullptr))
        return;

    ImGui::InputText("Library path", m_library_path, sizeof(m_library_path));
    if (m_library_handle) {
        ImGui::Text("%s", m_version_info.c_str());
        ImGui::Text("GPU available: %s", m_gpu_available ? "Yes" : "No");
        ImGui::RadioButton("KPConv", &m_network_type, 0);
        ImGui::RadioButton("RandLANet", &m_network_type, 1);
        if (m_segmentation_state == IDLING) {
            if (ImGui::Button("Unload Library")) {
                unload();
            }

            if (ImGui::Button("Start segmentation") && prepare_network()) {
                m_kpconv_config.base = m_network_config;
                m_randla_config.base = m_network_config;

                if (m_network_type == 0)
                    _configure_kpconv_network(m_network, &m_kpconv_config);
                else
                    _configure_randla_network(m_network, &m_randla_config);
                m_data_source = window->get_scene().get_data_source();
                if (m_data_source)
                    start_segmentation();
            }

            if (ImGui::CollapsingHeader("Network config")) {
                ImGui::InputInt("Number of iterations", &m_network_config.num_iterations);
                TOOLTIP("Amount of times to iterate over the dataset (default: 10)");

                ImGui::InputInt("Number of fetch threads", &m_network_config.num_fetch_threads);
                TOOLTIP("Number of threads to use for fetching data (defaults to the number of cores)");

                ImGui::InputInt("Validation size", &m_network_config.validation_size);
                TOOLTIP("Number of batches to take out of the dataset per iteration (default: 50)");

                ImGui::Checkbox("Prefer GPU inference", (bool*)&m_network_config.prefer_gpu_inference);
                TOOLTIP("Use the GPU for inference if available otherwise fallback to CPU inference (default: true)");

                ImGui::InputInt("Inference device", &m_network_config.inference_device);
                TOOLTIP("Inference device id to use for libtorch (default: 0)");

                if (m_network_type == 0) {
                    ImGui::InputFloat("Test smooth", &m_kpconv_config.test_smooth);
                    TOOLTIP("Weighting of old predictions vs new prediction");

                    ImGui::InputFloat("Test radius ratio", &m_kpconv_config.test_radius_ratio);
                    TOOLTIP("Percentage of the sphere radius used for masking predictions");

                    ImGui::InputInt("Expected N", &m_kpconv_config.expected_N);
                    TOOLTIP("Expected batch size order of magnitude ");

                    ImGui::InputInt("Kp", &m_kpconv_config.Kp);
                    ImGui::InputInt("Kd", &m_kpconv_config.Kd);
                    ImGui::InputInt("Low pass T", &m_kpconv_config.low_pass_T);
                    ImGui::InputInt("Sample batches", &m_kpconv_config.sample_batches);
                }

                ImGui::Checkbox("Enable live data", (bool*)&m_kpconv_config.base.enable_live_data);
                TOOLTIP("If true the network will provide data about the currently processed points\nand the probabilities for all points once every ten iterations. This does slow down the processing a bit\n");
            }

        } else if (m_segmentation_state == PROCESSING) {
            if (ImGui::Button("Interrupt") || WindowShouldClose()) {
                m_segmentation_state = STOPPING;
                _interrupt_segmentation(m_network);
            }
        }
        update(window);
    } else if (ImGui::Button("Load Library")) {
        auto path = std::filesystem::path(m_library_path);
        if (!path.empty()) {
            load(path);
        }
    }

    ImGui::ProgressBar(m_progress, ImVec2(200, 0), m_progress_state.c_str());
    // multi line textbox for log
    ImGui::BeginChild("library log");

    ImGui::TextUnformatted(m_log.begin(), m_log.end());

    ImGui::EndChild();
    ImGui::End();
}

void Interface::update(RL::Window* window)
{
    if (GetTime() - m_last_log_refresh < 0.2)
        return;

    m_last_log_refresh = GetTime();

    // forward logging to imgui window
    char* log_line = _fetch_log(m_network);
    if (log_line) {
        m_log.append(log_line);
        _free(log_line);
    }

    // check if segmentation is done
    if (m_segmentation_state == DONE && m_data_source) {
        m_segmentation_state = IDLING;
        m_segment_thread.join();
        m_segment_thread = {};
        if (m_segment_result == SEGMENT_OK) {
            auto const& points = m_data_source->get_points();
            m_data_source->set_predictions(m_segment_results, points.size());
        } else {
            TraceLog(LOG_ERROR, "Segmentation failed with error code %d", m_segment_result);
        }
    }

    // display current progress in gui
    char* progress_state = _fetch_progress(m_network, &m_progress);
    if (progress_state) {
        m_progress_state = progress_state;
        _free(progress_state);
    }

    // live display of currently processing points
    if (_is_info_data_updated(m_network, SEGMENT_INFO_CURRENT_POINTS)) {
        uint64_t info_size {}, num_points {};
        void* points = _get_info_data(m_network, SEGMENT_INFO_CURRENT_POINTS, &info_size);
        num_points = info_size / sizeof(Vector3);

        if (points) {
            memset(m_active_points.positions, 0, MAX_PREVIEW_POINTS * sizeof(Vector3));
            UpdatePointCloud(&m_active_points, (Vector3*)points, nullptr, std::min<int>(num_points, MAX_PREVIEW_POINTS));
            _free(points);
        }
    }

    // live display of current predictions
    if (m_update_probabilities_state == IDLING) {
        if (_is_info_data_updated(m_network, SEGMENT_INFO_CURRENT_PROBS)) {
            m_update_probabilities_state = PROCESSING;

            m_update_probabilities_thread = std::thread([this]() {
                uint64_t info_size {}, num_points {};
                void* probs = _get_info_data(m_network, SEGMENT_INFO_CURRENT_PROBS, &info_size);
                num_points = info_size / sizeof(uint8_t);

                if (probs) {
                    auto const& points = m_data_source->get_points();
                    m_data_source->set_predictions((uint8_t*)probs, num_points);
                }
                _free(probs);

                m_update_probabilities_state = DONE;
            });
        }
    } else if (m_update_probabilities_state == DONE) {
        m_update_probabilities_thread.join();
        m_update_probabilities_thread = {};
        m_update_probabilities_state = IDLING;
        m_data_source->update_point_cloud_predictions();

        window->get_viewport(1)->force_redraw();
        window->get_viewport(0)->force_redraw();
    }
}
}
