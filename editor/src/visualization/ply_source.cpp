/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define TINYPLY_IMPLEMENTATION
#include <fstream>
#include <happly.h>
#include <imgui.h>
#include <raymath.h>
#include <vector>

#include "L2DFileDialog.h"
#include "visualization/ply_source.hpp"

static const std::vector<Vector3> class_colors = {
    { 0.0f, 0.0f, 1.0f },    // Unclassified
    { 0.0f, 0.35f, 0.63f },  // Road
    { 0.96f, 0.61f, 0.14f }, // Road markings
    { 0.11f, 1.0f, 0.0f },   // Natural
    { 0.48f, 1.0f, 0.0f },   // Building
    { 0.5, 0.5f, 0.5f },     // Utility line
    { 1.0f, 0.76f, 0.0f },   // Pole
    { 1.0f, 0.38f, 0.0f },   // Car
    { 1.0f, 0.0f, 0.0f },    // Fence
};

namespace Viz {

void PLYSource::draw(RL::Window* w)
{
    if (m_point_cloud.pointCount <= 0 || m_current_display_option == 0)
        return;
    Matrix offset = MatrixTranslate(m_offset.x, m_offset.y, m_offset.z);
    Matrix m = MatrixScale(m_scale.x, m_scale.y, m_scale.z);

    m = MatrixMultiply(m, offset);
    if (m_convert_coordinate_system) {
        // rotate 90 degrees around x axis
        m = MatrixMultiply(m, MatrixRotateX(-90 * DEG2RAD));
    }

    DrawPoints3D(m_point_cloud, m_point_size, m);
}

void PLYSource::draw_gui(RL::Window* w)
{
    if (ImGui::Button("Select .ply")) {
        FileDialog::fileDialogOpen = true;
    }

    if (FileDialog::fileDialogOpen) {
        if (FileDialog::ShowFileDialog(&FileDialog::fileDialogOpen, m_file_path)) {
            FileDialog::fileDialogOpen = false;

            // load the file
            if (!m_file_path.empty()) {
                load_ply_file();
            }
        }
    }
    ImGui::Checkbox("Convert Coordinate System", &m_convert_coordinate_system);
    if (ImGui::IsItemHovered()) {
        ImGui::BeginTooltip();
        ImGui::Text("+Z up, +Y forward (KPConv) -> +Y up, +Z forward (OpenGL)");
        ImGui::EndTooltip();
    }

    ImGui::InputFloat3("Offset", &m_offset.x);

    ImGui::SliderInt("Point Size", &m_point_size, 1, 10);

    if (!m_prediction_classes.empty()) {
        if (ImGui::Button("Save"))
            save_ply_file();
    }

    if (ImGui::SliderFloat3("Scale", &m_scale.x, 0.01f, 2.f)) {
        if (m_uniform_scale) {
            m_scale.y = m_scale.x;
            m_scale.z = m_scale.x;
        }
    }

    if (ImGui::Checkbox("Uniform Scale", &m_uniform_scale)) {
        if (m_uniform_scale) {
            m_scale.y = m_scale.x;
            m_scale.z = m_scale.x;
        }
    }


    if (!m_point_cloud.colors)
        ImGui::SliderFloat4("Color", &m_point_cloud.color.x, 0.f, 1.f);

    // add info label
    ImGui::Text("Loaded %d vertices", m_point_cloud.pointCount);

    static const char* items[] = { "None", "Base", "Color", "Labels", "Prediction" };

    // disable combo box while updating
    if (m_updating_predictions)
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);

    if (ImGui::Combo("##combo", &m_current_display_option, items, IM_ARRAYSIZE(items))) {
        if (m_current_display_option == 1) {
            UnloadPointCloud(m_point_cloud);
            m_point_cloud = LoadPointCloud(m_positions.data(), nullptr, m_pc_shader, m_positions.size());
        } else if (m_current_display_option == 2) {
            UnloadPointCloud(m_point_cloud);
            m_point_cloud = LoadPointCloud(m_positions.data(), m_colors.data(), m_pc_vertex_color_shader, m_positions.size());
        } else if (m_current_display_option == 3) {
            UnloadPointCloud(m_point_cloud);
            m_point_cloud = LoadPointCloud(m_positions.data(), m_classes.data(), m_pc_vertex_color_shader, m_positions.size());
        } else if (m_current_display_option == 4 && m_predictions.size() > 0) {
            UnloadPointCloud(m_point_cloud);
            m_point_cloud = LoadPointCloud(m_positions.data(), m_predictions.data(), m_pc_vertex_color_shader, m_positions.size());
        }
    }
    if (m_updating_predictions)
        ImGui::PopItemFlag();
}

void PLYSource::save_ply_file()
{
    happly::PLYData plyOut;

    // Add mesh data (elements are created automatically)
    std::string const vertexName = "vertex";
    size_t const N = m_positions.size();

    plyOut.addElement(vertexName, N);
    auto& vertexElement = plyOut.getElement(vertexName);

    // De-interleave
    std::vector<float> xPos(N);
    std::vector<float> yPos(N);
    std::vector<float> zPos(N);
    for (size_t i = 0; i < N; i++) {
        xPos[i] = m_positions[i].x;
        yPos[i] = m_positions[i].y;
        zPos[i] = m_positions[i].z;
    }

    if (m_colors.size() > 0) {
        std::vector<uint8_t> r(N);
        std::vector<uint8_t> g(N);
        std::vector<uint8_t> b(N);

        for (size_t i = 0; i < m_colors.size(); i++) {
            r[i] = m_colors[i].x * 255;
            g[i] = m_colors[i].y * 255;
            b[i] = m_colors[i].z * 255;
        }
        vertexElement.addProperty<uint8_t>("red", r);
        vertexElement.addProperty<uint8_t>("green", g);
        vertexElement.addProperty<uint8_t>("blue", b);
    }

    if (m_prediction_classes.size() > 0) {
        vertexElement.addProperty<uint8_t>("scalar_Label", m_prediction_classes);
    }
    // Store
    vertexElement.addProperty<float>("x", xPos);
    vertexElement.addProperty<float>("y", yPos);
    vertexElement.addProperty<float>("z", zPos);

    auto p = std::filesystem::path(m_file_path);

    p = p.parent_path() / (p.stem().string() + "_segmented.ply");
    plyOut.write(p.generic_string(), happly::DataFormat::Binary);
}

void PLYSource::load_ply_file()
{
    happly::PLYData plyIn(m_file_path);

    std::vector<std::array<double, 3>> in_pos = plyIn.getVertexPositions();
    std::vector<std::array<uint8_t, 3>> in_col;
    std::vector<float> in_class;

    if (plyIn.getElement("vertex").hasProperty("red"))
        in_col = plyIn.getVertexColors();

    if (plyIn.getElement("vertex").hasProperty("scalar_Label"))
        in_class = plyIn.getElement("vertex").getProperty<float>("scalar_Label");

    m_positions.resize(in_pos.size());
    m_colors.resize(in_col.size());
    m_classes.resize(in_class.size());

    float color_scale = 1.f;

    if (in_col.size() > 0) {
        for (int i = 0; i < std::min<int>(in_col.size(), 500); i++) {
            if (in_col[i][0] > 1 || in_col[i][1] > 1 || in_col[i][2] > 1) {
                color_scale = 1.f / 255.f;
                break;
            }
        }
    }

    Vector3 min = { std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max() };
    Vector3 max = { std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min() };

    for (int i = 0; i < in_pos.size(); i++) {
        m_positions[i] = { (float)in_pos[i][0], (float)in_pos[i][1], (float)in_pos[i][2] };
        min.x = std::min<float>(min.x, in_pos[i][0]);
        min.y = std::min<float>(min.y, in_pos[i][1]);
        min.z = std::min<float>(min.z, in_pos[i][2]);

        max.x = std::max<float>(max.x, in_pos[i][0]);
        max.y = std::max<float>(max.y, in_pos[i][1]);
        max.z = std::max<float>(max.z, in_pos[i][2]);

        if (in_col.size() > 0)
            m_colors[i] = { (float)in_col[i][0] * color_scale, (float)in_col[i][1] * color_scale, (float)in_col[i][2] * color_scale };
        if (in_class.size() > 0) {
            int c = std::clamp<int>(in_class[i], 0, class_colors.size() - 1);
            m_classes[i] = class_colors[c];
        }
    }

    
    // center the point cloud
    Vector3 center = (min + max) / 2;
    //m_offset = center * -1;

    double cx = center.x;
    double cy = center.y;
    double cz = center.z;

    for (int i = 0; i < in_pos.size(); i++) {
        m_positions[i] = { float(in_pos[i][0] - cx), float(in_pos[i][1] - cy), float(in_pos[i][2] - cz) };
    }

    if (m_colors.size() > 0)
        m_point_cloud = LoadPointCloud(m_positions.data(), m_colors.data(), m_pc_vertex_color_shader, (int)m_positions.size());
    else
        m_point_cloud = LoadPointCloud(m_positions.data(), nullptr, m_pc_shader, (int)m_positions.size());
    m_point_cloud.color = { 1.f, 1.f, 1.f, 1.f };
}

void PLYSource::set_predictions(uint8_t const* preds, size_t num)
{
    if (num != m_positions.size())
        return;
    m_current_display_option = 4;
    m_updating_predictions = true;
    m_predictions.resize(num);
    m_prediction_classes.resize(num);
    for (int i = 0; i < num; i++) {
        m_predictions[i] = class_colors[preds[i]];
        m_prediction_classes[i] = preds[i];
    }
    m_updating_predictions = false;
}

void PLYSource::update_point_cloud_predictions()
{
    if (m_point_cloud.shader.id != m_pc_vertex_color_shader.id) {
        UnloadPointCloud(m_point_cloud);
        m_point_cloud = LoadPointCloud(m_positions.data(), m_predictions.data(), m_pc_vertex_color_shader, m_positions.size());
    } else {
        UpdatePointCloud(&m_point_cloud, nullptr, m_predictions.data(), m_positions.size());
    }
}

}
