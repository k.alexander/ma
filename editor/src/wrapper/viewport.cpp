/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <imgui.h>

#include "wrapper/viewport.hpp"
#include "wrapper/window.hpp"

namespace RL {
Viewport::Viewport(Rectangle const& viewport_size_and_position, Vector3 const& pos)
    : m_camera(pos)
{
    auto w = float(GetScreenWidth());
    auto h = float(GetScreenHeight());
    m_viewport_dimensions.x = viewport_size_and_position.x * w;
    m_viewport_dimensions.y = viewport_size_and_position.y * h;
    m_viewport_dimensions.width = viewport_size_and_position.width * w;
    m_viewport_dimensions.height = viewport_size_and_position.height * h;
    m_viewport_dimensions_percentage = viewport_size_and_position;
    m_render_texture = new RenderTexture(int(viewport_size_and_position.width * w), int(viewport_size_and_position.height * h));
    m_camera.cam().projection = CAMERA_PERSPECTIVE;
}

void Viewport::render(Window* w)
{
    if (!w->get_gui().io()->WantCaptureMouse) {
        if (m_camera.cam().projection == CAMERA_PERSPECTIVE) {
            if (IsMouseButtonDown(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(GetMousePosition(), m_viewport_dimensions)) {
                UpdateCamera(&m_camera.cam(), CAMERA_CUSTOM);
                m_want_redraw = true;
            }
        } else if (m_camera.cam().projection == CAMERA_ORTHOGRAPHIC) {
            if (IsMouseButtonDown(MOUSE_BUTTON_MIDDLE) && CheckCollisionPointRec(GetMousePosition(), m_viewport_dimensions)) {
                auto const d = GetMouseDelta();
                m_camera.cam().position += { d.x * (m_camera.cam().fovy / 360), 0, d.y * (m_camera.cam().fovy / 360) };
                m_camera.cam().target += { d.x * (m_camera.cam().fovy / 360), 0, d.y * (m_camera.cam().fovy / 360) };
                m_want_redraw = true;
            }
        }
    }
    if (m_want_redraw) {
        m_want_redraw = false;
        BeginTextureMode(m_render_texture->rt());
        {
            ClearBackground({ 20, 22, 22, 0 });
            m_camera.begin_3d();
            w->render_scene();
            m_camera.end_3d();
        }
        EndTextureMode();
    }
}

void Viewport::draw(Window* w)
{
    DrawTextureRec(m_render_texture->rt().texture, m_render_texture->dim(), { m_viewport_dimensions.x, m_viewport_dimensions.y }, WHITE);
    DrawRectangleLinesEx(m_viewport_dimensions, 1, { 110, 110, 110, 255 });
    if (!m_name.empty()) {
        DrawText(m_name.c_str(), m_viewport_dimensions.x + 3, m_viewport_dimensions.y + m_viewport_dimensions.height - 12, 8, WHITE);
    }
}

void Viewport::on_resize(int w, int h)
{
    m_viewport_dimensions.x = m_viewport_dimensions_percentage.x * w;
    m_viewport_dimensions.y = m_viewport_dimensions_percentage.y * h;
    m_viewport_dimensions.width = m_viewport_dimensions_percentage.width * w;
    m_viewport_dimensions.height = m_viewport_dimensions_percentage.height * h;
    m_render_texture->Resize(m_viewport_dimensions.width, m_viewport_dimensions.height);
}

Viewport::~Viewport()
{
    delete m_render_texture;
}
void Viewport::handle_input(Window* w)
{
    m_camera.handle_input(w);
}
}
