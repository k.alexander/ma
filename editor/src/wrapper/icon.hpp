/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <raylib.h>

namespace IG {
class Icon {
public:
    Image m_image {};
    Texture m_texture {};
    Icon() = default;
    void load(unsigned char* data, int size);
    ~Icon();
    void* get_texture();

    bool draw_button();
};
}