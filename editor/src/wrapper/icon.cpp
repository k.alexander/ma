/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <imgui.h>

#include "wrapper/icon.hpp"

namespace IG {
void Icon::load(unsigned char* data, int size)
{
    m_image = LoadImageFromMemory(".png", data, size);
    m_texture = LoadTextureFromImage(m_image);
    auto dpi = GetWindowScaleDPI().x;
    m_image.width *= dpi;
    m_image.height *= dpi;
}

Icon::~Icon()
{
    UnloadTexture(m_texture);
    UnloadImage(m_image);
}

void* Icon::get_texture()
{
    return reinterpret_cast<void*>(m_texture.id);
}

bool Icon::draw_button()
{
    return ImGui::ImageButton(get_texture(), { float(m_image.width), float(m_image.height) }, { 0, 0 }, { 1, 1 }, 1);
}
}