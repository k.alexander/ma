/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <raylib.h>

namespace RL {
class Window;
class Camera {
    Camera3D m_camera {};
    Ray m_last_ray {};

public:
    explicit Camera(Vector3 const& pos, float fov = 45.0, Vector3 const& target = {}, Vector3 const& up = { 0, 1, 0 });
    void begin_3d();
    static void end_3d();
    void look_at(const Vector3& target);
    Camera3D& cam() { return m_camera; }
    void handle_input(Window* w);
    void set_projection(CameraProjection p) { m_camera.projection = p; }
    void set_up(Vector3 up) { m_camera.up = up; }
    void set_fov(float fovy) { m_camera.fovy = fovy; }
};
}
