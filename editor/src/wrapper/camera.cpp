/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "wrapper/camera.hpp"
#include "wrapper/types.hpp"
#include "wrapper/window.hpp"
#include "wrapper/viewport.hpp"

namespace RL {

Camera::Camera(Vector3 const& pos, float fov, Vector3 const& target, Vector3 const& up)
{
    m_camera.fovy = fov;
    m_camera.position = pos;
    m_camera.target = target;
    m_camera.up = up;
}

void Camera::begin_3d()
{
    BeginMode3D(m_camera);
}

void Camera::end_3d()
{
    EndMode3D();
}
void Camera::look_at(Vector3 const& target)
{
    m_camera.target = target;
    UpdateCamera(&m_camera, CAMERA_ORBITAL);
}

void Camera::handle_input(Window* w)
{
    if (GetMouseWheelMove() != 0) {
        if (m_camera.projection == CAMERA_ORTHOGRAPHIC) {
            m_camera.fovy += GetMouseWheelMove(); // * 0.1;
        } else {
            auto diff = m_camera.target - m_camera.position;
            m_camera.position += diff * (GetMouseWheelMove() / 10);
        }
        for (auto& vp : w->get_viewports())
            vp->force_redraw();
    }
    // if (IsMouseButtonDown(MOUSE_MIDDLE_BUTTON)) {
    //     auto ray = GetMouseRay(GetMousePosition(), m_camera);
    //     if (!Math::NearlyEqual(m_last_ray.position, {})) {
    //         auto diff = ray.direction - m_last_ray.direction;
    //         m_camera.position -= diff * 8 * Math::Min(Math::Magnitude(m_camera.position), 1.0f);
    //     }
    //     m_last_ray = ray;
    // } else {
    //     m_last_ray = {};
    // }
}

}
