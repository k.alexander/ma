/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <raylib.h>

namespace RL {
class RenderTexture {
    Rectangle m_dimensions {}; // y is flipped
    ::RenderTexture m_render_texture { 0 };

public:
    RenderTexture(int width, int height);
    void Resize(int width, int height);
    void unload();
    int w() const { return int(m_dimensions.width); }
    int h() const { return int(m_dimensions.height * -1); }
    ~RenderTexture();

    ::RenderTexture& rt() { return m_render_texture; }

    // With inverted y
    const Rectangle& dim() const { return m_dimensions; }
};
}
