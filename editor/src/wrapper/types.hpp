/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cmath>
#include <filesystem>
#include <raylib.h>

namespace Util {
inline bool IsFile(const std::string& PathString)
{
    namespace fs = std::filesystem;
    const fs::path path(PathString);
    std::error_code ec;
    if (fs::is_directory(path, ec))
        return false;
    if (ec)
        TraceLog(LOG_ERROR, "Error in is_directory: %s", ec.message().c_str());
    if (fs::is_regular_file(path, ec))
        return true;
    if (ec)
        TraceLog(LOG_ERROR, "Error in is_regular_file: %s", ec.message().c_str());
    return false;
}
}

namespace Math {

template<class T>
inline T Min(T const& a, T const& b)
{
    if (a < b)
        return a;
    return b;
}

template<class T>
inline T Max(T const& a, T const& b)
{
    if (a > b)
        return a;
    return b;
}

template<class T>
inline T Clamp(T const& min, T const& val, T const& max)
{
    if (val < min)
        return min;
    if (val > max)
        return max;
    return val;
}

inline bool NearlyEqual(float const& a, float const& b)
{
    return std::abs(a - b) < 0.000001;
}

inline bool NearlyEqual(Vector3 const& a, Vector3 const& b)
{
    return NearlyEqual(a.x, b.x) && NearlyEqual(a.y, b.y) && NearlyEqual(a.z, b.z);
}

inline float Magnitude(const Vector3& v)
{
    return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}
}

inline Vector2 operator-(Vector2 const& a, Vector2 const& b)
{
    return Vector2 { a.x - b.x, a.y - b.y };
}

inline Vector2 operator+(Vector2 const& a, Vector2 const& b)
{
    return Vector2 { a.x + b.x, a.y + b.y };
}

inline Vector3 operator-=(Vector3& a, Vector3 const& b)
{
    a = { a.x - b.x, a.y - b.y, a.z - b.z };
    return a;
}

inline Vector3 operator+=(Vector3& a, Vector3 const& b)
{
    a = { a.x + b.x, a.y + b.y, a.z + b.z };
    return a;
}

inline Vector3 operator+(Vector3 const& a, Vector3 const& b)
{
    return Vector3 { a.x + b.x, a.y + b.y, a.z + b.z };
}

inline Vector3 operator-(Vector3 const& a, Vector3 const& b)
{
    return Vector3 { a.x - b.x, a.y - b.y, a.z - b.z };
}

inline Vector3 operator*(Vector3 const& a, float const& b)
{
    return Vector3 { a.x * b, a.y * b, a.z * b };
}

inline Vector3 operator/(Vector3 const& a, float const& b)
{
    return Vector3 { a.x / b, a.y / b, a.z / b };
}