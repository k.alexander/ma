/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <ImSequencer.h>
#include <string>

#include "wrapper/icon.hpp"
#include "wrapper/render_texture.hpp"

class ImGuiIO;

namespace RL {
class Window;
}

namespace IG {

class Gui {
    ImGuiIO* m_io {};
    RL::RenderTexture* m_render_texture { nullptr };

public:
    Gui() = default;
    ~Gui();

    void init(RL::Window* win, int w, int h);
    void on_resize(int w, int h);
    ImGuiIO* io() { return m_io; }
    void begin_frame();
    void end_frame();
};
}
