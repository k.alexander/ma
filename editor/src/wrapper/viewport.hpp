/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <string>

#include "wrapper/camera.hpp"
#include "wrapper/render_texture.hpp"

namespace RL {
class Window;
class Viewport {
    Camera m_camera;
    Rectangle m_viewport_dimensions, m_viewport_dimensions_percentage {};
    std::string m_name {};
    RenderTexture* m_render_texture { nullptr };
    bool m_want_redraw {true};

public:
    // Position & Size is in screen percent
    explicit Viewport(Rectangle const& viewport_size_and_position, Vector3 const& pos = { 0, 0, 0 });
    ~Viewport();
    Camera& cam() { return m_camera; }

    void handle_input(Window* w);
    void set_name(const std::string& name) { m_name = name; }
    // Render scene into texture
    void render(Window* w);

    void on_resize(int w, int h);

    Rectangle const& dim() { return m_viewport_dimensions; }

    // Draw texture to screen
    void draw(Window* w);

    void look_at(const Vector3& target) { m_camera.look_at(target); }

    void force_redraw() { m_want_redraw = true; }
};
}
