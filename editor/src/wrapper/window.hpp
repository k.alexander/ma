/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <memory>
#include <vector>

#include "visualization/scene.hpp"
#include "wrapper/gui.hpp"
#include "wrapper/render_texture.hpp"

namespace RL {
class Viewport;
class Window {
    Image m_icon {};
    IG::Gui m_gui {};
    Viz::Scene m_scene;
    std::vector<std::shared_ptr<RL::Viewport>> m_viewports;
    Vector2 m_last_mouse_pos {};
    int m_current_frame { 0 };
    int m_fps { 30 };

public:
    explicit Window(int w = 1280, int h = 720);
    ~Window();

    void Run();
    static void begin_drawing();
    static void end_drawing();
    void render_scene();
    Viz::Scene& get_scene() { return m_scene; }
    Vector2 const& get_last_mouse_pos() const { return m_last_mouse_pos; }
    int* current_frame_ptr() { return &m_current_frame; }
    void set_current_frame(int f) { m_current_frame = f; }
    int get_current_frame() { return m_current_frame; }

    int* get_fps() { return &m_fps; }
    IG::Gui& get_gui() { return m_gui; }
    float get_frame_time();

    Viewport* get_viewport(int index) { return m_viewports[index].get(); }
    std::vector<std::shared_ptr<RL::Viewport>>& get_viewports() { return m_viewports;  }
};
}