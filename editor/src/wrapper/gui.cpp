/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_raylib.h>
#include <implot.h>
#include <raylib.h>

#include "misc/assets.hpp"
#include "visualization/data_source.hpp"
#include "wrapper/gui.hpp"
#include "wrapper/types.hpp"
#include "wrapper/window.hpp"

namespace IG {
void Gui::begin_frame()
{
    BeginTextureMode(m_render_texture->rt());
    ClearBackground({});
    ImGui_ImplRaylib_ProcessEvent();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplRaylib_NewFrame();
    ImGui::NewFrame();
}

Gui::~Gui()
{
    ImGui_ImplRaylib_Shutdown();
    ImGui::DestroyContext();
    ImPlot::DestroyContext();
    delete m_render_texture;
}

void Gui::end_frame()
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    EndTextureMode();
    DrawTextureRec(m_render_texture->rt().texture, m_render_texture->dim(), Vector2 { 0, 0 }, WHITE);
}

void Gui::init(RL::Window* win, int w, int h)
{
    int current = GetCurrentMonitor();
    auto DPI = GetWindowScaleDPI();

    m_render_texture = new RL::RenderTexture(w, h);
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();
    m_io = &ImGui::GetIO();
    ImFontConfig cfg {};
    cfg.SizePixels = roundf(13 * DPI.x);
    m_io->Fonts->AddFontDefault(&cfg);
    if (!m_io->Fonts->Build())
        TraceLog(LOG_ERROR, "ImGui failed to build font atlas");
    ImGui::StyleColorsDark();
    ImGui_ImplRaylib_Init();
    ImGui_ImplOpenGL3_Init();
}

void Gui::on_resize(int w, int h)
{
    m_render_texture->Resize(w, h);
}
}