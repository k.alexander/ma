/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "wrapper/render_texture.hpp"

namespace RL {
RenderTexture::RenderTexture(int width, int height)
{
    m_dimensions.width = float(width);
    m_dimensions.height = float(height * -1);
    m_render_texture = LoadRenderTexture(width, height);
}

RenderTexture::~RenderTexture()
{
    unload();
    m_render_texture = { 0 };
}

void RenderTexture::unload()
{
    UnloadRenderTexture(m_render_texture);
}

void RenderTexture::Resize(int width, int height)
{
    m_dimensions.height = float(height * -1);
    m_dimensions.width = float(width);
    unload();
    m_render_texture = LoadRenderTexture(width, height);
}
}