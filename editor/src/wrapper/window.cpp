/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <imgui.h>
#include <raylib.h>

#include "misc/assets.hpp"
#include "wrapper/viewport.hpp"
#include "wrapper/window.hpp"
#include "visualization/ply_source.hpp"

namespace RL {
Window::Window(int w, int h)
{
    SetTraceLogLevel(LOG_INFO);
    InitWindow(w, h, "Visualizer");
    m_icon = LoadImageFromMemory(".png", assets::get_icon(), assets::ICON_SIZE);
    if (m_icon.height > 0)
        SetWindowIcon(m_icon);
    SetTargetFPS(60);
    SetExitKey(-1);
    SetWindowState(FLAG_WINDOW_RESIZABLE);

    // Properly center on main monitor
    auto pos = GetMonitorPosition(0);
    int mh = GetMonitorHeight(0);
    int mw = GetMonitorWidth(0);
    SetWindowPosition(static_cast<int>(pos.x + float(mw - w) / 2),
        static_cast<int>(pos.y + float(mh - h) / 2));

    m_viewports.emplace_back(new Viewport(Rectangle { 0, 0, 0.6, 1 }, Vector3 { 90, 70, 90 }));
    m_viewports.emplace_back(new Viewport(Rectangle { 0.6, 0, 0.4, 0.5 }, Vector3 { 0, 10, 0 }));
    m_viewports[0]->look_at(Vector3 { 0, 0, 0 });
    m_viewports[0]->set_name("Main");


    m_viewports[1]->cam().set_up({ 0, 0, 1 });
    m_viewports[1]->look_at(Vector3 { 0, 0, 0 });
    m_viewports[1]->cam().set_projection(CAMERA_ORTHOGRAPHIC);
    m_viewports[1]->set_name("Top");
    m_viewports[1]->cam().set_fov(5);

    m_gui.init(this, w, h);
    m_scene.set_data_source<Viz::PLYSource>();
}

Window::~Window()
{
    CloseWindow();
    UnloadImage(m_icon);
}

void Window::Run()
{
    while (!WindowShouldClose()) {
        if (IsWindowResized()) {
            auto w = GetScreenWidth();
            auto h = GetScreenHeight();
            for (auto& viewport : m_viewports)
                viewport->on_resize(w, h);
            m_gui.on_resize(w, h);
        }

        for (auto& viewport : m_viewports) {
            if (!m_gui.io()->WantCaptureMouse && CheckCollisionPointRec(GetMousePosition(), viewport->dim()))
                viewport->handle_input(this);
            viewport->render(this);
        }

        // Draw viewport textures
        begin_drawing();
        for (auto& viewport : m_viewports)
            viewport->draw(this);
        m_gui.begin_frame();
       
        m_scene.draw_data_gui(this);
        m_gui.end_frame();

        DrawFPS(2, 1);
        end_drawing();
        m_last_mouse_pos = GetMousePosition();
    }
}

void Window::begin_drawing()
{
    ::BeginDrawing();
    ClearBackground({ 20, 22, 22, 255 });
}

void Window::end_drawing()
{
    ::EndDrawing();
}

float Window::get_frame_time()
{
    return m_current_frame / float(m_fps);
}

void Window::render_scene()
{
    m_scene.draw(this);
}

}