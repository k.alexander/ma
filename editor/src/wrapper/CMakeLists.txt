target_sources(
  App
  PRIVATE types.hpp
          render_texture.cpp
          render_texture.hpp
          viewport.cpp
          viewport.hpp
          gui.cpp
          gui.hpp
          window.cpp
          window.hpp
          camera.cpp
          camera.hpp
          icon.cpp
          icon.hpp)
