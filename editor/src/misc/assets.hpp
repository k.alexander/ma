/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace assets {
extern int ICON_SIZE;
extern unsigned char* get_icon();
extern int PLAY_SIZE;
extern unsigned char* get_play();
extern int START_SIZE;
extern unsigned char* get_start();
extern int PAUSE_SIZE;
extern unsigned char* get_pause();
extern int END_SIZE;
extern unsigned char* get_end();
}
