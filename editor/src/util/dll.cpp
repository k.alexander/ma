/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_WIN32)
#    define WIN32_LEAN_AND_MEAN
#    include <windows.h>
#else
#    include <dlfcn.h>
#endif
#include <iostream>

#include "util/dll.hpp"

namespace util::dll {
void* load(std::filesystem::path const& path)
{
#if defined(_WIN32)
    HMODULE dll = nullptr;
    SetDllDirectoryW(path.parent_path().wstring().c_str());

    dll = LoadLibraryW(path.wstring().c_str());

    SetDllDirectoryW(nullptr);

    if (!dll) {
        DWORD error = GetLastError();

        if (error == ERROR_PROC_NOT_FOUND)
            return nullptr;

        char* message = nullptr;

        FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER,
            nullptr, error,
            MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
            (LPSTR)&message, 0, nullptr);

        std::cerr << "LoadLibrary failed for " << path.string().c_str() << ": " << message << "(" << error << ")\n";

        if (message)
            LocalFree(message);
    }

    return dll;
#else
    void* res = nullptr;
    try {
        res = dlopen(path.c_str(), RTLD_LAZY);

        if (!res)
            std::cerr << "dlopen failed for" << path.c_str() << ": " << dlerror() << "\n";
    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
    }

    return res;
#endif
}

void* sym(void* dll, const char* func)
{
    void* handle;

#if defined(_WIN32)
    handle = (void*)GetProcAddress((HMODULE)dll, func);
#else
    handle = dlsym(dll, func);
#endif
    return handle;
}

void unload(void* dll)
{
#if defined(_WIN32)
    FreeLibrary((HMODULE)dll);
#else
    if (dll)
        dlclose(dll);
#endif
}
}
