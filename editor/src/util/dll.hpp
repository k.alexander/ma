/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <filesystem>

namespace util::dll {
void* load(std::filesystem::path const& path);

void* sym(void* dll, const char* func);

template<typename T>
T sym(void* dll, const char* func)
{
    return reinterpret_cast<T>(sym(dll, func));
}

void unload(void* dll);
}
